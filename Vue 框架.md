# 🍔Vue 框架🍔

## 基础介绍

### 什么是 Vue？

`Vue` 是一个**易用、灵活、高效的渐进式 `JavaScript` 框架**，其**优点或特性**🎏：

- 创建单页面应用的轻量级 `Web` 应用框架
- 简单易用
- 双向数据绑定/响应式
- 组件化的思想
- 视图，数据，结构分离
- 虚拟 `DOM`
- 数据驱动视图

> <u>🧩*知识扩展：*</u>
>
> 渐进式的理解：逐渐增强，通过逐步学习，集成更多的功能
>
> 库和框架的理解：
>
> - 库是方法的集合，每次调用方法，实现一个特定的功能，例如：`moment`、`axios` 等
>
> - 框架是一整套完整的项目解决方案，框架实现了大部分的功能，我们需要按照框架的规则写代码，例如：`Vue`、`React`、`Angular` 等

### 什么是 SPA 单页面应用？

> `SPA` 单页面应用（`	SinglePage Application`）指只有一个主页面的应用，**仅在页面初始化时加载一次相应的 `HTML`、`CSS` 和 `JS` 等资源**，所有功能在一个 `HTML` 页面上实现，对每一个功能模块进行组件化。**一旦页面加载完成，`SPA` 不会因为用户的跳转操作而进行页面的重新加载或跳转，而是通过路由机制来切换相关组件，实现局部资源的刷新，避免页面的重新加载**

**优点**：

1. 用户体验好、页面切换快，内容的改变不需要重新加载整个页面，避免了不必要的跳转和重复渲染

2. `SPA` 对服务器压力相对较小
3. 前后端职责分离，架构清晰，前端进行交互逻辑，后端负责处理数据

**缺点**：

1. 首屏（初次）加载慢：为实现单页面 `Web` 应用功能和显示效果，需要在加载资源的时候将 `CSS` 和 `JS` 统一加载，部分页面按需加载

2. 不利于 `SEO`：由于所有的内容都在一个页面中动态替换显示，所以在 `SEO` 上有着天然的弱势


> <u>🧩*知识扩展：*</u>
>
> `MPA` 多页面应用（`MultiPage Application`）指有多个独立页面的应用，每个页面必须重复加载 `JS`、`CSS` 等相关资源，多页应用跳转需要整页资源刷新

### 什么是 MVVM 设计模式？

> `MVVM` 是 `Model-View-ViewModel` 的缩写，是**一种软件架构设计模式**，不是具体的东西，可以说是**一种设计思想**，其中：
>
> - `Model` 代表**数据模型**，可定义数据和编写业务逻辑
> - `View` 代表**视图**，负责将数据渲染展示成页面
> - `ViewModel` 代表**视图模型**，**负责监听 `Model` 数据模型的数据变化并且控制 `View` 视图的更新，处理 `View` 视图的交互操作**，简单地讲，`Model` 和 `View` 之间并无直接关联，`ViewModel` 就是同步 `Model` 和 `View` 的一个对象，通过**双向数据绑定**将 `Model` 和 `View` 连接起来，`Model` 数据的变化会立即反应到 `View` 上，而 `View` 中的数据变化也会同步到 `Model` 中

**优点**：

1. 实现了视图（`View`）和模型（`Model`）的分离，**降低代码耦合，提⾼视图或逻辑的复⽤性**

2. 开发者**只需关注业务逻辑，不需要手动操作 `DOM`，不需要关注数据状态的同步问题**，复杂的数据状态维护完全由 `MVVM` 来统一管理

**缺点**：

**`Bug` 很难被调试**，因为使⽤双向绑定的模式，当看到界⾯异常，有可能是 `View` 代码有问题，也可能是 `Model` 代码有问题，数据绑定使得⼀个位置的 `Bug` 被快速传递到别的位置，要定位原始出问题的地⽅就变得不那么容易，另外，数据绑定的声明是指令式地写在 `View` 的模板当中，这些内容是没办法打断点 `debug` 的

> <u>🧩*知识扩展：*</u>
>
> 原生 `JS` 和 `Vue` 修改视图的思想分别是什么？
>
> - 原生 `JS` ：`DOM` 驱动，无论修改什么页面内容，先找对象，操作 `DOM`
> - `Vue`：数据驱动，想更新视图，直接操作数据即可，数据变化，视图自动更新

### 什么是组件化？

> **将一个完整的页面拆分成一个个功能明确的模块（小组件）的过程，就是组件化**
>
> 简单来说，组件是可复用的 `Vue` 实例，每个组件各自独立，且都包含自己的 `HTML`、`CSS`、`JS` 文件，**容易维护，便于复用，提高开发效率**

![组件化思想](images/组件化思想.png)

------



## Vue2.x

> 🔗`Vue2` 官方中文文档：https://v2.cn.vuejs.org/

### 插值表达式

> 使用插值表达式 `{{}}` 可以插入变量，在指定位置渲染指定变量的值，还可以实现字符串拼接，简单的算术运算，`API` 的调用以及三元表达式等作用，但是**它只能添加表达式，不能添加语句，并且不能在标签属性中使用**

```vue
<template>
	<div>
		<h1>说明插值表达式的使用</h1>
		<p>{{ myName }}</p>
		<!-- 1.拼接字符串 -->
		<p>我的名字叫：{{ myName }}</p>
		<p>{{ '我的名字叫：' + myName} }</p>
      
		<!-- 2.调用API -->
		<p>{{ '我的名字叫：' + myName.toUpperCase() }}</p>
      
		<!-- 3.简单的算术运算 -->
		<p>我的年龄是：{{ age + 1 }}</p>
      
		<!-- 4.三元表达式 -->
		<p>{{ age >= 18 ? '成年' : '未成年' }}</p>
      
		<!-- 但是要注意，里面不能写语句，如自增自减、if else语句等 -->
		<!-- <p>我的年龄是：{{ age++ }}</p> -->
		<!-- <p>{{ if(age >= 18) }}</p> -->
	</div>
</template>

<script>
export default {
	data() {
		return {
			myName: 'xiaoming',
			 age: 10
		}
	}
}
</script>
```



### v-text

> 和插值表达式的作用类似，但 `v-text` 会**不管标签之前有什么内容，完全的更新标签的内容**

```vue
<template>
	<div>
		<h1>说明v-text的使用</h1>
		<p v-text="msg"></p>
		<!-- 1.内容会被替换 -->
		<p v-text="msg">我也是其它的内容</p>
      
		<!-- 2.拼接字符串 -->
		<p v-text="msg + '我是拼接的内容'"></p>
      
		<!-- 3.简单的算术运算 -->
		<p v-text="age + 10"></p>
      
		<!-- 4.调用API -->
		<p v-text="msg.toUpperCase()"></p>
      
		<!-- 5.三元表达式 -->
		<p v-text="age >= 18 ? '成年' : '未成年'"></p>
	</div>
</template>

<script>
export default {
	data() {
		return {
			msg: 'helloworld',
			age: 10
		}
	}
}
</script>
```



### v-html

> 和 `v-text` 一样，`v-html` 会**不管标签之前有什么内容，完全的更新标签的内容，但它能解析网页结构（`HTML`）**，而插值表达式和 `v-text` 会原样输出，**📣一般用于富文本框**

```vue
<template>
	<div>
		<h1>说明v-html的使用</h1>
		<p v-html="msg"></p>
		<!-- 1.内容会被替换 -->
		<p v-html="msg">我也是其它的内容</p>
      
		<!-- 2.拼接字符串 -->
		<p v-html="msg + '我是拼接的内容'"></p>
      
		<!-- 3.简单的算术运算 -->
		<p v-html="age + 10"></p>
      
		<!-- 4.调用API -->
		<p v-html="msg.toUpperCase()"></p>
      
		<!-- 5.三元表达式 -->
		<p v-html="age >= 18 ? '成年' : '未成年'"></p>
      
		<!-- 6.解析HTML -->
		<p>{{ content }}</p>
		<p v-text="content"></p>
		<p v-html="content"></p>
	</div>
</template>

<script>
export default {
	data() {
		return {
			msg: 'helloworld',
			age: 10,
			content: '<p>你好世界！</p>'
		}
	}
}
</script>
```



### v-bind

> 实现**标签属性值的动态绑定**
>
> 📍语法：`<标签 v-bind:属性 = "值">`
>
> 📍简写：`<标签 :属性 = "值">`

```vue
<template>	
	<img :src="imageSrc">

	<!-- 属性名绑定 -->
	<img :[key]="imageSrc"></button>

	<!-- 内联字符串拼接 -->
	<img :src="'/path/to/images/' + fileName">

	<!-- class绑定 -->
	<!-- 类名带中横线，只能用引号：class-a => 'class-a' -->
	<div :class="{classA: isTrue, 'classB': isFalse}"></div>
	<div :class="classObject"></div>
	<div :class="[classA, classB]"></div>
	<div :class="[classA, {classB: isTrue, classC: isFalse}]"></div>

	<!-- style绑定 -->
	<!-- 样式名称带中横线，用小驼峰：font-size => fontSize -->
	<!-- 还可用引号：font-size => 'font-size' -->
	<div :style="{color: color, fontSize: fontSize + 'px'}"></div>
	<div :style="styleObject"></div>
	<div :style="[styleObjectA, styleObjectB]"></div>

	<!-- 绑定一个属性对象 -->
	<img v-bind="{src: imageSrc, 'other-attr': otherProp}"></div>
</template>

<script>
export default {
	data() {
		return {
			imageSrc: 'xxx',
			key: 'src',
			fileName: 'xxx',
			classA: 'aaa',
			classB: 'bbb',
			classC: 'ccc',
			isTrue: true,
			isFalse: false,
			classObject: {
				classA: true,
				'classB': false
			},
			color: 'red',
			fontSize: 30,
			styleObject: {
				color: 'red',
				fontSize: '13px'
			}
		}
	}
}
</script>
```



### v-model

> 实现**双向数据绑定**，但不是任何元素都能添加 `v-model` ，**只有 `input`、`textarea`、`select` 等表单元素以及组件可以使用**，其本质上不过是**语法糖**，它负责监听用户的输入事件以更新数据
>
> 📍语法：`<标签 v-model = "值">`
>
> 🧰常用修饰符：
>
> - `.number`：输入字符串以 `parseFloat()` 转为有效的数字
> - `.trim`：去除首尾空白字符
> - `.lazy`：取代 `input` 监听 `change` 事件

```vue
<template>	
	<!-- 文本 -->
	<input v-model="msg" placeholder="edit me">
	<p>msg:{{ msg }}</p>

	<!-- 多行文本 -->
	<p>message:{{ message }}</p>
	<textarea v-model="message" placeholder="edit me"></textarea>

	<!-- 单个复选框，绑定布尔值 -->
	<input type="checkbox" id="checkbox" v-model="checked">
	<label for="checkbox">{{ checked }}</label>

	<!-- 多个复选框，绑定同一个数组 -->
	<input type="checkbox" id="jack" value="Jack" v-model="checkedNames">
	<label for="jack">Jack</label>
	<input type="checkbox" id="john" value="John" v-model="checkedNames">
	<label for="john">John</label>
	<input type="checkbox" id="mike" value="Mike" v-model="checkedNames">
	<label for="mike">Mike</label>
	<p>{{ checkedNames }}</p>

	<!-- 单选按钮 -->
	<input type="radio" id="one" value="One" v-model="picked">
  <label for="one">One</label>
  <input type="radio" id="two" value="Two" v-model="picked">
  <label for="two">Two</label>
  <p>{{ picked }}</p>

	<!-- 选择框 -->
  <select v-model="selected">
    <option disabled value="">请选择</option>
    <option value="A">A</option>
    <option value="B">B</option>
    <option value="C">C</option>
  </select>
  <p>{{ selected }}</p>
</template>

<script>
export default {
	data() {
		return {
			msg: '',
			message: '',
			checked: false,
			checkedNames: [],
			picked: '',
			selected: ''
    }
  }
}
</script>
```

- 自定义组件中的 `v-model`

> **📦本质上是 `value` 属性和 `input` 事件的一层包装**，主要实现了两个操作：
>
> 1. 根据父组件传值，子组件给命名为 `value` 的 `props` 属性赋值
>
> 2. 父组件监听子组件元素发出的 `input` 事件

```vue
<!-- 子组件hm-input -->
<template>	
	<input
		type="text"
		:value="value"
		@input="$emit('input', $event.target.value)"
	/ >
</template>

<script>
export default {
	props: {
		value: String
	}
}
</script>
```

```vue
<!-- 父组件 -->
<template>	
	<hm-input
		:value="username"
		@input="username = $event"
	/ >
	<!-- 等价于 -->
	<hm-input v-model="username" />
</template>
```

> `v-model` 在内部为**不同的输入元素使用不同的 `props` 属性并抛出不同的事件**：
>
> - `<text>` 和 `<textarea>` 元素使用 `value` 属性和 `input` 事件
> - `<checkbox>` 和 `<radio>` 元素使用 `checked` 属性和 `change` 事件
> - `<select>` 元素使用 `value` 属性和 `change` 事件

一个组件上的 `v-model` 默认会利用名为 `value` 的 `prop` 和名为 `input` 的事件，但是像单选框、复选框等类型的输入控件可能会将 `value` 属性用于其他不同的目的，但 **`model` 选项可以用来避免这样的冲突**：

```vue
<!-- 子组件hm-checkbox -->
<template>	
	<input
		type="checkbox"
		:checked="checked"
		@change="$emit('change', $event.target.checked)"
	/ >
</template>

<script>
export default {
	model: {
		prop: 'checked',
		event: 'change'
	},
	props: {
		checked: Boolean
	}
}
</script>
```

```vue
<!-- 父组件 -->
<template>	
	<hm-checkbox v-model="checked" />
</template>
```



### v-show 和 v-if

> `v-show` 通过**为元素设置 `display` 样式**实现元素的显示和隐藏，如果 `bool` 值为 `true` 则显示，为 `false` 则添加 `display：none` 样式隐藏元素
>
> 📍语法：`<标签 v-show = "bool 值">`

> `v-if` 通过**元素的创建和销毁**来实现元素的显示和隐藏，如果 `bool` 值为 `false` 时，不会创建元素，当想切换多个元素时，可在 `<template>` 元素上使用 `v-if` 条件渲染，最终渲染不包含 `<template>` 元素
>
> 📍语法：`<标签 v-if = "bool 值">`
>
> `v-if` 还有另外一个作用是**作为分支结构，实现多个条件的控制**
>
> 📍语法：
>
> ```html
> <标签 v-if = "bool 值">
> <标签 v-else-if = "bool 值">
> ...
> <标签 v-else = "bool 值">
> ```
>
> ⚠需要注意的是， **`v-else` 元素必须紧跟在带 `v-if` 或者 `v-else-if` 的元素的后面，否则它将不会被识别**

两者的应用场景📣：

- 如果是频繁的切换显示隐藏状态，使用 `v-show`（`v-show` 只是控制 `CSS` 样式，而 `v-if` 频繁切换会大量的创建和销毁元素，消耗性能）

- 如果是不用频繁切换，要么显示，要么隐藏的情况，适合于用 `v-if`（`v-if` 是惰性的，如果初始值为  `false` ，那么这些元素就直接不创建了，节省初始渲染的开销）



### v-for

> 可以**遍历数组或者对象**，用于动态渲染结构，⚠需要注意的是，**当和 `v-if` 一起使用时，`v-for` 的优先级比 `v-if` 更高**，每次渲染都会先执行循环再判断条件，无论如何循环都不可避免，浪费性能，要避免出现这种情况，则在外层嵌套 `<template>`，在这一层进行 `v-if` 判断，然后在内部进行 `v-for` 循环，而如果条件出现在循环内部，可通过计算属性提前过滤掉那些不需要显示的项
>
> 📍语法：
>
> - 数组：`<标签 v-for = "(item, index) in array" :key = "唯一值">`
> - 对象：`<标签 v-for = "(value, key, index) in object" :key = "唯一值">`
>
> 其中 `key` 要求：必须是字符串或者数字，且要保证唯一性（标准的 `key` 需要指定成 `id` ）

当 `Vue` 在更新使用 `v-for` 渲染的元素列表时，它**默认使用“就地更新”的策略🛠：`Vue` 会尽可能的就地（同层级，同位置）对比虚拟 `DOM` ，复用旧 `DOM` 结构，进行差异化更新。**



### v-on

> `v-on` 简单来说就是**为标签元素绑定事件**，其有三种写法✏：
>
> 1. `<标签  v-on:事件类型 = "少量代码">`
> 2. `<标签  v-on:事件类型 = "事件处理函数">`
> 3. `<标签  v-on:事件类型 = "事件处理函数(参数)">`
>
> 📍简写：`<标签  @事件类型 = "事件处理函数">`
>
> 📍事件修饰符语法：
>
> 1. `<标签  @事件类型.修饰符>`
> 2. `<标签  @事件类型.修饰符 = "事件处理函数">`
>
> 🧰常用的事件修饰符，有以下几种：
>
> - `.prevent`：调用 `event.preventDefault()` 阻止事件默认行为
> - `.stop`：调用 `event.stopPropagation()` 阻止事件冒泡
> - `.{keyCode | keyAlias}`：只当事件是从特定键码或者键别名触发时才触发回调
> - `.native`：监听组件根元素的原生事件

（1）事件处理函数的**定义位置**

在 `Vue` 中，函数不在 `data` 中定义，有一个专门的结构来定义用户自定义函数：`methods`，**在 `methods `中可通过使用 `this` 获取当前组件实例的任何成员。**

（2）事件处理函数的**参数**

- 如果事件处理函数没有手动传递参数，则会默认传递一个事件对象


```js
addCount(e) {
	// 通过形参获取
	// ...
}
```

- 如果手动传递了参数，那么默认的事件对象就不再传递，**如果还想使用事件对象，则需要手动传递 `$event` (🚨名称绝对不能改)**


```vue
<div @click="addCount(100, $event)">add</div>
```

```js
addCount(num, e) {
	// 通过传递实参获取
	// ...
}
```



### 自定义指令

> 自己定义指令，封装 `DOM` 操作，扩展额外功能，其使用方式有以下两种：
>
> 1. `<标签 v-自定义指令名称>`
> 2. `<标签 v-自定义指令名称 = "值">`

- **全局**注册

> 在组件外部通过 `Vue.directive` 创建全局指令，所有组件都可以引入使用

```js
src/main.js
-------------------------
import Vue from 'vue'
Vue.directive('自定义指令名称', {
	inserted(el, binding) {
		// ...
	}
})
```

- **局部**注册

> 在组件内部通过 `directives` 来创建，只有当前组件可以使用

```vue
<script>
export default {
	directives: {
		自定义指令名称: {
			inserted(el, binding) {
				// ...
			}
		}
	}
}
</script>
```

- 指令**封装**

```js
src/utils/directives.js
-------------------------
export const 自定义指令名称 = {
	inserted(el, binding){
		// ...
	}
}
```

#### 钩子函数

> 以下为 `Vue2` 官方文档说明

一个指令定义对象可以提供如下几个钩子函数：

- `bind`：只调用一次，指令第一次绑定到元素时调用
- `inserted`：被绑定元素插入父节点时调用（仅保证父节点存在，但不一定已被插入文档中）
- `update`：所在组件的 `VNode` 更新时调用，但是可能发生在其子 `VNode` 更新之前（指令的值可能发生了改变，也可能没有）
- `componentUpdated`：指令所在组件的 `VNode` 及其子 `VNode` 全部更新后调用
- `unbind`：只调用一次，指令与元素解绑时调用

#### 钩子函数参数

> 以下为 `Vue2` 官方文档说明

指令钩子函数会被传入以下常用几个参数：

- `el`：指令所绑定的元素，可以用来直接操作 `DOM`

- `binding`：一个对象，包含以下属性：
  - `name`：指令名，不包括 `v-` 前缀

  - `value`：指令的绑定值，例如：`v-my-directive="1 + 1"` 中，绑定值为 `2`

  - `oldValue`：指令绑定的前一个值，仅在 `update` 和 `componentUpdated` 钩子中可用，无论值是否改变都可用



### 计算属性 computed

> 一个特殊属性，可以监听依赖项数据的变化，并且在数据变化时执行某个业务逻辑，避免在模板中写太多的业务逻辑，⚠注意的是**不能监听异步操作中的数据变化（异步请求或者更改 `DOM` 等）**
>
> 📍语法：
>
> ```js
> computed: {
> 	计算属性名称() {
> 	...
> 	return 值
> 	}
> }
> ```
>
> ⚠注意：
>
> - 计算属性必须定义在 `computed` 中，并且不要和 `data` 里重名
> - **计算属性必须是一个 `function` 函数，计算属性必须有 `return` 返回值**
> - 计算属性不能被当作方法调用，要作为属性来使用
>
> 计算属性默认情况下只能获取，不能修改，当**要给计算属性进行赋值**时，📍其语法的完整写法如下所示：
>
> ```js
> computed: {
> 	计算属性名称: {
> 		get() {
> 			...
> 			return 值
> 		},
> 		set(value) {
> 			...
> 		}
> 	}
> }
> ```

- 计算属性 VS 方法

|                           计算属性                           |                         方法                         |
| :----------------------------------------------------------: | :--------------------------------------------------: |
| **计算属性是基于依赖项进行缓存的**，当执行一次计算属性之后，会将当前的计算结果存储到缓存，在依赖项的值没有发生变化的时候，反复的调用不会真正的执行计算属性的函数，而是直接从缓存中获取之前的结果，**只有依赖项发生了变化，才会重新触发计算属性** | `methods` 中的方法，调用就一定会执行，没有缓存这一说 |



### 侦听器 watch

> 侦听器是**真正意义上的监听**，不需要手动调用，当所侦听的属性值（`data`、`computed`）发生改变，就会自动触发
>
> 📍语法：
>
> ```js
> watch: {
> 	被侦听的属性名称(newVal, oldVal) {
> 	.....
> 	}
> }
> ```
>
> ⚠注意：**它的命名不能随意，必须和想侦听的属性名称完全一致**
>
> 🧩侦听器有以下两种扩展：
>
> （1）**深度侦听**
>
> 可以侦听**对象内部属性值**的变化，其有两种方式：
>
> - 侦听指定对象的**所有属性值变化**
>
>
> ```js
> watch: {
> 	被侦听的对象名: {
> 		deep: true,
> 		handler(newObj, oldObj) {
> 			...
> 		}
> 	}
> }
> ```
>
> - 侦听指定对象的**某一个属性值变化**
>
>
> ```js
> watch: {
> 	'对象.属性'(newVal, oldVal) {
> 			...
> 	}
> }
> ```
>
> ⚠注意：**必须用引号包裹住属性名**
>
> （2）**立即执行**
>
> 如果想在侦听开始之后被立即调用，可设置 `immediate: true`

与 `computed` 计算属性有以下几点差异：

1. 它不用手动调用，它是自动触发

2. 它没有返回值，所以一般是将结果赋值给另外一个变量

3. 它**可以侦听异步操作中数据的变化（例如更改 `DOM`，或是根据异步操作的结果去修改另一处的状态）**



### .sync 修饰符

> 在有些情况下，可能需要**对一个 `props` 数据进行“双向绑定”**

（1）在子组件中发出**以 `update:` 开头的事件**

```js
this.$emit('update:title', newTitle)
```

（2）父组件可以监听这个事件并根据需要更新数据

```html
<text-document
  :title="doc.title"
  @update:title="doc.title = $event"
></text-document>
<!-- 等价于 -->
<text-document :title.sync="doc.title"></text-document>
```

**⚠注意带有 `.sync` 修饰符的 `v-bind` 不能和表达式一起使用。**（🌰例如 `:title.sync="doc.title + '!'"` 是无效的）



### ref 和 $refs

> **用于获取 `DOM` 元素或者组件实例，以便使用其中的属性或者方法**
>
> `ref` 被用来给元素或子组件注册引用信息，引用信息将会注册在父组件的 `$refs` 对象上，**如果在普通 `DOM` 元素上使用，引用指向的是 `DOM` 元素，而如果用在子组件上，引用就指向组件实例**
>
> 当 `v-for` 用于元素或组件的时候，引用信息将是包含 `DOM` 节点或组件实例的数组
>
> ⚠注意：
>
> - **如果有多个元素设置了相同名称的 `ref` ，那么通过 `$refs` 只能获取到最后一个**，意味着设置 `ref` 的时候，一定要**注意不要重名**
> - **`$refs` 只会在组件渲染完成之后生效，并且它们不是响应式的，应该避免在模板或计算属性中访问 `$refs`**

```html
<!-- 获取DOM元素 -->
<p ref="p">hello</p>
<!-- 获取组件实例 -->
<child-component ref="child"></child-component>
```

```js
// 在父组件中使用$refs获取组件实例 
this.$refs.child
```



### $children 和 $parent

> `$parent` 可以用来从一个子组件访问父组件的实例
>
> `$children` 可以获取当前实例的直接子组件，返回的是一个**数组集合**，如果能清楚知道子组件的顺序，也可以使用下标来操作，⚠但需要注意的是，**`$children` 并不保证顺序，也不是响应式的**

```html
<template>
	<div class="hello_world">
		<com-a></com-a>
    <com-b></com-b>
  </div>
</template>
```

```vue
this.$children[0] => <com-a></com-a>
this.$children[1] => <com-b></com-b>
```



### $nextTick

> **可将回调延迟到下次 `DOM` 更新循环之后执行**，在修改数据后立即使用这个方法，可获取更新后的 `DOM`
>
> 因为**在 `Vue` 中更新 `DOM` 是异步执行的**，📣在一些情况下，如虽然数据改变了，但 `DOM` 还没更新完毕，需要等待其更新之后才能调用某些方法，这时候就需要用到 `$nextTick`，或者在 `created()` 生命周期进行 `DOM` 操作，此时页面的 `DOM` 还未渲染，没办法操作 `DOM`，因此也需要用到 `$nextTick`
>
> `$nextTick` 的核心🛠是利用了如 `Promise`、`setTimeout` 等原生 `JavaScript` 方法来模拟对应的宏、微任务的实现，**本质是为了利用 `JavaScript` 的这些异步回调任务队列来实现 `Vue` 框架中自己的异步回调队列**

```js
methods: {
	// ...
	example() {
	// 修改数据
	this.message = 'changed'
	// DOM还没有更新
	this.$nextTick(function() {
		// DOM已被更新
		this.doSomethingElse()
	})
	}
}
```



### slot 插槽

> 如果希望能够**自定义组件内部的一些结构**，可以用到 `<slot>` 插槽

#### 默认插槽

> 又名匿名插槽，当 `<slot>` 没有指定 `name` 属性值的时候，是一个默认显示插槽，**一个组件内只有一个匿名插槽**

（1）基本语法

- 自定义组件内用 `<slot></slot>` 占位，**如果没有包含一个 `<slot>` 元素，则该组件起始标签和结束标签之间的任何内容都会被抛弃**


```html
<template>	
	<a
		v-bind:href="url"
		class="nav-link"
	>
  	<slot></slot>
	</a>
</template>
```

- 在父组件中自定义组件标签之间添加内容，当组件渲染的时候，`<slot></slot>` 将会被替换，**插槽内可以包含任何模板代码，包括 `HTML` 甚至其它的组件**


```html
<navigation-link url="/profile">
  自定义内容
</navigation-link>

<navigation-link url="/profile">
  <!-- 添加一个 Font Awesome 图标 -->
  <span class="fa fa-user"></span>
  自定义内容
</navigation-link>

<navigation-link url="/profile">
  <!-- 添加一个图标的组件 -->
  <font-awesome-icon name="user"></font-awesome-icon>
  自定义内容
</navigation-link>
```

（2）默认内容

在封装自定义组件时，可以为预留的 `<slot>` 插槽提供默认内容，它只会**在没有提供内容的时候被渲染**。

```html
<template>	
	<a
		:href="url"
		class="nav-link"
	>
  	<slot>默认内容</slot>
	</a>
</template>
```

#### 具名插槽

> 带有具体名字的插槽，也就是带有 `name` 属性的 `<slot>`，**一个组件可以出现多个具名插槽**，使用多个插槽需要 `name` 属性来区分每个插槽，**不带 `name` 属性的 `<slot>` 会带有默认名字 “`default`”**

在向具名插槽提供内容的时候，可以在一个 `<template>` 元素上使用 `v-slot` 指令，并以 `v-slot` 的参数形式提供其名称，跟 `v-on` 和 `v-bind` 一样，**`v-slot` 也有缩写，即替换为字符 `#`**，🌰例如 `v-slot:header` 可以被重写为 `#header`。

```html
<div class="container">
  <header>
    <slot name="header"></slot>
  </header>
  <main>
    <slot></slot>
  </main>
  <footer>
    <slot name="footer"></slot>
  </footer>
</div>
```

```html
<base-layout>
  <template #header>
    <h1>标题</h1>
  </template>

  <!-- 默认插槽方式1： -->
  <!-- <p>主要内容</p> -->
    
  <!-- 默认插槽方式2： -->
  <template #default>
    <p>主要内容</p>
  </template>

  <template #footer>
    <button>关闭</button>
  </template>
</base-layout>
```

#### 作用域插槽

> 在定义 `<slot>` 插槽的同时，**在插槽上也可以绑定数据进行传值的，让父组件的插槽内容能够访问子组件内部的数据**，根据子组件的传递过来的数据决定如何渲染该插槽	

```html
<span>
  <slot :username="user.username" :age="18"></slot>
</span>
```

绑定在 `<slot>` 元素上的属性被称为插槽 `prop` ，都会被收集到一个对象中，⚠需要注意的是，**一般情况下 `v-slot` 只能添加在 `<template>` 元素上，当被提供的内容只有默认插槽时，组件的标签才可以被当作插槽的模板来使用。**

```html
<current-user>
  <template #default="slotProps">
  	<p>名字：{{ slotProps.username }}</p>
    <p>年龄：{{ slotProps.age }}</p>
  </template>
</current-user>

<!-- 注意默认插槽的缩写语法不能和具名插槽混用 -->
<!-- 只要出现多个插槽，请始终为所有的插槽使用完整的基于<template>的语法 -->
<current-user v-slot="slotProps">
  <p>名字：{{ slotProps.username }}</p>
  <p>年龄：{{ slotProps.age }}</p>
</current-user>
```

当插槽提供了多个属性的时候，也可以使用**解构**来传入具体的插槽属性：

```html
<current-user v-slot="{username, age}">
  <p>名字：{{ username }}</p>
  <p>年龄：{{ age }}</p>
</current-user>

<!-- 重命名 -->
<current-user v-slot="{username: name, age}">
  <p>名字：{{ name }}</p>
  <p>年龄：{{ age }}</p>
</current-user>

<!-- 设置默认值，用于插槽属性是undefined的情形 -->
<current-user v-slot="{username = '小明', age}">
  <p>名字：{{ username }}</p>
  <p>年龄：{{ age }}</p>
</current-user>
```



### 组件基础与通讯

#### 组件的注册使用

（1）**创建**组件，封装要复用的 `HTML` 标签，`CSS` 样式，`JS` 代码

（2）**引入**组件

（3）**注册**组件

- **全局**注册：在 `main.js` 中注册

![全局注册](images/全局注册.png)

- **局部**注册：在 `xxx.vue` 中注册（**常用👍**）

![局部注册](images/局部注册.png)

在进行组件的注册时，定义组件名的方式有两种：

|                       方式                        | 说明                                                         |
| :-----------------------------------------------: | :----------------------------------------------------------- |
| **短横线命名法**（例如 `hm-header` 和 `hm-main`） | 注册： `Vue.component('hm-button', HmButton)`<br />使用： `<hm-button></hm-button>` |
|  **大驼峰命名法**（例如 `HmHeader` 和 `HmMain`）  | 注册：`Vue.component('HmButton', HmButton)`<br />使用：`<HmButton></HmButton>` 和 `<hm-button></hm-button>` 都可以 |

**写法推荐👍**：

- **定义组件名时，使用大驼峰命名法**，更加方便，即 `Vue.component('HmButton', HmButton)`

- **使用组件时**，遵循 `HTML5`（`HTML` 大小写不敏感）规范，**使用短横线命名法**，即 `<hm-button> </hm-button>`


⚠注意：组件名不能和内置的 `HTML` 标签同名

（4）**使用**组件：组件名当成 `<组件名></组件名>` 标签使用即可

#### 组件的样式冲突

> 默认情况下，写在组件中的样式会全局生效，因此很容易造成多个组件之间的样式冲突问题

可以在 `<style>` 标签添加 `scoped` 属性，使它的 `CSS` 样式**只作用于当前组件中的元素**，它的原理🛠如下：

- **当前组件内标签都被添加 `data-v-hash` 值的属性**

- **`CSS` 选择器都被添加 `[data-v-hash值]` 的属性选择器**

🧱最终效果：必须是当前组件的元素，才会有这个自定义属性，才会被这个样式作用到。

![组件的样式冲突](images/组件的样式冲突.png)

> <u>*🧩扩展知识：*</u> 样式穿透 `::v-deep`

#### 组件的通信方式

> 父子关系：因为在单文件组件中不能通过 `components` 来创建结构上的子组件，意味着在项目，所谓父组件和子组件都是单独的单文件组件，它们更多是的**引入使用关系**

##### 父传子

> 将父组件中的数据传递给子组件，让子组件来使用
>
> `props` 官方推荐写法👍： **声明时用小驼峰命名法，使用时用短横线命名法**

（1）在子组件中通过 **`props` 属性（数组或对象**）来定义接收父组件的数据，在 `props` 中声明的成员可以像在 `data` 中声明的成员一样来使用

```js
// 方式1：数组
props: ['username']

// 方式2：对象 => 提供props验证
props: {
	username: String // 类型检测
}
// 或者
props: {
	username: {
		// type: String,
		type: [String, Number], // 多类型检测
		required: true, // 必填项
		default: ''  // 设置默认值，注意的是对象或数组的默认值必须从一个工厂函数返回
	},
	age: {
		type: Number,
		// 自定义验证函数，在非生产环境下，如果该函数返回一个falsy的值 (也就是验证失败)，控制台警告将会被抛出
		validator: function(value) {
			return value >= 0
		}
	}
}
```

（2）在父组件中使用子组件的位置，为子组件的 `props` 中的成员赋值

- **如果赋的值是一个变量（数字、布尔值等），则需要使用动态绑定 `v-bind`**
- 如果赋的值是一个字符串值，则直接赋值

```html
<son :username="name"></son>
```

##### 子传父

> 将子组件的数据传递给父组件

（1）在子组件中**通过 `this.$emit('自定义事件名'，{ 数据对象 })` 发出一个事件**，同时传递相应的数据

```js
methods:{
	sayPrice(){
		// 官方推荐使用 kebab-case 的自定义事件名
		this.$emit('get-son-price', 100)
	}
}
```

（2）在父组件中监听子组件发出的事件，并调用函数获取事件所传递的数据，为父组件中的成员赋值

```html
<son :username="name" @get-son-price="getSonPrice"></son>
```

```js
methods: {
	getSonPrice(price) {
		// ...
	}
}
```

##### 事件总线 eventBus

> 使用 `Vue` 实例的 `$on`、`$emit` 两个方法来实现**任何组件之间**的通信，但会导致组件之间相互依赖，不利于复用，所以这个方法不常用

（1）创建一个全局的 `Vue` 实例，即事件总线

```js
src/utils/eventBus.js
-------------------------
import Vue from 'vue'
export default new Vue()
```

（2）在一个组件中通过事件总线发出事件

```js
// 引入事件总结
import eventBus from '@/utils/eventBus.js'
------------------------------------
methods: {
	sendYouName() {
		eventBus.$emit('getYouName', name)
	}
}
```

（3）在需要监听的组件中通过事件总线来监听事件并对数据进行处理

```js
// 引入事件总结
import eventBus from '@/utils/eventBus.js'
--------------------------------------------------
// 在组件一加载完毕就添加对事件的监听
mounted () {
	// 需要注意的是，回调函数必须是箭头函数
	eventBus.$on('getYouName', (data) => {
		// ...
	})
}
```

#### 动态组件

> 通过 `Vue` 的 `<component>` 元素加一个特殊的 `is` 属性来实现**同一位置的不同组件之间动态切换**
>
> 📍语法：`<component :is="已注册组件名字"></component>`

#### 缓存组件

> `<keep-alive>` 是 `Vue` 提供的一个内置组件，主要作用是用于**保留组件状态或避免重新渲染**
>
> `<keep-alive>` 包裹动态组件时，会缓存不活动的组件实例，在组件切换过程中将状态保留在内存中，而不是销毁它们，它的 `activated` 和 `deactivated` 这两个生命周期钩子函数也将会被相应执行，同时`beforeDestroy` 和 `destroyed` 就不会再被触发
>
> `<keep-alive>` 有三个属性：
>
> - `include`：字符串、正则表达式或数组，只有名称匹配的组件会被缓存
> - `exclude`：字符串、正则表达式或数组，名称匹配的组件都不会被缓存
> - `max`：数字，最多可以缓存多少组件实例，一旦这个数字达到了，在新实例被创建之前，已缓存组件中最久没有被访问的实例会被销毁掉
>
> 🛠匹配首先检查组件自身的 `name` 选项，如果 `name` 选项不可用，则匹配它的局部注册名称（父组件 `components` 选项的键值）

```html
<!-- 基本 -->
<keep-alive>
  <component :is="view"></component>
</keep-alive>

<!-- 多个条件判断的子组件 -->
<keep-alive>
  <comp-a v-if="a > 1"></comp-a>
  <comp-b v-else></comp-b>
</keep-alive>

<!-- 和<transition>一起使用 -->
<transition>
  <keep-alive>
    <component :is="view"></component>
  </keep-alive>
</transition>

<!-- 逗号分隔字符串 -->
<keep-alive include="a,b">
  <component :is="view"></component>
</keep-alive>

<!-- 正则表达式 (使用v-bind) -->
<keep-alive :include="/a|b/">
  <component :is="view"></component>
</keep-alive>

<!-- 数组 (使用v-bind) -->
<keep-alive :include="['a', 'b']">
  <component :is="view"></component>
</keep-alive>

<keep-alive :max="10">
  <component :is="view"></component>
</keep-alive>
```



### 生命周期函数

> 一个组件即一个 `Vue` 实例，每个 `Vue` 实例都有⼀个完整的⽣命周期：
>
> 1. 开始创建 (空实例)
>
> 2. 初始化数据
>
> 3. 编译模版
>
> 4. 挂载 `DOM`
>
> 5. 渲染、更新数据 => 重新渲染
>
> 6. 卸载
>
>
> 这⼀系列过程我们称之为 `Vue` 的⽣命周期

![⽣命周期的三大阶段](images/⽣命周期的三大阶段.png)

- **<u>生命周期示意图</u>**

![生命周期示意图](images/生命周期示意图.png)

- **<u>各个生命周期钩子函数的作用</u>**

|          生命周期          | 执行时机                                                     |
| :------------------------: | :----------------------------------------------------------- |
|       `beforeCreate`       | 在实例创建之后，进行数据侦听和事件/侦听器的配置之前同步调用（也就是说**不能访问到 `data`、`computed`、`watch`、`methods` 上的数据和方法**） |
| **`created`**（**常用👍**） | 在实例初始化完成之后被立即同步调用，在这一步中，实例已完成对选项的处理，意味着**以下内容已被配置完毕：数据侦听、计算属性、方法、事件/侦听器的回调函数**，然而挂载阶段还没开始，真实 `DOM` 还未⽣成 |
|       `beforeMount`        | 在挂载开始之前被调⽤（此时实例已完成模板编译，相关的 `render` 函数⾸次被调⽤） |
| **`mounted`**（**常用👍**） | 实例被挂载后调用，**初次渲染的 `DOM` 可以进行操作**（⚠注意 `mounted` 不会保证所有的子组件也都被挂载完成，如果希望等到整个视图都渲染完毕再执行某些操作，可以在 `mounted` 内部使用 `$nextTick`） |
|       `beforeUpdate`       | 在**数据发生改变后，`DOM` 被更新之前**被调用（发⽣在虚拟 `DOM` 打补丁之前） |
|         `updated`          | 在**数据更改导致的虚拟 `DOM` 重新渲染和更新完毕之后**被调用（⚠注意 `updated` 不会保证所有的子组件也都被重新渲染完毕，如果希望等到整个视图都渲染完毕，也可以在 `updated` 里使用 `$nextTick` ） |
|        `activated`         | 被 `<keep-alive>` 缓存的组件激活时调用                       |
|       `deactivated`        | 被 `<keep-alive>` 缓存的组件失活时调用                       |
|      `beforeDestroy`       | 在实例销毁之前调⽤，实例仍然完全可用，`this` 仍能获取到实例  |
|        `destroyed`         | 在实例销毁之后调用，对应 `Vue` 实例的所有指令都被解绑，所有的事件监听器被移除，所有的子实例也都被销毁（📣像定时器，`WebScoket` 连接等资源的回收，需要手动释放，防止内存泄漏） |

可以在钩子函数 `created`、`beforeMount`、`mounted` 中请求异步数据，因为在这三个钩子函数中 `data` 已经创建，可以将服务端返回的数据进行赋值，**👍推荐在 `created` 钩子函数中调用异步请求**，在该函数中调用异步请求有以下**优点**：

1. 能更快获取到服务端数据，减少页面加载时间，用户体验更好
2. `SSR` 不支持 `beforeMount` 、`mounted` 钩子函数，放在 `created` 中有助于一致性



### 📌*进阶知识点*📌

#### *MVC 与 MVP 的区别是什么？*

> `MVC`、`MVP` 和 `MVVM` 是三种常见的软件架构设计模式

| `MVC`                                                        | `MVP`                                                        |
| :----------------------------------------------------------- | ------------------------------------------------------------ |
| `MVC` 通过分离 `Model`、`View` 和 `Controller` 的方式来组织代码结构，其中 `View` 负责页面的显示逻辑，`Model` 负责存储页面的业务数据，以及对相应数据的操作，并且 `View` 和 `Model` 应用了观察者模式，**当 `Model` 层发生改变的时候，它会通知有关 `View` 层更新页面**<br />`Controller` 层是 `View` 层和 `Model` 层的纽带，它主要负责用户与应用的响应操作，当**用户与页面产生交互的时候，`Controller` 中的事件触发器就开始工作，通过调用 `Model` 层，来完成对 `Model` 的修改，然后 `Model` 层再去通知 `View` 层更新** | **`MVP` 模式与 `MVC` 唯一不同的在于 `Presenter` 和 `Controller`**，在 `MVC` 模式中是使用观察者模式来实现当 `Model` 层数据发生变化的时候，通知 `View` 层的更新，这样 `View` 层和 `Model` 层耦合在一起，当项目逻辑变得复杂的时候，可能会造成代码的混乱，并且可能会对代码的复用性造成一些问题，在 `MVP` 的模式中是通过使用 `Presenter` 来实现对 `View` 层和 `Model` 层的解耦，**`MVC` 中的 `Controller` 只知道 `Model` 的接口，因此它没有办法控制 `View` 层的更新**，而 **`MVP` 模式中，`View` 层的接口也暴露给了 `Presenter`，因此可以在 `Presenter` 中将 `Model` 的变化和 `View` 的变化绑定在一起，以此来实现 `View` 和 `Model` 的同步更新**，`Presenter` 还包含了其他的响应逻辑 |
| <img src="images/MVC.png" alt="MVC" style="zoom:50%;" />     |                                                              |

#### *关于 import 引入问题*

如果暴露成员使用 `export default` ，那么说明这个模块只暴露一个成员，在组件中引入是可以不用解构。

如果定义成员时使用 `export` ，那么说明这个模块会暴露多个成员，在引入时需要进行解构。

#### *data 为什么是一个函数而不是对象？*

> 以下为 `Vue2` 官方文档说明

当一个组件被定义，`data` 必须声明为返回一个初始数据对象的函数，**因为组件可能被用来创建多个实例，如果 `data` 仍然是一个纯粹的对象，则所有的实例将共享引用同一个数据对象。**

<u>✔解决方案：</u>通过提供 `data` 函数，每次创建一个新实例后，能够调用 `data` 函数返回初始数据的一个全新副本数据对象。

#### *Vue 模板编译的原理是什么？*

> `Vue` 中的模板 `template` 无法被浏览器解析并渲染，因为这不属于浏览器的标准，不是正确的 `HTML` 语法，所以需要**将 `template` 模板转化成一个 `JavaScript` 函数，这样浏览器就可以执行这一个函数并渲染出对应的 `HTML` 元素，就可以让视图跑起来**，这一个转化的过程就是模板编译

模板编译又分三个阶段，分别为解析 `parse`，优化 `optimize`，生成 `generate`：

- **解析**阶段：对模板 `template` 进行解析，将标签、指令、属性等**转化为抽象语法树 `AST`**

- **优化**阶段：遍历 `AST`，**找出其中的静态节点并进行标记**，方便在页面重渲染的时候进行 `diff` 比较时，**直接跳过这一些静态节点，减少渲染过程中的重复计算**，优化 `runtime` 的性能

- **生成**阶段：将最终的 `AST` **转化为 `render` 函数字符串**

🧱最终生成可执行的渲染函数 `render`，它会返回虚拟 `DOM` 节点，用于渲染真实的 `DOM`。

#### *什么是虚拟 DOM ?*

> `HTML` 渲染出来的真实 `DOM` 是个非常复杂的树形结构，每个标签都只是树的某个节点，并且属性也非常多，其中无用的属性居多，**虚拟 `DOM` 则可以用最少的属性结构来描述真实的 `DOM` ，🛠本质上是使用一个个保存标签信息的 `JS` 对象将其表示出来**

当页面的状态发生改变，需要对页面的 `DOM` 结构进行调整的时候，首先根据变更的状态重新构建起新的对象，然后将这新的对象和旧的对象进行比较，记录下两者的差异，最后将记录有差异的地方应用到真正的 `DOM` 树中去，这样视图就会进行更新。

真实 `DOM` 如下：

```html
<ul id="list">
	<li class="item">哈哈</li>
	<li class="item">呵呵</li>
	<li class="item">嘿嘿</li>
</ul>
```

对应的虚拟 `DOM` 为：

```js
let vDOM = {
	tagName: 'ul', // 标签名
	props: {
		id: 'list'
	}, // 标签属性
	children: [ 
		{ tagName: 'li', props: { class: 'item' }, children: ['哈哈'] },
		{ tagName: 'li', props: { class: 'item' }, children: ['呵呵'] },
		{ tagName: 'li', props: { class: 'item' }, children: ['嘿嘿'] },
	] // 标签子节点
}
```

因此在一些比较复杂的 `DOM` 变化场景中，**通过对比虚拟 `DOM` 再更新真实 `DOM` 会比直接更新真实 `DOM` 的效率更高。**

#### *Vue 中的 key 到底有什么作用？*

> 给 `Vue` 中的虚拟 `DOM` 节点（`vNode`）添加一个唯⼀标识，可以更好地区别各个元素，它的作用⚙是**优化对比复用策略，提高渲染性能**

![key的作用](images/key的作用.png)

#### *什么是 diff 算法？*

> [参考文章：15张图，20分钟吃透Diff算法核心原理，我说的！！！](https://juejin.cn/post/6994959998283907102)

> `diff` 算法可以看作是一种**对比算法**，对比的对象是新旧虚拟 `DOM`。顾名思义，**`diff` 算法可以找到新旧虚拟`DOM` 之间的差异，**但 `diff` 算法中其实并不是只有对比虚拟 `DOM`，还有**根据对比后的结果精准地更新真实 `DOM`** ，进而提高效率
>
> 在新旧虚拟 `DOM` 对比的时候，**`diff` 算法比较只会在同层级进行**，不会跨层级比较，所以 `diff` 算法是**深度优先算法**

`diff` 算法比较新旧虚拟 `DOM` 的**原理如下🛠**：

> 其中 `newVnode` 和 `oldVnode` 为同层的新旧虚拟节点

![diff算法](images/diff算法.png)

首先是在调用 `patch` 方法中通过 `sameVnode` 方法来对比当前同层的虚拟节点是否为同一种类型的节点（通过比较 `key` 值、标签名等），**如果不是相同节点，则没必要比对，直接将整个节点替换成新虚拟节点（即推倒重建），而如果是相同节点，则继续执行 `patchVnode` 方法进行深层比对**，分以下几种情况：

- 如果新旧虚拟节点是同一个对象，则终止执行
- 如果新旧虚拟节点是文本节点，且文本不一样，则直接将真实 `DOM` 中的文本更新为新虚拟节点的文本
- **旧虚拟节点有子节点**，新虚拟节点没有，则直接销毁删除真实 `DOM` 里对应的子节点
- **新虚拟节点有子节点**，旧虚拟节点没有，则创建新虚拟节点的子节点，并更新到真实 `DOM` 上去
- **新旧虚拟节点都有子节点**，且子节点有不一样，则执行 `updateChildren ` 方法来递归对比子节点，对相同的子节点复用或更新，对不相同的子节点则是创建或直接销毁（**算法核心🔥**）

#### *什么是单向数据流？*

> 以下为 `Vue2` 官方文档说明

所有 `prop` 都使得其父子 `prop` 之间形成了一个单向下行绑定：**父级 `prop` 的更新会向下流动到子组件中，但是反过来则不行**，这样可防止从子组件意外变更父级组件的状态，从而导致你的应用的数据流向难以理解。

每次父级组件发生变更时，子组件中所有的 `prop` 都将会刷新为最新的值，这意味着你不应该在一个子组件内部改变 `prop`，如果你这样做了，`Vue` 会在浏览器的控制台中发出警告。

📣两种常见的试图变更一个 `prop` 的情形：

（1）**作为一个 `data` 数据来使用**

```js
props: ['initialCounter'],
data() {
  return {
    counter: this.initialCounter
  }
}
```

（2）**以原始值传入且通过计算属性 `computed` 进行转换**

```js
props: ['size'],
computed: {
  normalizedSize() {
    return this.size.trim().toLowerCase()
  }
}
```

⚠注意：**在 `JavaScript` 中对象和数组是通过引用传入的，所以对于一个数组或对象类型的 `prop` 来说，在子组件中改变变更这个对象或数组本身将会影响到父组件的状态**

#### *简述 Vue 的双向数据绑定或响应式原理*

> `Vue` 是采用**数据劫持**结合**发布者-订阅者模式/观察者模式**的方式，通过 `Object.defineProperty()` 来劫持对象各个属性的 `setter`，`getter` 存取器，在数据变动时发布消息给订阅者，触发相应的监听回调，对关联的组件重新渲染更新

![Vue的双向数据绑定](images/Vue的双向数据绑定.png)

主要分为以下几个步骤：

（1）对需要 `observe` 的数据对象进行递归遍历，包括子属性对象的属性，都加上 `setter` 和 `getter`，给这个对象的某个值赋值，就会触发 `setter`，那么就能监听到数据变化

（2）`compile` 解析模板指令，将模板中的变量替换成数据，然后初始化渲染视图，并将每个指令对应的节点绑定更新函数，添加监听数据的订阅者，一旦数据有变动，收到通知，更新视图

（3）订阅者 `Watcher` 是 `Observer` 和 `Compile` 之间通信的桥梁，主要做的事情是：首先在自身实例化时往属性订阅器 `dep` 里面添加自己，而且自身必须有一个 `update()` 方法，等待属性变动 `dep.notice()` 通知时，能调用自身的 `update()` 方法，并触发 `Compile` 中绑定的回调，则功成身退

（4）`MVVM` 作为数据绑定的入口，整合 `Observer`、`Compile` 和 `Watcher` 三者，通过 `Observer` 来监听自己的 `model` 数据变化，通过 `Compile` 来解析编译模板指令，最终利用 `Watcher` 搭起 `Observer` 和 `Compile` 之间的通信桥梁，达到数据变化 -> 视图更新，视图交互变化（`input`）-> 数据 `model` 变更的双向绑定效果

#### *Object.defineProperty 和 Proxy 的优缺点*

> `Proxy` 实现的响应式原理与 `Vue2` 的实现原理相同，实现方式大同小异

|                   `Object.defineProperty`                    |                           `Proxy`                            |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| （1）`Object.defineProperty()` **不能动态监测到对象属性的添加或删除，以及数组下标和长度的变化**（可以通过使用 `$set`、`$delete` 解决）<br />（2）兼容性较好（不支持 `IE8` 以及更低版本浏览器） | （1）**可以直接监听整个对象，⽽⾮是对象的某个属性**<br />（2）**可以直接监听数组的变化**<br />（3）拦截⽅法丰富：多达 `13` 种，不限于`get`、`set`、`deleteProperty`、`has` 等，比`Object.defineProperty` 强大很多，可在访问或修改原始对象上的属性时进行拦截，不需用使用 `$set` 或 `$delete` 触发响应式<br />（4）支持 `Map`，`Set`，`WeakMap` 和 `WeakSet`<br />（5）兼容性较差 |


#### *父子组件的生命周期执行顺序*

- **加载渲染**过程

```vue
父beforeCreate->父created->父beforeMount->子beforeCreate->子created->子beforeMount->子mounted->父mounted
```

- **子组件更新**过程

```vue
父beforeUpdate->子beforeUpdate->子updated->父updated
```

- **父组件更新**过程

```vue
父beforeUpdate->父updated
```

- **销毁**过程

```vue
父beforeDestroy->子beforeDestroy->子destroyed->父destroyed
```

#### *Vue 组件之间如何进行通信？*

（1）`props` 和 `$emit`

（2）事件总线 `eventBus`

（3）`$children`、`$parent` 和 `$refs`

（4）`Vuex`

（5）`provide` 和 `inject`（依赖注入）

> `provide` 和 `inject`一起使用，**允许一个祖先组件向其所有子孙后代注入依赖**，不论组件层次有多深，并在其上下游关系成立的时间里始终生效，其中：
>
> - `provide` 选项是一个**返回对象的函数**，允许指定想要提供给后代组件的数据、方法
> - `inject` 选项是一个**字符串数组**，接收指定想要添加在这个实例上的数据、方法

- **祖先**组件

```js
export default {
	provide() {
		return {
			value: this.value, // 共享给子孙组件的数据
			getMap: this.getMap
		}
	},
	data() {
		return {
			value: '祖先组件的数据'
		}
	},
	methods: {
		getMap() {
			// ...
		}
	}
}
```

- **子孙**组件

```js
export default {
	inject: ['value', 'getMap']
}
```

（6）`$attrs` 和 `$listeners`

>  `$attrs` 和 `$listeners` 可以用来作为**跨级组件之间的通信机制 (父传孙)**，⚠需要注意子孙组件的 `inheritAttrs` 属性要设置为 `false`

- **父**组件

```html
<template>
  <div>
    <my-child1 :money="100" desc="你好哇" @test1="fn1" @test2="fn2"></my-child1>
  </div>
</template>
```

- **子**组件

```vue
<template>
  <div class="my-child1">
    <!-- $attrs => { "money": 100, "desc": "你好哇" } -->
    <my-child2 v-bind="$attrs" v-on="$listeners" ></my-child2>
  </div>
</template>

<script>
import MyChild2 from './my-child2'
export default {
  inheritAttrs: false,
  created () {
    console.log(this.$listeners)
  },
  components: {
    MyChild2
  }
}
</script>
```

![$listeners](images/$listeners.png)

- **孙**组件

```vue
<template>
  <div>
    我是child2 - {{ money }} - {{ desc }}
    <button @click="clickFn">按钮</button>
  </div>
</template>

<script>
export default {
  inheritAttrs: false,
  props: ['money', 'desc'],
  methods: {
    clickFn () {
      this.$emit('test1', '嘎嘎')
      this.$emit('test2', '嘿嘿')
    }
  }
}
</script>
```

![$attrs和$listeners](images/$attrs和$listeners.png)

#### *delete 和 Vue.delete 删除的区别是什么？*

> `delete` 和 `Vue.delete` 都是对数组或对象进行删除的方法

对于对象来说其实是没有区别的，使用方法会直接删除对象的属性（物理删除）：

```js
let obj = {
	name: 'fufu',
  age: 20
}
// delete obj.age  => {name: 'fufu'}
// Vue.delete(obj, 'age') => {name: 'fufu'}
```

但是这**两种方法对于数组来说是有区别🔎**的：

```js
let arr = [1, 2, 3, 4, 5]
delete arr[2]  // [1, 2, empty, 4, 5]
Vue.delete arr[2]  // [1, 2, 4, 5]
```

**`delete` 只是将被删除的元素变成了 `empty`/`undefined`，其他元素的键值还是不变，数组长度也不变（逻辑删除），而 `Vue.delete` 是直接删除该元素，数组长度发生变化（物理删除）。**

#### *文件夹 assets 和 static 的异同点是什么？*

|                            相同点                            |                            差异点                            |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| `assets` 和 `static` 两个**都是存放静态资源文件**，项目中所需要的资源文件图片，字体图标，样式文件等都可以放在这两个文件夹里 | `assets` 中存放的静态资源文件在项目打包时，也就是**运行 `npm run build` 时会将 `assets` 中放置的静态资源文件进行打包上传**，所谓打包简单点可以理解为压缩体积，代码格式化，而**压缩后的静态资源文件最终也会放置在 `static` 文件中跟着 `index.html` 一同上传至服务器**，**`static` 中放置的静态资源文件就不会要走打包、压缩、格式化等流程，而是直接进入打包好的目录，直接上传至服务器**，因为避免了压缩直接进行上传，在打包时会提高一定的效率，但是 `static` 中的资源文件由于没有进行压缩等操作，所以文件的体积也就相对于 `assets` 中打包后的文件提交较大点，在服务器中就会占据更大的空间 |

📣建议：将项目中 `template` 需要的 `CSS` 文件、 `JS` 文件等都可以放置在 `assets` 中，走打包这一流程来减少体积，而项目中引入的第三方资源文件可以放置在 `static` 中，如 `iconfont.css` 等文件，因为这些引入的第三方资源文件已经经过处理，不再需要处理，可以直接上传。

#### Vue 项目支付功能实现

- 支付宝支付

> 点击支付宝支付,  携带订单号调用后台接口，后台返回一个 `form` 表单(`HTML` 字符串结构)，提交 `form` 表单就可以调用支付宝支付

```html
<!-- alipayWap: 后台接口返回的form片段 -->
<div v-html="alipayWap" ref="alipayWap"></div>
```

```js
methods: {
	toAlipay () {
		this.$axios.get('xxx').then(res => {
			this.alipayWap = res;
      // 等待DOM更新, 等页面中有这个form表单
			this.$nextTick(() => {
      	this.$refs.alipayWap.children[0].submit()
      })
		})
	}
}
```

![支付宝支付](images/支付宝支付.png)

- 微信支付

> 需要自己根据后台返回的 `url` 生成二维码页面
>
> 博客参考1： https://blog.csdn.net/qq_36710522/article/details/90480914
>
> 博客参考2： https://blog.csdn.net/zyg1515330502/article/details/94737044

![微信支付](images/微信支付.png)



------



## Vue3.x

> 🔗`Vue3` 官方中文文档：https://cn.vuejs.org/

> `Vue3` 现已成为默认版本，其动机和目的是：
>
> 1. **更好的逻辑复用与代码组织**
>    `composition` 组合式 `API` 取代 `options` 选项式 `API`，使代码量变少，**分散式维护转为集中式维护**
> 2. **更好的类型推导**
>    `Vue3` 源码使用 `TypeScript` 重写，代码对 `TypeScript` 的支持更友好，`TypeScript` 类型检测可以使代码更加稳定

- `Vue3` 的**新特性**🎏

![Vue3的优势](images/Vue3的优势.png)

|               `Vue3` 与 `Vue2` 的主要**区别**                |
| :----------------------------------------------------------: |
| **数据响应式原理重新实现，`ES6` 的 `proxy` 替代了 `ES5` 的 `Object.defineProperty`**，可以直接对整个对象进行劫持，**✔解决了 `Vue2` 中的响应式问题，大大优化了响应式监听的性能** |
| **提供了 `composition` 组合式 `API`**，可以更好的**逻辑复用，利于维护** |
| **源码用 `TS` 重写，有更好的类型推导，类型检测更为严格，代码更稳定** |

- `Options ` 和 `Composition`

![options VS composition](images/options VS composition.webp)

`Vue3` 提供[两种组织代码](https://cn.vuejs.org/guide/introduction.html#api-styles)逻辑的写法✏：

1. 通过`data`、`methods`、`watch` 等配置选项组织代码逻辑是选项式 `API` 写法
2. 所有逻辑在 `setup` 函数中，使用 `ref`、`watch` 等函数组织代码是组合式 `API` 写法（**推荐👍**）

🌰举例说明，准备了两份代码实现同一功能，两者做对比：

![组合式API介绍](images/组合式API介绍.png)

（1）`Options API`

```vue
<template>
  <button @click="toggle">显示隐藏图片</button>
  <img v-show="show" alt="Vue logo" src="./assets/logo.png" />
  <hr />
  计数器：{{ count }} <button @click="increment">累加</button>
</template>
<script>
export default {
  data() {
    return {
      show: true,
      count: 0,
    };
  },
  methods: {
    toggle() {
      this.show = !this.show;
    },
    increment() {
      this.count++;
    },
  },
};
</script>
```

（2）`Composition API`

```vue
<template>
  <button @click="toggle">显示隐藏图片</button>
  <img v-show="show" alt="Vue logo" src="./assets/logo.png" />
  <hr />
  计数器：{{ count }} <button @click="increment">累加</button>
</template>
<script>
import { ref } from 'vue';
// 抽离业务，封装功能模块（组合式函数写法，便于逻辑复用，以“use”开头命名）
const useCount = () => {
	const count = ref(0)
	const increment = () => {
		count.value++
	}
  return { count, increment }
}
  
export default {
  setup () {
    // 显示隐藏
    const show = ref(true)
    const toggle = () => {
      show.value = !show.value
    }
    // 计数器模块
		const { count, increment } = useCount()
    
    return { show, toggle, count, increment }
  }
};
</script>
```



### setup 函数

> `setup` 函数是 `Vue3` 特有的选项，作为组合式 `API` 的入口
>
> 从生命周期来看，**`setup` 会在 `beforeCreate` 钩子函数之前执行**，此时组件实例还没创建，**`setup` 中不能使用 `this`，`this` 不是组件实例，指向 `undefined`**
>
> ⚠注意：**如果数据或者函数在模板中使用，需要在 `setup` 中 `return` 返回**
>
> 🧩扩展：今后在 `Vue3` 的项目中几乎用不到 `this`，**所有的东西都需通过函数获取**

![setup钩子的生命周期](images/setup钩子的生命周期.png)

```vue
<script>
import { ref } from 'vue'

export default {
  setup() {
    const count = ref(0)
    // 返回值会暴露给模板和组件实例（其他的选项式API钩子）
    return { count }
  },
	// 以下vue2代码不建议写，vue3中的所有业务逻辑都写在setup里，不与vue2语法混合使用
  beforeCreate() {},
  mounted() {
    // 其他的选项也可以通过组件实例来获取setup()暴露的属性
    console.log(this.count) // 0
  }
}
</script>

<template>
  <button @click="count++">{{ count }}</button>
</template>
```



### setup 语法糖

> 对于使用的组合式 `API`，**👍推荐通过 `<script setup>` 以获得更加简洁的代码**
>
> 相比于普通的 `<script>` 语法，它的**不同点**在于：
>
> - 不需要 `export default {}`
> - 不需要 `setup()` 以及 `return` 数据
> - **任何在 `<script setup>` 声明的顶层绑定 (包括变量、函数声明，以及 `import` 导入的内容) 都能在同一组件的模板中直接使用**

```vue
<script setup>
import { capitalize } from './helpers'
import MyComponent from './MyComponent.vue'
// 默认setup中的普通变量不是响应式的，需要通过reactive、ref函数转换成响应式数据
const msg = 'Hello!'
const log = () => {
  console.log(msg)
}
</script>

<template>
	<!-- vue3中可以有多个根元素 -->
  <button @click="log">{{ msg }}</button>
	<div>{{ capitalize('hello') }}</div>
	<MyComponent />
</template>
```

**如果想要使用 `TypeScript`，在 `<script>` 上添加 `lang="ts"`**：

```html
<script setup lang="ts"></script>
```



### reactive 函数

> `reactive()` 函数的作用⚙是**将传入的对象类型数据转换成响应式数据，并会返回该对象的响应式代理**
>
> 📍语法：`const 变量 = reactive(...)`

```vue
<script setup>
import { reactive } from 'vue'

const state = reactive({ count: 0 })
const increment = () => {
  state.count++
}
</script>

<template>
  <button @click="increment">{{ state.count }}</button>
</template>
```

⚠值得注意的是，**`reactive()` 返回的是一个原始对象的 `Proxy` 代理对象，返回的对象以及其中嵌套的对象都会通过 `Proxy` 包裹，因此不等于原始对象，👍建议只使用响应式代理，避免使用原始对象。**

```js
const raw = {}
const proxy = reactive(raw)

// 代理对象和原始对象不是全等的
console.log(proxy === raw) // false
```

为保证访问代理的一致性，对同一个原始对象调用 `reactive()` 会总是返回同样的代理对象，而对一个已存在的代理对象调用 `reactive()` 会返回其本身。

```js
// 在同一个对象上调用reactive()会返回相同的代理
console.log(reactive(raw) === proxy) // true

// 在一个代理上调用reactive()会返回它自己
console.log(reactive(proxy) === proxy) // true
```

#### reactive 局限性

> 以下是 `Vue3` 官方文档说明

`reactive()` 函数有两条限制🚨：

（1）**仅对对象类型有效（对象、数组和 `Map`、`Set` 这样的集合类型）**，而对 `string`、`number` 和 `boolean` 这样的简单类型无效

（2）因为 `Vue` 的响应式系统是通过属性访问进行追踪的，因此我们必须始终保持对该响应式对象的相同引用，这意味着我们**不可以随意地替换一个响应式对象**，因为这将**导致对初始引用的响应性连接丢失**，比如：

```js
let state = reactive({ count: 0 })
// 上面的引用({ count: 0 })将不再被追踪（响应性连接已丢失！）
state = reactive({ count: 1 })
```

同时这也意味着当**将响应式对象的属性赋值或解构至本地变量时，或是将该属性传入一个函数时，也会失去响应性**：

```js
const state = reactive({ count: 0 })

// n是一个局部变量，和state.count失去响应性连接，不影响原始的state
let n = state.count
n++

// count也和state.count失去了响应性连接，不会影响原始的state
let { count } = state
count++

// 该函数接收一个普通数字，并且将无法跟踪state.count的变化
callSomeFunction(state.count)
```



### 深层响应性

> **在 `Vue` 中，状态都是默认深层响应式的**，这意味着即使在更改深层次的对象或数组，任何改动也能被检测到

```js
import { reactive } from 'vue'

const obj = reactive({
  nested: { count: 0 },
  arr: ['foo', 'bar']
})

const mutateDeeply = () => {
  // 以下都会按照期望工作
  obj.nested.count++
  obj.arr.push('baz')
}
```



### ref 函数

> `reactive()` 函数处理的数据必须是对象类型，如果是简单类型就无法转换成响应式，此时需要使用 `ref()` 函数，其作用⚙是**对将传入的数据（简单类型、复杂类型均支持）转换成响应式数据，并返回一个带 `value` 属性的响应式、可更改的 `ref` 对象，当值为对象类型时，会用 `reactive()` 函数自动转换它的 `.value`**
>
> 所谓可更改，就是说可为 `.value` 赋予新的值也是响应式的，即所有对 `.value` 的操作都将被追踪
>
> 📍语法：`const 变量 = ref(...)`
>
> ⚠注意：**在 `<script>` 中需要通过 `value` 属性访问或修改 `ref` 对象的值，而在模板中 `ref` 对象会自动解包，不需要额外的 `value` 属性**

```js
const count = ref(0)
console.log(count) // { value: 0 }
console.log(count.value) // 0

count.value++
console.log(count.value) // 1
```

**一个包含对象类型值的 `ref` 可以响应式地替换整个对象，不会丢失响应性。**

```js
const objectRef = ref({ count: 0 })
// 响应式替换
objectRef.value = { count: 1 }
```

**`ref` 被传递给函数或是从一般对象上被解构时，不会丢失响应性。**

```js
const obj = {
  foo: ref(1),
  bar: ref(2)
}
// 仍然是响应式的
const { foo, bar } = obj

// 该函数接收一个ref，需要通过.value取值，但它会保持响应性
callSomeFunction(obj.foo)
```

简言之，`ref()` 能创建一种对任意值的引用，并能够在不丢失响应性的前提下传递这些引用。

#### ref 在模板中的解包

> **仅当 `ref` 在模板中作为顶层属性被访问时，才会被自动解包**，所以不需要使用 `.value`

```vue
<script setup>
import { ref } from 'vue'

const count = ref(0)
const increment = () => {
  count.value++
}
</script>

<template>
	<!-- 无需.value取值 -->
  <button @click="increment">{{ count }}</button>
</template>
```

🌰例如 `object` 是顶层属性，但 `object.foo` 不是，将不会像预期的那样工作，因为 `object.foo` 是一个 `ref` 对象，渲染结果会是 `[object Object]1`。

```js
const object = { foo: ref(1) }
```

```html
<div>{{ object.foo + 1 }}</div>
```

可以通过将 `foo` 改成顶层属性来解决这个问题，现在的渲染结果将是 `2`。

```js
const object = { foo: ref(1) }
const { foo } = object
```

```html
<div>{{ foo + 1 }}</div>
```

如果一个 `ref` 是文本插值（`{{}}`）计算的最终值，它也将被解包，因此相当于 `{{ object.foo.value }}`，下面的渲染结果将为 `1`。

```html
<div>{{ object.foo }}</div>
```

#### ref 在响应式对象中的解包

> **只有当嵌套在一个深层响应式对象内，作为属性被访问或更改时才会发生 `ref` 解包**，因此会表现得和一般的属性一样，**但当其作为浅层响应式对象的属性被访问时不会解包**

```js
const count = ref(1)
const obj = reactive({ count })

// ref会被解包
console.log(obj.count === count.value) // true

// 会更新obj.count
count.value++
console.log(count.value) // 2
console.log(obj.count) // 2

// 也会更新count
obj.count++
console.log(obj.count) // 3
console.log(count.value) // 3
```

如果将一个新的 `ref` 赋值给一个关联了已有 `ref` 的属性，那么它会替换掉旧的 `ref`。

```js
const otherCount = ref(2)
obj.count = otherCount

console.log(obj.count) // 2
// 原始ref已经和state.count失去联系
console.log(count.value) // 1
```

#### ref 在数组和集合类型中的解包

> 跟响应式对象不同，**当 `ref` 作为响应式数组或像 `Map` 这种原生集合类型的元素被访问时，不会进行解包**

```js
const books = reactive([ref('Vue 3 Guide')])
console.log(books[0].value)

const map = reactive(new Map([['count', ref(0)]]))
console.log(map.get('count').value)
```

#### reactive 与 ref 的选择

|                          `reactive`                          |                            `ref`                             |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| 可以转换对象类型数据成为响应式对象，但是**不支持简单类型数据** | 可以**支持简单类型数据**转换为响应式数据，**也支持复杂类型数据**，但是**操作的时候需要 `.value`**，`ref` 函数**内部的实现依赖于 `reactive` 函数** |

👍**尽量使用 `ref` 函数支持所有场景**，确定属性的对象使用 `reactive` 可以省去`.value`。



### toRefs 函数

> `toRefs()` 函数可以**将一个响应式对象转换为一个普通对象**，这个普通对象的**每个属性都是指向源对象相应属性的 `ref`**，每个单独的 `ref` 都是使用 `toRef()` 函数创建的，简单来说就是**对对象中的每一个属性做一次包装处理，使其成为响应式数据**
>
> 📍语法：`const 变量 = toRefs(响应式对象)`
>
> ⚠注意：
>
> - `toRefs()` 在调用时只会为源对象上的可枚举属性创建 `ref`，**如果要为可能还不存在的属性创建 `ref`，请改用 `toRef()`**
> - **解构或展开一个通过 `reactive()` 或 `ref()` 函数创建的响应式对象，会使它的数据丢失响应式特性**（原因是 `Vue3` 底层是对整个对象进行监听劫持），**使用 `toRefs()` 可以保证对象解构或展开的每个属性和方法都是响应式的**（📣常见的使用场景）

```vue
<script setup>
import { ref, toRefs } from "vue";

const user = ref({ name: "tom", age: 18 });
// 解构ref或reactive，会导致响应式丢失
// let { name, age } = user.value;
let { name, age } = toRefs(user.value);
const addAge = () => {
  // age += 1;
  // age.value += 1;
  user.value.age += 1;
  console.log("user.age", user.value.age);
  // console.log("age", age);
  console.log("age", age.value);
};
</script>

<template>
  <div>
    <p>{{ user }}</p>
    <p>姓名：{{ name }}</p>
    <p>年龄：{{ age }} <button @click="addAge">一年又一年</button></p>
  </div>
</template>
```



### computed 函数

> `computed` 计算属性的基本思想和 `Vue2` 的完全一致，组合式 `API` 下的计算属性只是修改了写法
>
> 📍语法：`const 变量 = computed(() => 返回一个新的值)`
>
> `computed()` 函数接收一个 `getter` 回调函数，**返回一个只读的响应式 `ref` 对象**，和其他一般的 `ref` 类似，**需要通过 `.value` 访问计算结果，也会在模板中自动解包**，因此在模板中引用时无需添加 `.value`
>
> ⚠注意：**`getter` 回调函数一定要有 `return` 返回值**

```vue
<script setup>
import { ref, computed } from 'vue'

const count = ref(0)
const doubleCount = computed(() => count.value * 2)

const list = ref([1, 2, 3, 4, 5, 6, 7, 8])
const filterList = computed(() => list.value.filter(item => item > 3))
</script>
```

**`computed()` 也可以接受一个带有 `get` 和 `set` 函数的对象来创建一个可读写的 `ref` 对象**：

```vue
<script setup>
import { ref, computed } from 'vue'

const firstName = ref('John')
const lastName = ref('Doe')
const fullName = computed({
  get: () => firstName.value + ' ' + lastName.value,
  set: (val) => {
    [firstName.value, lastName.value] = val.split(' ')
  }
})
</script>
```



### watch 函数

> `watch` 侦听器的基本思想和 `Vue2` 的完全一致，组合式 `API` 下的侦听器也只是修改了写法
>
> 📍语法：`watch(数据源, (newVal, oldVal) => { 业务逻辑 }, { 额外配置选项 })`
>
> `watch` 可以传入不同形式的数据源：它可以是一个 `ref` (包括计算属性)、一个响应式对象、一个 `getter` 函数、或多个数据源组成的数组，当侦听多个来源时，回调函数接受两个数组，分别对应来源数组中的新值和旧值
>
> 🧰额外配置选项是可选的，支持以下几种选项：
>
> - **`immediate`**：在侦听器创建时立即执行一遍回调，第一次调用时旧值是 `undefined`
> - **`deep`**：**如果源是对象，强制深度遍历，以便在深层级变更时触发回调**

- 侦听**单个数据**的变化

```vue
<script setup>
import { ref, watch } from 'vue'
  
const count = ref(0)
watch(count, (newVal, oldVal) => {
  console.log(`count发生变化，老值为${oldVal}，新值为${newVal}`)
})
</script>
```

- 侦听**多个数据**的变化

> 第一个参数需改写成**数组**写法

```vue
<script setup>
import { ref, watch } from 'vue'
  
const count = ref(0)
const name = ref('小明')
watch([count, name], ([newCount, newName], [oldCount,oldName]) => {
  console.log('count或者name发生变化', [newCount, newName], [oldCount, oldName])
})
</script>
```

- 侦听**对象**的变化

> 💡技巧：**不管是 `reactive`，还是 `ref`，或者是否嵌套对象，侦听对象的变化可以直接开启深度侦听**

> 直接给 `watch()` 传入一个响应式对象，会**隐式地创建一个深层侦听器**，该回调函数在所有嵌套的变更时都会被触发

```vue
<script setup>
import { reactive, watch } from 'vue'
  
const obj = reactive({ count: 0 })
watch(obj, (newVal, oldVal) => {
	// 在嵌套的属性变更时触发
	// 注意：因为它们是同一个对象，newVal此处和oldVal是相等的
  console.log('数据发生变化', newVal, oldVal)
})
obj.count++
</script>
```

> **通过 `watch` 监听的 `ref` 对象默认是浅层侦听的，直接修改嵌套的对象属性不会触发回调执行**，需要开启 `deep` 选项，强制转成深层侦听器

```vue
<script setup>
import { ref, watch } from 'vue'
  
const obj = ref({ count: 0 })
watch(obj, () => {
  console.log('数据发生变化')
})
const changeStateByCount = () => {
  // 直接修改不会引发回调执行
  obj.value.count++
}
</script>
```

```vue
<script setup>
import { ref, watch } from 'vue'
  
const obj = ref({ count: 0 })
watch(obj, () => {
  console.log('数据发生变化')
}, { deep: true })
const changeStateByCount = () => {
  // 此时修改可以触发回调
  obj.value.count++
}
</script>
```

- 侦听**对象某个属性**的变化

> 通过传入一个 `getter` 函数并返回对象某个属性，即可侦听对象某个属性的变化，**当要侦听对象某一个复杂类型的属性时，需配置深度侦听**
>
> ⚠注意：**`Vue3` 不支持直接侦听响应式对象的属性值**

```vue
<script setup>
import { ref, watch } from 'vue'

const user = ref({
  name: 'jack',
  info: {
    gender: '男',
    age: 18,
  }
})
watch(() => user.value.name, (newVal, oldVal) => {
  console.log('对象的name属性发生变化', newVal, oldVal);
})
watch(() => user.value.info, (newVal, oldVal) => {
  // 注意：newVal此处和oldVal是相等的
  console.log('对象的info属性发生变化', newVal, oldVal);
}, { deep: true })
</script>
```



### 生命周期函数

> `Vue3` 的生命周期钩子函数**以 `on` 开头**，生命周期函数是可以执行多次的，**当多次执行时，传入的回调会在时机成熟时依次执行**，业务逻辑可以写在多个生命周期函数中执行，**而在 `Vue2` 中需要写同一个生命周期函数中，不可多次执行，前面的生命周期函数会被后面的覆盖**
>
> 📍语法：`on生命周期钩子函数(() => {...})`

```vue
<script setup>
import { onMounted } from "vue";
onMounted(()=>{
  console.log('onMounted1触发')
})
  
onMounted(()=>{
  console.log('onMounted2触发')
})
</script>
```

- **生命周期图示**

<img src="images/Vue3的生命周期图示.png" alt="Vue3的生命周期图示" style="zoom:50%;" />

- **`Vue3` 和 `Vue2` 的生命周期对比**

> 除销毁时的生命周期函数名称不同之外，其余均相似

|      选项式 `API`      |           组合式 `API`            |
| :--------------------: | :-------------------------------: |
|     `beforeCreate`     | 不需要（直接写到 `setup` 函数中） |
|       `created`        | 不需要（直接写到 `setup` 函数中） |
|     `beforeMount`      |          `onBeforeMount`          |
|       `mounted`        |            `onMounted`            |
|     `beforeUpdate`     |         `onBeforeUpdate`          |
|       `updated`        |            `onUpdated`            |
| 🚨**`beforeDestroyed`** |      🚨**`onBeforeUnmount`**       |
|    🚨**`destroyed`**    |        🚨**`onUnmounted`**         |
|      `activated`       |           `onActivated`           |
|     `deactivated`      |          `onDeactivated`          |



### 组件通信

#### defineProps (父传子)

> `defineProps()` 是一个仅 `<script setup>` 中可用的编译宏命令，并**不需要显式地导入**
>
> 📍语法：`const props = defineProps({...})`
>
> `defineProps()` 接收与 `Vue2` 中的 `props` 选项相同的值（**数组或对象**），类型、默认值等写法相同，而且 **`defineProps` 会返回一个对象，其中包含了可以传递给组件的所有 `props`，声明的 `props` 会自动暴露给模板，在 `<script setup>` 中，需要通过 `defineProps` 函数返回的对象来获取相应的 `prop` 属性（即`props.xxx`）**
>
> 👍官方推荐写法： **声明时用小驼峰命名法，使用时用短横线命名法**
>
> ⚠注意：
>
> - `defineProps()` 宏中的参数**不可以访问 `<script setup>` 中定义的其他变量**，因为在编译时整个表达式都会被移到外部的函数中
> - 如果解构 `props` 对象，**解构出的变量将会丢失响应性**，因此推荐通过 `props.xxx` 的形式来使用其中的 `prop`，**当确实需要解构 `props` 对象，请使用 `toRefs()` 或 `toRef()` 函数保持响应性**

如果没有使用 `<script setup>`，必须以 `props` 选项的方式声明，`props` 对象会作为 `setup()` 函数的第一个参数被传入：

```js
export default {
  props: ['title'],
  setup(props) {
    console.log(props.title)
  }
}
```

- **父传子实现步骤**

（1）父组件中给子组件绑定属性

```vue
<script setup>
import { ref } from 'vue'
import MyChild from './components/MyChild.vue'

const money = ref(10000)
const car = ref('玛莎拉蒂')
</script>

<template>
  <div>
    <h1>我是父组件</h1>
    <div>金钱：{{ money }}</div>
    <div>车辆：{{ car }}</div>
    <hr />
    <MyChild :money="money" :car="car" />
  </div>
</template>
```

（2）子组件内部通过 **`defineProps`** 函数来接收数据

```vue
<script setup>
// const props = defineProps(["money", "car"])
const props = defineProps({
	money: Number,
	car: {
		type: String,
		default: '宝马'
	}
})
console.log(props.car)
</script>

<template>
  <div>
    <h3>我是子组件</h3>
    <div>金钱：{{ money }} <br> 跑车：{{ car }}</div>
    <button>吞金兽想花钱</button>
  </div>
</template>
```

- **两种常见的试图变更一个 `prop` 的情形**

> 以下是 `Vue3` 官方文档说明

（1）`prop` 被用于传入初始值，而子组件想在之后将其作为一个局部数据属性，在这种情况下，最好是新定义一个局部数据属性，从 `props` 上获取初始值即可：

```js
const props = defineProps(['initialCounter'])

// 计数器只是将props.initialCounter作为初始值
// 像下面这样做就使prop和后续更新无关
const counter = ref(props.initialCounter)
```

（2）需要对传入的 `prop` 值做进一步的转换，在这种情况中，最好是基于该 `prop` 值定义一个计算属性：

```js
const props = defineProps(['size'])

// 该prop变更时计算属性也会自动更新
const normalizedSize = computed(() => props.size.trim().toLowerCase())
```

##### prop 校验

> 以下是 `Vue3` 官方文档说明

```js
defineProps({
  // 基础类型检查（给出null和undefined值则会跳过任何类型检查）
  propA: Number,
  // 多种可能的类型
  propB: [String, Number],
  // 必传，且为String类型（所有prop默认都是可选的）
  propC: {
    type: String,
    required: true
  },
  // Number类型的默认值
  propD: {
    type: Number,
    default: 100
  },
  // 对象类型的默认值
  propE: {
    type: Object,
    // 对象或数组的默认值，必须从一个工厂函数返回
    // 该函数接收组件所接收到的原始prop作为参数
    default(rawProps) {
      return { message: 'hello' }
    }
  },
  // 自定义类型校验函数
  propF: {
    validator(value) {
      return ['success', 'warning', 'danger'].includes(value)
    }
  },
  // 函数类型的默认值
  propG: {
    type: Function,
    // 不像对象或数组的默认，这不是一个工厂函数，这会是一个用来作为默认值的函数
    default() {
      return 'Default function'
    }
  }
})
```

#### defineEmits (子传父)

> `defineEmits()` 也是一个仅 `<script setup>` 中可用的编译宏命令，并**不需要显式地导入**
>
> 📍语法：
>
> 1. `const emit = defineEmits(['自定义事件名', ...])`
> 2. `emit('自定义事件名', 数据)`
>
> `defineEmits()` 接收一个自定义事件的**数组**，显式地声明它要触发的自定义事件，而且**在 `<script setup>` 中，`defineEmits` 会返回一个 `emit` 函数供开发者使用，等同于 `Vue2` 中的 `$emit` 方法，而在模板中，也可以直接使用 `$emit` 方法触发自定义事件**
>
> 👍官方推荐写法： **声明时用小驼峰命名法，使用时用短横线命名法**
>
> ⚠注意：
>
> - `defineEmits()` 宏**不能在子函数中使用，它必须直接放置在 `<script setup>` 的顶级作用域下**
> - 如果一个原生事件的名字（例如 `click`）被定义在 `emits` 选项中，则监听器只会监听组件触发的 `click` 事件而不会再响应原生的 `click` 事件
> - 和原生 `DOM` 事件不一样，**组件触发的事件没有冒泡机制**
> - `defineEmits()` 传入的参数**不能引用在 `setup` 作用域中声明的局部变量**

如果显式地使用了 `setup` 函数而不是 `<script setup>`，则事件需要通过 [`emits`](https://cn.vuejs.org/api/options-state.html#emits) 选项来定义，`emit` 函数也被暴露在 `setup()` 的上下文对象上：

```js
export default {
  emits: ['inFocus', 'submit'],
  // setup(props, ctx) {
  //   ctx.emit('submit')
  // }
  setup(props, { emit }) {
    emit('submit')
  }
}
```

- **子传父实现步骤**

（1）子组件通过 `defineEmits()` 获取 `emit` 函数，然后通过 `emit` 触发事件并且传递数据

```vue
<script setup>
const props = defineProps({
	money: Number,
	car: {
		type: String,
		default: '宝马'
	}
})
const emit = defineEmits(['changeMoney'])
const spendMoney = () => {
	emit('changeMoney', 10)
}
</script>

<template>
	<div>
    <h3>我是子组件</h3>
    <div>金钱：{{ money }} <br> 跑车：{{ car }}</div>
    <button @click="spendMoney">吞金兽想花钱</button>
  </div>
</template>
```

（2）父组件监听子组件发出的自定义事件

```vue
<script setup>
import { ref } from 'vue'
import MyChild from './components/MyChild.vue'

const money = ref(10000)
const car = ref('玛莎拉蒂')
const changeMoney = (num) => {
  money.value = money.value - num
}
</script>

<template>
	<div>
    <h1>我是父组件</h1>
    <div>金钱：{{ money }}</div>
    <div>车辆：{{ car }}</div>
    <hr />
    <MyChild :money="money" :car="car" @changeMoney="changeMoney" />
  </div>
</template>
```

#### provide 与 inject (依赖注入)

> 通过 `provide()` 和 `inject()` 函数可以简便实现**跨级组件通信**，任何后代的组件树，无论层级有多深，都可以注入由父组件提供给整条链路的依赖
>
> `provide(注入名, 值)` 提供后代组件需要依赖的**数据或方法**：
>
> - 第一个参数可以是**字符串或是 `Symbol`**，一个组件**可多次调用 `provide()`**，使用**不同的注入名**，注入**不同的依赖值**
> - 第二个参数可以是任意类型的值，包括响应式的状态（比如 `ref`）
>
> `inject(注入名)` 注入 `provide()` 提供的**数据或方法**：
>
> - **如果提供的值是一个 `ref`，注入进来的会是该 `ref` 对象，而不会自动解包为其内部的值**
> - 如果父组件链上多个组件都**提供同一个注入名**，那么**离得更近的组件将会覆盖链上更远的组件所提供的值**，如果没能匹配到值，`inject()` 将返回 `undefined`

如果不使用 `<script setup>`，请确保 `provide()` 和 `inject()` 是在 `setup()` 函数内同步调用：

```js
// 祖先组件
import { provide } from 'vue'

export default {
  setup() {
    provide('message', 'hello!')
  }
}
```

```js
// 后代组件
import { inject } from 'vue'

export default {
  setup() {
    const message = inject('message')
    return { message }
  }
}
```

![Vue3的依赖注入](images/Vue3的依赖注入.png)

- **依赖注入实现代码**

> ⚠注意：**数据谁提供，谁负责修改**

（1）**祖先**组件

```vue
<script setup>
import { provide, ref } from 'vue';
import ParentCom from './ParentCom.vue';

// app组件数据传递给child
const count = ref(0);
provide('count', count);

// app组件方法传递给child，调用的时候可以回传数据
const updateCount = (num) => {
  count.value += num;
};
provide('updateCount', updateCount);
</script>

<template>
  <div
    style="border: 10px solid #ccc; padding: 50px; width: 600px"
  >
    app组件 - {{ count }} updateCount
    <ParentCom />
  </div>
</template>
```

（2）**父**组件

```vue
<script setup>
import ChildCom from './ChildCom.vue';
</script>

<template>
  <div style="padding: 50px">
    parent组件
    <hr />
    <ChildCom />
  </div>
</template>
```

（3）**子**组件

```vue
<script setup>
import { inject } from 'vue';
  
const count = inject('count');
const updateCount = inject('updateCount');
</script>

<template>
  <div style="padding: 50px; border: 10px solid #ccc">
    child组件 - {{ count }} <button @click="updateCount(100)">修改count</button>
  </div>
</template>
```



### 模板引用

> 通过 `ref` 标识**获取真实的 `DOM` 元素或者组件实例对象**的引用，实现步骤如下✏：
>
> 1. 调用 `ref` 函数创建一个空的 `ref` 对象：`const xxxRef = ref(null)`
> 2. 通过 `ref` 标识绑定 `ref` 对象到标签上： `<标签 ref="xxxRef">`
> 3. 通过 `xxxRef.value` 访问 `DOM` 元素或者组件实例
>
> **如果用于普通 `DOM` 元素，引用将是元素本身，如果用于子组件，引用将是子组件的实例**
>
> ⚠注意：**`ref` 必须等待组件挂载后（即 `onMounted` 之后）才能对它进行访问**

```vue
<script setup>
import { ref } from 'vue'

// 1.创建一个空的ref对象，必须和模板里的ref同名
const iptRef = ref(null)  
const clickFn = () => {
  console.log('iptRef  ----->  ', iptRef);
  // 3.通过xxxRef.value访问DOM元素或者组件实例
  iptRef.value.focus()
}
</script>

<template>
  <div>
    <!-- 2.绑定到标签ref属性上 -->
    <input ref="iptRef">
    <br>
    <button @click="clickFn">操作DOM</button>
  </div>
</template>
```

#### defineExpose (暴露组件属性和方法)

> **默认情况下，在 `<script setup>` 语法糖下的组件是默认私有的，子组件内部的属性和方法是不开放给父组件访问的**，可以通过 `defineExpose()` 编译宏指定暴露哪些属性和方法允许被访问，并**不需要显式地导入**
>
> 📍语法：`defineExpose({属性名1, 方法名1, ...})`

- **子**组件

```vue
<script setup>
import { ref } from 'vue'

const count = ref(0)
const hello = () => {
  alert("点我干啥")
}
defineExpose({ count, hello })
</script>

<template>
  <h3>我是Child组件</h3>
</template>
```

- **父**组件

```vue
<script setup>
import Child from './components/Child.vue'
import { ref } from 'vue'

const childRef = ref(null)
const clickFn = () => {
	console.log('childRef.value  ----->  ', childRef.value)
  console.log('childRef.value.count  ----->  ', childRef.value.count)
  childRef.value.hello()
}
</script>

<template>
  <Child ref="childRef" />
	<button @click="clickFn">点我获取子组件内数据和方法</button>
</template>
```

#### v-for 中的模板引用

> 当在 `v-for` 中使用模板引用时，对应的 `ref` 中包含的值是一个**数组**，它将在元素被挂载后包含对应整个列表的所有元素
>
> ⚠注意：`ref` 数组**不保证与源数组相同的顺序**

```vue
<script setup>
import { ref, onMounted } from 'vue'

const list = ref([
  /* ... */
])
const itemRefs = ref([])
onMounted(() => console.log(itemRefs.value))
</script>

<template>
  <ul>
    <li v-for="item in list" ref="itemRefs">{{ item }}</li>
  </ul>
</template>
```

#### 函数模板引用

> 除了使用字符串值作名字，`ref` 还可以绑定为一个**函数**，该函数会收到元素引用 `el` 作为其第一个参数，也可以绑定一个组件方法而不是内联函数，它会**在每次组件更新时都被调用**
>
> ⚠注意：需要**使用动态的 `:ref` 绑定才能够传入一个函数**，当绑定的元素被卸载时，函数也会被调用一次，此时的 `el` 参数会是 `null`

```html
<input :ref="(el) => { /* 将el赋值给一个数据属性或ref变量 */ }">
```



### v-if 和 v-for 的优先级

> **`Vue2` 和 `Vue3` 对于 `v-if` 和 `v-for` 同时使用时的优先级不同**

|        `Vue2`        |       `Vue3`        |
| :------------------: | :-----------------: |
| `v-for` 的优先级更高 | `v-if` 的优先级更高 |



### 组件 v-model

> 与 `Vue2` 不同，在 `Vue3` 中 **`v-model` 在组件上默认情况都是使用 `modelValue` 作为 `prop` 属性，并以 `update:modelValue` 作为对应的事件**

 `v-model` 在原生元素上的用法与 `Vue2` 一致：

```html
<input v-model="searchText" />
<!-- 等价于 -->
<input
  :value="searchText"
  @input="searchText = $event.target.value"
/>
```

而当使用在一个组件上时，与 `Vue2` 不同，`v-model` 会被展开为如下的形式：

```html
<CustomInput v-model="searchText" />
<!-- 等价于 -->
<CustomInput
  :modelValue="searchText"
  @update:modelValue="newValue => searchText = newValue"
/>
```

组件内部需要做以下两件事：

1. 将 `modelValue` 绑定到内部原生 `<input>` 元素的 `value` 属性
2. 当原生的 `input` 事件触发时，触发一个携带了新值的 `update:modelValue` 自定义事件

```vue
<!-- CustomInput.vue -->
<script setup>
defineProps(['modelValue'])
defineEmits(['update:modelValue'])
</script>

<template>
  <input
    :value="modelValue"
    @input="$emit('update:modelValue', $event.target.value)"
  />
</template>
```

#### 多个 v-model 绑定

> 在 `Vue2` 中，组件只有一个 `v-model` 绑定，而在 `Vue3` 中，可以通过给 `v-model` **指定一个或多个参数**，即可在单个组件实例上**创建多个 `v-model` 双向绑定**，组件上的每一个 `v-model` 都会同步不同的 `prop` 属性，而无需额外的选项
>
> 📍语法：`<标签 v-model:参数 = "值">`

```html
<CustomInput v-model="searchText" v-model:title="bookTitle" />
```

```vue
<!-- CustomInput.vue -->
<script setup>
defineProps({
  modelValue: String,
  title: String
})
defineEmits(['update:modelValue', 'update:title'])
</script>

<template>
  <input
    :value="modelValue"
    @input="$emit('update:modelValue', $event.target.value)"
  />
	<input
    :value="title"
    @input="$emit('update:title', $event.target.value)"
  />
</template>
```

#### 自定义修饰符

> 默认情况下，可**通过 `defineProps()` 函数声明的 `modelModifiers` 属性来访问到组件的 `v-model` 上所添加的自定义修饰符**，它的**默认值是一个空对象**，当修饰符被使用时，对象内就会包含该修饰符的键名，并它的值为 `true`

```html
<MyComponent v-model.capitalize="myText" />
```

```vue
<script setup>
const props = defineProps({
  modelValue: String,
  modelModifiers: { default: () => ({}) }
})
const emit = defineEmits(['update:modelValue'])
/*
	当capitalize修饰符被使用时，modelModifiers属性就会包含了capitalize键名且其值为true
*/
console.log(props.modelModifiers) // { capitalize: true }
const emitValue = (e) => {
  let value = e.target.value
  if (props.modelModifiers.capitalize) {
    // 首字母大写
    value = value.charAt(0).toUpperCase() + value.slice(1)
  }
  emit('update:modelValue', value)
}
</script>

<template>
  <input :value="modelValue" @input="emitValue" />
</template>
```

##### 带参数的修饰符

> **对于有参数、修饰符的 `v-model` 绑定，`defineProps()` 函数声明的属性名是 `修饰符 + Modifiers`**

```html
<UserName
  v-model:first-name.capitalize="first"
  v-model:last-name.uppercase="last"
/>
```

```vue
<script setup>
const props = defineProps({
  firstName: String,
  lastName: String,
  firstNameModifiers: { default: () => ({}) },
  lastNameModifiers: { default: () => ({}) }
})
defineEmits(['update:firstName', 'update:lastName'])

console.log(props.firstNameModifiers) // { capitalize: true }
console.log(props.lastNameModifiers) // { uppercase: true}
</script>

<template>
  <input
    :value="firstName"
    @input="$emit('update:firstName', $event.target.value)"
  />
  <input
    :value="lastName"
    @input="$emit('update:lastName', $event.target.value)"
  />
</template>
```



### TypeScript 与组合式 API

> `<script>` 上添加 `lang="ts"` 才能写 `TypeScript` 代码，即 `<script setup lang="ts"></script>`
>
> 这之前的语法声明都称之为“**运行时声明**”，而搭配 `TS` 语法的声明被称之为“**基于类型的声明**”，编译器会尽可能地尝试根据类型参数推导出等价的运行时选项
>
> ⚠注意：基于类型的声明或者运行时声明可以择一使用，但是**两者不能同时使用**

#### 为组件的 props 标注类型

> 通过**泛型参数**来定义 `props` 的类型

```vue
<script setup lang="ts">
const props = defineProps<{
  money: number;
  car?: string;
}>()

// 也可以将props的类型移入一个单独的接口中（推荐写法👍）
interface Props {
  money: number;
  car?: string;
}
const props = defineProps<Props>();
</script>
```

**当使用基于类型的声明时，会失去为 `props` 声明默认值的能力，可通过 `withDefaults()` 编译器宏解决**，此外还帮助程序为默认值提供类型检查，并确保返回的 `props` 类型删除了已声明默认值的属性的可选标志。

```typescript
const props = withDefaults(defineProps<Props>(), {
  car: "桑塔纳"
});
```

#### 为组件的 emits 标注类型

> 与 `props` 一样，也是通过**泛型参数**来定义 `emits` 的类型，可以对所触发事件的类型进行更细粒度的控制
>
> 类型参数可以是以下其中一种：
>
> 1. 一个可调用的函数类型，但是**写作一个包含调用签名的类型字面量**，它将被用作返回的 `emit` 函数的类型
> 2. 一个类型字面量，其中**键是事件名称，值是数组或元组类型，表示事件的附加接受参数**，如果使用了具名元组，那每个参数都可以有一个显式的名称

```vue
<script setup lang="ts">
// ...
const emit = defineEmits<{
  (e: "updateMoney", money: number): void;
}>();
  
// 也可以将emits的类型移入一个单独的接口中（推荐写法👍）
interface Emits {
  (e: "updateMoney", money: number): void;
}
const emit = defineEmits<Emits>();
</script>

<template>
  <div>我是子组件</div>
  <div>money - {{ money }}</div>
  <button @click="emit('updateMoney', 10000)">点击+10000</button>
</template>
```

在 `3.3+` 版本中，还提供了一种可选的、更简洁的语法📍：

```typescript
const emit = defineEmits<{
  updateMoney: [money: number];
}>();
```

#### 为 ref() 标注类型

> `ref()` 会根据初始化时的值隐式推导其类型，当需要指定一个更复杂的类型，在调用 `ref()` 时传入一个**泛型参数**，来覆盖默认的推导行为

```vue
<script setup lang="ts">
import { ref } from "vue";
// 如果是简单类型，建议省略，使用默认的类型推导
const money = ref(10000);
// 等价于const money = ref<number>(10000);

// 如果是复杂类型，推荐指定泛型的完整写法
// 复杂数据一般是后台返回数据，默认值是空，无法进行类型推导
type TodoItem = {
  id: number
  name: string
  done: boolean
}
type TodoList = TodoItem[]
const list = ref<TodoList>([])

setTimeout(() => {
  list.value = [
    { id: 1, name: '吃饭', done: false },
    { id: 2, name: '睡觉', done: true }
  ]
}, 1000)
</script>
```

⚠需要注意的是，**如果你指定了一个泛型参数但没有给出初始值，那么最后得到的就将是一个包含 `undefined` 的联合类型**：

```typescript
// 推导得到的类型：Ref<string | number>
const year = ref<string | number>('2023')
year.value = 2023

// 推导得到的类型：Ref<number | undefined>
const n = ref<number>()
```

#### 为 reactive() 标注类型

> `reactive()` 也会隐式地从它的参数中推导类型，如果要显式地标注一个变量的类型，可以使用**接口或者类型别名**
>
> ⚠注意：**不推荐使用 `reactive()` 的泛型参数**，因为处理了深层次 `ref` 解包的返回值与泛型参数的类型不同

```vue
<script setup lang="ts">
import { reactive } from 'vue'

// 默认值属性是固定的，推荐使用类型推导
// 推导得到的类型：{ title: string }
const book = reactive({ title: '新华字典' })

// 根据默认值推导不出我们需要的类型，推荐使用接口或者类型别名给变量指定类型
// 需要的类型：{ title: string, year?: number }
type Book = {
  title: string
  year?: number
}
const book: Book = reactive({ title: '新华字典' })
book.year = 2023
</script>
```

#### 为 computed() 标注类型

> `computed()` 会自动从其计算函数的**返回值**上推导出类型，也可以通过**泛型参数**显式指定类型
>
> 💡技巧：**一般情况下，计算属性都会省略类型**

```vue
<script setup lang="ts">
import { ref, computed } from 'vue'

const count = ref(0)
// 推导得到的类型：ComputedRef<number>
const doubleCount = computed(() => count.value * 2)
// 若返回值不是string类型则会报错
const floatDoubleCount = computed<string>(() => (count.value * 2).toFixed(2));
</script>
```

#### 为事件处理函数标注类型

> 没有类型标注时，`event` 事件对象会隐式地标注为 `any` 类型，因此**在处理原生 `DOM` 事件时，建议显式地为 `event` 事件对象标注类型**
>
> 💡技巧：在 `VSCode` 编译器中，鼠标🖱悬停在事件监听上（`@input`、`addEventListener` 等）即可查看相应类型

```vue
<script setup lang="ts">
// 1. @change="handleChange($event)" 查看$event类型
// 2. 鼠标悬停事件@change查看类型
const handleChange = (event: Event) => {
  // 在访问event上的属性时可能需要使用类型断言
  // event.target是EventTarget | null类型
  // 通过document.querySelector('input')查看返回值类型
  console.log((event.target as HTMLInputElement).value)
}
</script>

<template>
  <input type="text" @change="handleChange" />
</template>
```

#### 为模板引用标注类型

> 模板引用需要通过一个显式指定的**泛型参数**和一个初始值 `null` 来创建
>
> ⚠需要注意的是，**为了严格的类型安全，有必要在访问 `el.value` 时使用 `?.` 可选链或类型守卫**，这是因为直到组件被挂载前，这个 `ref` 的值都是初始的 `null`，并且在由于 `v-if` 的行为将引用的元素卸载时也可以被设置为 `null`
>
> 💡技巧：在 `VSCode` 编译器中，鼠标🖱悬停在 `document.querySelector(...)` 上即可查看对应 `DOM` 元素类型

```vue
<script setup lang="ts">
import { ref, onMounted } from 'vue'
// 通过document.querySelector('input')查看元素类型
const el = ref<HTMLInputElement | null>(null)
onMounted(() => {
  el.value?.focus()
})
</script>

<template>
  <input ref="el" />
</template>
```

#### 为组件模板引用标注类型

> 为了获取组件的类型，**首先需要通过 `typeof` 得到其类型，再使用 `TypeScript` 内置的 `InstanceType` 工具类型来获取其实例类型**

```vue
<!-- 子组件 -->
<script setup lang="ts">
import { ref } from 'vue'

const isContentShown = ref(false)
const open = () => (isContentShown.value = true)

defineExpose({
  open
})
</script>
```

```vue
<!-- 父组件 -->
<script setup lang="ts">
import MyModal from './MyModal.vue'

const modal = ref<InstanceType<typeof MyModal> | null>(null)
const openModal = () => {
  modal.value?.open()
}
</script>
```

如果**组件的具体类型无法获得**，或者并不关心组件的具体类型，则**可使用 `ComponentPublicInstance`**，它**只会包含所有组件都共享的属性**：

```typescript
import { ref } from 'vue'
import type { ComponentPublicInstance } from 'vue'

const child = ref<ComponentPublicInstance | null>(null)
```

#### 为 provide 与 inject 标注类型

> `Vue` 提供了一个 **`InjectionKey` 接口**，它是一个继承自 `Symbol` 的泛型类型，可以**用于标注 `provide` 与 `inject` 之间的同步注入值的类型**
>
> 👍建议：为了可以被多个组件导入，可将注入 `key` 的类型放在一个单独的文件中

```typescript
import { provide, inject } from 'vue'
import type { InjectionKey } from 'vue'

const key = Symbol() as InjectionKey<string>
provide(key, 'foo') // 若提供的是非字符串值会导致错误

const foo = inject(key) // foo的类型：string | undefined
```

⚠需要注意的是，**当使用字符串注入 `key` 时**，注入值的类型是 `unknown`，**需要通过泛型参数显式声明**：

```typescript
const foo = inject<string>('foo') // 类型：string | undefined
```

注入值仍然可以是 `undefined`，因为无法保证提供者一定会在运行时 `provide` 这个值。

**当提供一个默认值后，`undefined` 类型就可以被移除**：

```typescript
const foo = inject<string>('foo', 'bar') // 类型：string
```

**如果确定该值将始终被提供**，则还可以**使用类型断言强制转换该值**：

```typescript
const foo = inject('foo') as string
```



------



## Vue Router

> 🔗`Vue Router` 官方中文文档：https://v3.router.vuejs.org/zh/

> 前端 `Vue` 中的**路由是指路径和组件的映射关系**

> `Vue Router` 是 `Vue.js` 官方的路由管理器，用 `Vue.js` + `Vue Router` 创建单页应用是非常简单的，使用 `Vue.js`，我们已经可以通过组合组件来组成应用程序，当你要把 `Vue Router` 添加进来，我们需要做的是，将组件 (`components`) 映射到路由 (`routes`)，然后告诉 `Vue Router` 在哪里渲染它们

### 3.x 版本（Vue2.x）

#### 基本使用

（1）下载安装

```shell
npm install vue-router@3
```

（2）引入并挂载 `vue-router` ，然后创建路由对象，并进行路由配置

```js
src/router/index.js
-------------------------
// 这个就是当前项目中的路由模块，以后与路由配置相关的代码全部在这个文件中搞定
// 1.引入Vue
import Vue from 'vue'
// 2.引入VueRouter构造函数
import VueRouter from 'vue-router'

// 引入路由所需要映射的组件
import Index from '@/views/Index.vue'
// 3.挂载：明确的告诉Vue，使用vue-router来进行路由的管理
// 如果在一个模块化工程中使用它，必须要通过Vue.use()明确地安装路由功能
Vue.use(VueRouter)

// 4.创建路由对象
let router = new VueRouter({
// 5.配置路由：告诉我你的路由到底映射那一个组件，通过routes可以配置多个路由，它是一个数组，里面包含着一个一个的对象，每一个对象就是一个单独的路由配置
  routes: [
    // 6.配置具体的路由，一般情况下，常见的配置属性有：
    // name：路由名称
    // path：路由路径，如/login
    // component：路由所映射的组件对象
    // children：设置嵌套路由
    // redirect：设置重定向
    {
      name: 'Index',
      path: '/index',
      component: Index
    }
  ]
})

// 暴露：因为当前是一个路由模块
// 不能使用module.exports来进行暴露，否则报错
export default router
```

（3）在 `main.js` 中引入并注入路由

```js
// 引入路由模块
import router from './router'

new Vue({
  // 注入路由，让整个应用的组件都能使用路由功能
  router,
  render: h => h(App)
}).$mount('#app')
```

（4）在根组件 `App.vue` 设置路由所映射组件的展示区域

```html
<template>
  <div id="app">
    <!-- 路由匹配到的组件将渲染在这里 -->
    <router-view></router-view>
  </div>
</template>
```



#### 匹配优先级

同一个路径可以匹配多个路由，此时匹配的优先级就按照路由的定义顺序🛠：**路由定义越早，优先级越高**。



#### 动态路由匹配

> 我们经常需要把某种模式**匹配到的所有路由，全都映射到同个组件**，例如有一个 `User` 组件，对于所有 `ID` 各不相同的用户，都要使用这个组件来渲染，那么我们可以在 `vue-router` 的路由路径中使用“动态路径参数”来区分不同的用户

```js
const router = new VueRouter({
  routes: [
    // 动态路径参数以冒号开头
    { path: '/user/:id', component: User }
  ]
})
```

可以在一个路由中设置多段“路径参数”，对应的值都会设置到 `$route.params` 中。

|              路由               |       匹配路径        |            `$route.params`             |
| :-----------------------------: | :-------------------: | :------------------------------------: |
|        `/user/:username`        |     `/user/evan`      |         `{ username: 'evan' }`         |
| `/user/:username/post/:post_id` | `/user/evan/post/123` | `{ username: 'evan', post_id: '123' }` |

- 响应路由参数的变化

> 当使用路由参数时，例如从 `/user/213` 导航到 `/user/5646`，**原来的组件实例会被复用**，因为两个路由都渲染同个组件，比起销毁再创建，复用则显得更加高效，不过**这也意味着组件的生命周期钩子不会再被调用**

（1）复用组件时，想对路由参数的变化作出响应的话，你**可以简单地 `watch` 侦听 `$route` 对象**

```js
export default {
	watch: {
		$route(to, from) {
			// 对路由变化作出响应...
		}
	}
}
```

（2）或者使用**组件内部的 `beforeRouteUpdate` 导航守卫**

```js
export default {
	beforeRouteUpdate(to, from, next) {
		// 对路由变化作出响应...
		// 不要忘记调用next()
		next()
  }
}
```



#### 嵌套路由

> 实际开发过程中，嵌套结构要对应着嵌套路由，需要为谁添加嵌套路由，就在这个父级路由中添加 `children` 配置，与父级的路由配置没有区别

（1）添加嵌套路由配置

```js
const router = new VueRouter({
  routes: [
    {
      path: '/user',
      component: User,
      children: [
        {
          // 当/user/profile匹配成功，
          // UserProfile会被渲染在User的<router-view>中
          path: 'profile',
          component: UserProfile
        },
        {
          // 当/user/posts匹配成功
          // UserPosts会被渲染在User的<router-view>中
          path: 'posts',
          component: UserPosts
        }
      ]
    }
  ]
})
```

（2）在父级组件中添加 `router-view`，谁有嵌套路由就在谁里面添加

```html
<div class="user">
	<router-view></router-view>
</div>
```

⚠**要注意，以 `/` 开头的嵌套路径会被当作根路径，这让你充分的使用嵌套组件而无须设置嵌套的路径。**

此时基于上面的配置，当你访问 `/user/foo` 时，`User` 的出口是不会渲染任何东西，这是因为没有匹配到合适的子路由，如果你想要渲染点什么，可以提供一个空的子路由：

```js
const router = new VueRouter({
  routes: [
    {
      path: '/user',
      component: User,
      children: [
        // 当/user匹配成功，
        // UserHome会被渲染在User的<router-view>中
        { path: '', component: UserHome }
        // ...其他子路由
      ]
    }
  ]
})
```



#### 路由懒加载

> 以下为 `Vue Router` 官方文档说明

当打包构建应用时，`JavaScript` 包会变得非常大，影响页面加载，如果我们能把不同路由对应的组件分割成不同的代码块，然后**当路由被访问的时候才加载对应组件**，这样就更加高效了。

```js
const router = new VueRouter({
  routes: [{ path: '/foo', component: () => import('./Foo.vue') }]
})
```



#### 路由重定向

> 匹配 `path` 路径后，通过 `redirect` 可强制跳转到其他路径

```js
// 方式1：路径
export const constantRoutes = [
	{
		path: '/404',
		component: () => import('@/views/404'),
	},
	{ path: '*', redirect: '/404' }
]

// 方式2：命名路由
export const constantRoutes = [
    { path: '/a', redirect: { name: 'b' }}
]
```



#### 滚动行为

> 使用前端路由，当切换到新路由时，想要页面滚到顶部，或者是保持原先的滚动位置，就像重新加载页面那样，这些行为 `Vue Router` 都能做到，而且更好，它让你可以自定义路由切换时页面如何滚动
>
> ⚠注意: **这个功能只在支持 `history.pushState` 的浏览器中可用**

`scrollBehavior` 方法接收 `to` 和 `from` 路由对象，第三个参数 `savedPosition` 当且仅当 `popstate` 导航 (通过浏览器的前进或者后退按钮触发) 时才可用：

```js
const router = new VueRouter({
  routes: [...],
  scrollBehavior (to, from, savedPosition) {
		// 对于所有路由导航，简单地让页面滚动到顶部
		// return { x: 0, y: 0 }
           
		// 返回savedPosition，在按下后退/前进按钮时，就会像浏览器的原生表现那样
		if (savedPosition) {
			return savedPosition
		} else {
			return { x: 0, y: 0 }
		}
  }
})
```



#### 路由模式

> `Vue Router` 除了默认 `hash` 模式，还有 `history` 模式

```js
const router = new VueRouter({
  // mode: 'hash',
  mode: 'history',
  routes: [...]
})
```

##### hash 模式

> `URL` 带着一个 `#`，例如：`http://www.abc.com/#/vue`，它的 `hash` 值就是 `#/vue`
>
> **`hash` 值会出现在 `URL` 里面，但是不会出现在 `HTTP` 请求中**，对后端完全没有影响，所以**改变 `hash` 值，不会重新加载页面**，这种模式的浏览器支持度很好，低版本的 `IE` 浏览器也支持这种模式

`hash` 模式的主要原理🛠就是 `onhashchange()` 事件：

```js
window.onhashchange = function(event) {
  console.log(event.oldURL, event.newURL);
  let hash = location.hash.silce(1);
}
```

使用 `onhashchange()` 事件的好处就是在页面的 `hash` 值发生变化时，无需向后端发起请求，`window` 就可以监听事件的改变，并按规则加载相应的代码。

除此之外，`hash` 值变化对应的 `URL` 都会被浏览器记录下来，这样浏览器就能实现页面的前进和后退，虽然是没有请求后端服务器，但是页面的 `hash` 值和对应的 `URL` 关联起来了。

##### history 模式

> `URL` 中没有 `#`，例如：`http://abc.com/user/id`，相比 `hash` 模式更加好看
>
> `history` 模式使用的是传统的路由分发模式，即用户在访问或者刷新一个 `URL` 时，服务器会接收这个请求并解析这个 `URL`，然后做出相应的逻辑处理，但是**需要后台配置支持**，如果后台没有正确的配置，当用户在浏览器直接访问就会返回 `404`，所以需要**在服务端增加一个覆盖所有情况的候选资源**：如果 `URL` 匹配不到任何静态资源，则应该返回同一个 `index.html` 页面，这个页面就是应用依赖的页面
>
> 📜参考文章：[vue-router踩坑：History模式下，打包后页面访问404](https://blog.csdn.net/bxl0218/article/details/80974811)

在 `JS` 中，`history` 的 `API` 可以分为两大部分：

1. **修改历史状态**：包括了 `HTML5` 中新增的 `pushState()` 和 `replaceState()` 方法，这两个方法应用于浏览器的历史记录栈，提供了对历史记录进行修改的功能。只是**当进行修改时，虽然修改了 `URL`，但浏览器不会立即向后端发送请求**，如果要做到改变 `URL`，但又不刷新页面的效果，就需要前端用上这两个 `API`，`Vue Router` 在 `history` 模式下正是如此

2. **切换历史状态**： 包括 `forward()`、`back()`、`go()` 三个方法，分别对应了浏览器的前进，后退，跳转操作

🧩这里提一句，`location.href` 与 `pushState()` 一样都是跳转页面，但前者会刷新页面。

##### 两种模式对比

> [参考文章：vue路由中hash和history模式的区别](https://blog.csdn.net/jiangjunyuan168/article/details/122694404?ops_request_misc=&request_id=&biz_id=102&utm_term=vue%E8%B7%AF%E7%94%B1hash%E4%B8%8Ehistory%E5%8C%BA%E5%88%AB&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-2-122694404.nonecase&spm=1018.2226.3001.4187)

调用 `history.pushState()` 相比于直接修改 `hash`，存在以下**优势**✨：

1. **`pushState()` 设置的新 `URL` 可以是与当前 `URL` 同源的任意 `URL`**，而 `hash` 只可修改 `#` 后面的部分，因此只能设置与当前 `URL` 同文档的 `URL`
2. **`pushState()` 设置的新 `URL` 可以是与当前 `URL` 一模一样，这样也会把记录添加到栈中**，而 `hash` 设置的新值必须与原来不一样才会触发动作将记录添加到栈中
3. `pushState()` 通过 `stateObject` 参数可以添加任意类型的数据到记录中，而 `hash` 只可添加短字符串
4. `pushState()` 可额外设置 `title` 属性供后续使用

在 `hash` 模式下，仅 `hash` 符号之前的 `URL` 会被包含在请求中，后端如果没有做到对路由的全覆盖，也不会返回 `404` 错误，而 `history` 模式下，如果没有相应的路由或资源，将返回 `404` 错误。



#### 声明式导航

> `<router-link>` 组件支持用户在具有路由功能的应用中点击导航，**通过 `to` 属性指定目标地址**，默认渲染成带有正确链接的 `<a>` 标签，可以**通过配置 `tag` 属性生成别的标签**
>
> 另外，**当目标路由成功激活时，链接元素自动设置一个表示激活的 `CSS` 类名**，类名如下所示：
>
> - `router-link-active`：**模糊**匹配，`to="/my"` 可以匹配到 `/my` 、`/my/a`、`/my/b`
> - `router-link-exact-active`：**精确**匹配，`to="/my"` 仅可以匹配 `/my`

```html
<!-- 字符串 -->
<router-link to="/home">Home</router-link>
<!-- 渲染结果 -->
<a href="home">Home</a>

<!-- 使用v-bind的JS表达式 -->
<router-link :to="{ path: '/home' }">Home</router-link>

<!-- 命名的路由 -->
<router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>

<!-- 带查询参数，下面的结果为/register?plan=private -->
<router-link :to="{ path: '/register', query: { plan: 'private' }}"
  >Register</router-link
>
```



#### 编程式导航

> 除了使用 `<router-link>` 创建 `<a>` 标签来定义导航链接，我们还可以借助 `$router` 的实例方法，通过编写代码来实现

（1）`this.$router.push(location)`

该方法的参数可以是一个**字符串路径**，或者一个**描述地址的对象**，可以导航到不同的 `URL`，并且会向 `history` 栈添加一个新的记录，所以当用户点击浏览器后退按钮时，则回到之前的 `URL`。

> `query` 和 `params` 的区别🔎：
>
> - 使用 `query` 传参会在浏览器地址栏中显示参数，而 `params` 传参不会显示，数据相对安全
>
> - **不设置动态路由参数的情况下，`params` 传递的参数会在页面刷新后丢失，而 `query` 参数不会丢失**（原因：`params` 传参是在内存中传参，`query` 传参是在地址栏传参）

```js
// 字符串
this.$router.push('/home')

// 对象
this.$router.push({ path: '/home' })

// 命名的路由
this.$router.push({ name: 'user', params: { userId: '123' }})

// 带查询参数，变成/register?plan=private
this.$router.push({ path: 'register', query: { plan: 'private' }})
```

⚠注意：**`params` 必须和 `name` 搭配使用，如果提供了 `path`，`params` 会被忽略，同样的规则也适用于 `router-link` 组件的 `to` 属性。**

```js
this.$router.push({ name: 'user', params: { userId: '123' }}) // -> /user/123
this.$router.push({ path: `/user/${ '123' }` }) // -> /user/123

// 这里的params不生效
this.$router.push({ path: '/user', params: { userId: '123' }}) // -> /user
```

（2）`this.$router.replace(location)`

跟 `$router.push` 很像，唯一不同就是它不会向 `history` 栈添加一个新记录，会替换掉当前的 `history` 栈记录。

（3）`this.$router.back()`

（4）`this.$router.forward()`

（5）`this.$router.go(n)`

这个方法的参数是一个**整数**，意思是在 `history` 记录中向前或者后退多少步。

```js
// 在浏览器记录中前进一步，等同于$router.forward()
router.go(1)

// 后退一步记录，等同于$router.back()
router.go(-1)

// 前进3步记录
router.go(3)

// 如果history记录不够用，那就默默地失败呗
router.go(-100)
router.go(100)
```




#### 导航守卫

> 顾名思义，导航守卫的意思就是监听每一个路由跳转的过程，然后提供一些钩子函数让你有机会在跳转过程中进行限制操作
>
> **参数或查询的改变并不会触发进入或离开的导航守卫**，可以通过 `$route` 对象来应对这些变化，或使用 `beforeRouteUpdate` 的组件内守卫
>
> ⚠注意：**守卫是异步解析执行，导航在所有守卫 `resolve` 完之前一直处于等待中**

每个守卫方法（除了 `afterEach`）都接收三个参数：

1. `to`：即将要进入的目标路由对象
2. `from`：当前导航正要离开的路由对象
3. `next`：一定要调用该方法来 `resolve` 当前钩子，其执行效果依赖于 `next` 方法的调用参数

|                 参数                 | 说明                                                         |
| :----------------------------------: | ------------------------------------------------------------ |
|               `next()`               | 进行管道中的下一个钩子，**如果全部钩子执行完了，可以执行跳转，此时导航的状态就是 `confirmed` 确认** |
|            `next(false)`             | 中断当前的导航                                               |
| `next('/')` 或 `next({ path: '/' })` | 当前的导航被中断，然后进行一个新的导航，跳转到另一个地址，你可以向 `next` 传递任意位置对象，且允许设置诸如 `replace: true`、`name: 'home'` 之类的选项以及任何用在 `router-link` 的 `to` 属性或 `router.push` 中的选项 |
|            `next(error)`             | 如果传入 `next` 的参数是一个 `Error` 实例，则导航会被终止且该错误会被传递给 `router.onError()` 注册过的回调 |

⚠要注意的是，**确保 `next` 函数在任何给定的导航守卫中都被严格调用一次**，它可以出现多于一次，但是只能在所有的逻辑路径都不重叠的情况下，否则钩子永远都不会被解析或报错。

##### 全局守卫

> 挂载到 `router` 实例上，**用于控制路由的每一次跳转**

（1）`router.beforeEach` **全局前置守卫**

```js
const router = new VueRouter({ ... })

router.beforeEach((to, from, next) => {
  // 常用于登录验证
  // ...
})
```

（2）`beforeResolve` **全局解析守卫**

和 `router.beforeEach` 类似，区别是在导航被确认之前，同时在所有组件内守卫和异步路由组件被解析之后，解析守卫就被调用。

（3）`afterEach` **全局后置守卫**

和其他全局守卫不同的是，不会接受 `next` 函数也不会改变导航本身。

```js
router.afterEach((to, from) => {
  // ...
})
```

##### 路由守卫

> 在路由配置上直接定义 `beforeEnter` 守卫，**只有进入到当前组件才会执行**，与全局前置守卫方法的参数是一样的

```js
const router = new VueRouter({
  routes: [
    {
      path: '/foo',
      component: Foo,
      beforeEnter: (to, from, next) => {
          // ...
      }
    }
  ]
})
```

##### 组件内守卫

（1）`beforeRouteEnter`

```js
export default {
	beforeRouteEnter(to, from, next) {
		next(vm => {
			// 通过vm访问组件实例
		})
  }
}
```

在渲染该组件的对应路由被 `confirm` 前调用（大概意思是**路由进入之前调用**），以及**不能获取 `this` 实例，因为当守卫执行前，组件实例还没被创建**，不过可以通过传一个回调给 `next()` 来访问组件实例，在导航被确认的时候执行回调，并且把组件实例作为回调方法的参数。

⚠**要注意的是 `beforeRouteEnter` 是支持给 `next` 传递回调的唯一守卫，对于 `beforeRouteUpdate` 和 `beforeRouteLeave` 来说，组件实例 `this` 已经可用，所以不支持传递回调。**

（2）`beforeRouteUpdate`

```js
export default {
	beforeRouteUpdate(to, from, next) {
		// 对路由参数的变化作出响应...
		// 不要忘记调用next()
		next()
  }
}
```

在当前路由改变，但是该**组件被复用时调用**，例如对于一个带有动态参数的路径 `/foo/:id`，在 `/foo/1` 和 `/foo/2` 之间跳转的时候，由于会渲染同样的 `Foo` 组件，因此组件实例会被复用，而这个钩子就会在这个情况下被调用。

（3）`beforeRouteLeave`

**导航离开该组件的对应路由时调用**，其有以下使用场景📣：

1. 当组件内有定时器，路由切换时，需要清除以免导致内存占用
2. 将公用信息保存到 `session` 或 `Vuex` 中
3. 当页面有未关闭的窗口，或在用户还未保存修改前突然离开的时候，该导航可以通过 `next(false)` 来取消阻止页面跳转

```js
export default {
	beforeRouteLeave(to, from, next) {
		const answer = window.confirm('Do you really want to leave? you have unsaved changes!')
		if(answer) {
    	next()
  	} else {
    	next(false)
  	}
	}
}
```

##### 完整的导航解析流程

![完整的导航解析流程](images/完整的导航解析流程.webp)

1. 导航被触发
2. 在失活的组件里调用 `beforeRouteLeave` 守卫
3. 调用全局的 `beforeEach` 守卫
4. 在重用的组件里调用 `beforeRouteUpdate` 守卫
5. 在路由配置里调用 `beforeEnter`
6. 解析异步路由组件
7. 在被激活的组件里调用 `beforeRouteEnter`
8. 调用全局的 `beforeResolve` 守卫
9. 导航被确认
10. 调用全局的 `afterEach` 钩子
11. 触发 `DOM` 更新
12. 调用 `beforeRouteEnter` 守卫中传给 `next` 的回调函数，创建好的组件实例会作为回调函数的参数传入



### 4.x 版本（Vue3.x）

> `Vue2` 升级到 `Vue3` 之后，配套的 `Vue Router` 也升级为 `4.x` 版本，与 `3.x` 版本的语法基本一致，但是有一些细微的修改

#### 基本使用

（1）下载安装

```tex
npm install vue-router@4
```

（2）引入 `vue-router`，然后创建路由对象，并进行路由配置

```js
// src/router/index.js
import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";

const router = createRouter({
  // history: createWebHistory(),
  history: createWebHashHistory(),
  // 配置具体的路由，一般情况下，常见的配置属性有：
  // name：路由名称
  // path：路由路径，如/login
  // component：路由所映射的组件对象
  // children：设置嵌套路由
  // redirect：设置重定向
  routes: [
    { path: "/", component: () => import("../views/Home.vue") },
    { path: "/login", component: () => import("../views/Login.vue") },
  ],
});

export default router;
```

（3）在 `main.js` 中引入和注册路由

```js
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
// 必须在创建完app之后，以及要在mount之前通过use方式来注册router路由
createApp(App).use(router).mount("#app");
```

（4）在根组件 `App.vue` 设置路由所映射组件的展示区域 `<router-view>`



#### 路由模式

> 不同于 `3.x` 版本的 `mode`，**在 `4.x` 版本中是通过 `history` 配置路由模式**，并引进两个函数：
>
> 1. `createWebHashHistory()`：`hash` 模式
> 2. `createWebHistory()`：`history` 模式（**官方推荐**👍）

```js
import {
  createRouter,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";

const router = createRouter({
  // history: createWebHistory(), // history模式
  history: createWebHashHistory(), // hash模式
  routes: [{...}],
});

export default router;
```



####  useRouter 和 useRoute 函数

> 在 `Vue3` 中的 `setup` 里面没有访问 `this`，所以不能再直接访问 `this.$router` 或 `this.$route`，作为替代，需要**使用 `useRouter()` 和 `useRoute()` 函数获取路由实例和当前路由对象**
>
> ⚠注意：
>
> - `useRouter()` 和 `useRoute()` 函数**必须在 `setup` 顶层使用**，否则函数返回值是 `undefined`
> - **在 `<template>` 模板中仍然可以访问 `$router` 和 `$route`**

```vue
<script setup>
import { useRouter, useRoute } from 'vue-router'

const router = useRouter() // 路由实例
const route = useRoute() // 当前路由对象
const pushWithQuery = (query) => {
	router.push({
		path: '/login',
		query: {
			...route.query,
			...query,
		},
	})
}
</script>
```

`route` 对象是一个**响应式对象**，所以它的任何属性都可以被监听，但应该👍**避免监听整个 `route` 对象，在大多数情况下，应该直接监听对象中某一个期望改变的数据。**

```vue
<script setup>
import { useRoute } from 'vue-router'

const route = useRoute()
const userData = ref()
watch(() => route.params.id, async newId => {
	userData.value = await fetchUser(newId)
})
</script>
```



### 📌*进阶知识点*📌

#### *如何解决路由的组件复用问题？*

> `Vue Router` 官方文档：[路由 - 动态路由匹配 - 响应路由参数的变化](https://router.vuejs.org/zh/guide/essentials/dynamic-matching.html#%E5%93%8D%E5%BA%94%E8%B7%AF%E7%94%B1%E5%8F%82%E6%95%B0%E7%9A%84%E5%8F%98%E5%8C%96) 

> 由于路由的组件复用机制，**当路由的路径 `path` 没有发生变化时，路由对应的组件会被直接复用**，也就是生命周期钩子函数不再执行，请求也就不会再次发起
>
> ✔解决思路：**给 `<router-view>` 加上 `key` 值为每次路由切换的完整路径**

```tex
fullPath        /category/1029487?username=zs&age=18
path            /category/1029487
```

```html
<RouterView :key="$route.fullPath" />
```



------



## Vuex

> 🔗`Vuex` 官方中文文档：https://v3.vuex.vuejs.org/zh/

> `Vuex` 是一个专为 `Vue.js` 应用程序开发的**状态管理**模式，它采用集中式存储管理应用的所有组件的状态，并以相应的规则保证状态以一种可预测的方式发生变化
>
> 当我们的应用遇到多个组件共享状态时，单向数据流的简洁性很容易被破坏：
>
> - 多个视图依赖于同一状态
> - 来自不同视图的行为需要变更同一状态
>
> 这时候就需要用到 `Vuex`

**`Vuex` 的状态存储是响应式的**，当 `Vue` 组件从 `store` 中读取状态的时候，若 `store` 中的状态发生变化，那么相应的组件也会相应地得到高效更新，但不能直接改变 `store` 中的状态，💥**改变 `store` 中的状态的唯一途径就是显式地 `commit` 提交 `mutation`**，这样使得我们可以方便地跟踪每一个状态的变化。

![Vuex](images/Vuex.png)

`Vuex` 和 `localStorage` 之间的**区别**🔎：

|                       | `Vuex`                                           | `localStorage`                                               |
| :-------------------: | ------------------------------------------------ | ------------------------------------------------------------ |
| 存储方式（**主要**💣） | 存储在**内存**中，**读取内存比读取硬盘速度要快** | 以文件的方式**存储在本地硬盘中**，只能存储**字符串类型**的数据，存储对象需要 `JSON` 的 `stringify()` 和 `parse()` 方法进行处理 |
|       应用场景📣       | 用于**组件之间**的传值，能做到数据的**响应式**   | 一般是在**跨页面**传递数据时使用，但不能做到数据的响应式     |
|        永久性         | **刷新页面时，`Vuex` 存储的值会丢失**            | 刷新页面时，存储的值**不会丢失**                             |

⚠需要注意的是，对于不变的数据确实可以用 `localStorage` 可以代替 `Vuex`，但是**当两个组件共用一个数据源（对象或数组）时，如果其中一个组件改变了该数据源，希望另一个组件也响应变化时，`localStorage` 无法做到**。



### State

> 提供公共的数据源，所有共享的状态数据统一放到 `state` 中进行储存

```js
const store = new Vuex.Store({
  state: {
    // 管理数据
    count: 0
  }
})
```

组件访问 `state` 数据的方式如下🗃：

- 方式一：**通过 `$store.state` 访问**

（1）**插值表达式**

```html
<div> count：{{ $store.state.count }}</div>
```

（2）**将 `state` 属性定义在计算属性中**

```js
computed: {
	count() {
		return this.$store.state.count
	}
}
```

```html
<div> count：{{ count }}</div>
```

- 方式二：**`mapState` 辅助函数**

> 当一个组件需要获取多个状态的时候，将这些状态都声明为计算属性会有些重复和冗余，为了解决这个问题，可以使用 `mapState` 辅助函数帮助我们生成计算属性

```js
import { mapState } from 'vuex'
export default {
	computed: {
		...mapState(['count'])
		// 等价于
		// count() {
		//		return this.$store.state.count
		// }
	}
}
```



### Getter

> 在 `state` 中的数据的基础上，进一步对数据进行加工得到新数据，可以认为是 **`store` 的计算属性**，像计算属性一样，**`getter` 的返回值会根据它的依赖被缓存起来，且只有当它的依赖值发生了改变才会被重新计算**
>
> `getter` 接受 `state` 作为其第一个参数，其他 `getter` 作为第二个参数，⚠需要注意的几点：
>
> - `getter` 在通过**属性访问**时是作为 `Vue` 的响应式系统的一部分**缓存其中**的
> - `getter` 在通过**方法访问**时，每次都会去进行调用，而**不会缓存结果**

```js
const store = new Vuex.Store({
  state: {
    list: [1,2,3,4,5,6,7,8,9,10]
  },
  getters: {
    // 必须要有返回值
    // 1.通过属性访问
    filterList: state => state.list.filter(item => item > 5),
    filterListLength: (state, getters) => getters.filterList.length,
    // 2.通过方法访问
    getListItem: state => index => {
    	return state.list[index]
  	}
  }
})
```

组件访问 `getters` 数据的方式如下🗃：

- 方式一：**通过 `$store.getters` 访问**

（1）**插值表达式**

```html
<div> length：{{ $store.getters.filterListLength }}</div>
<div> item：{{ $store.getters.getListItem(2) }}</div>
```

（2）**将 `getters` 属性定义在计算属性中**

```js
computed: {
	filterListLength() {
		return this.$store.getters.filterListLength
	}
}
```

```html
<div> length：{{ filterListLength }}</div>
```

- 方式二：**`mapGetters` 辅助函数**

> 将 `store` 中的 `getter` 映射到局部计算属性

```js
import { mapGetters } from 'vuex'
computed: {
	...mapGetters(['filterList', 'filterListLength'])
}
```



### Mutation

> **更改 `Vuex` 的 `store` 中的状态的唯一方法是提交 `mutation`，`mutation` 中方法名不能重名，并且 `mutation` 只能进行同步操作**🧨，如果有异步请求，应该放置在 `actions` 中
>
> `mutation` 会接受 `state` 作为第一个参数，载荷 `payload`（即传入额外的参数）作为第二个参数，在大多数情况下，载荷应该是一个对象

**`mutation` 需遵守 `Vue` 的响应规则🛠：**

- 最好提前在 `store` 中初始化好所有所需属性

- 当需要在对象上添加新属性时，应该采用下面两种方式：

  - 使用 `Vue.set(obj, 'newProp', 123)`

  - 以新对象替换老对象


```js
state.obj = { ...state.obj, newProp: 123 }
```

👍命名建议：**使用常量替代 `mutation` 方法名**

```js
const store  = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    addCount: (state, num = 1) => {
      state.count += num
    },
    REDUCE_COUNT: state => {
      state.count -= 1
    }
  }
})
```

组件提交 `mutation` 的方式如下🗃：

- 方式一：**通过 `$store.commit` 提交 `mutation`**

```html
<div @click="$store.commit('REDUCE_COUNT')"></div>
<div @click="$store.commit('addCount', 5)"></div>
<div @click="addCount(5)"></div>
```

```js
methods: {
	addCount(num) {
		this.$store.commit('addCount', num)
	}
}
```

- 方式二：**`mapMutations` 辅助函数**

> 将组件中的 `methods` 映射为 `store.commit` 调用

```js
import { mapMutations } from 'vuex'

methods: {
	...mapMutations(['addCount'])
  // 将this.addCount()映射为this.$store.commit('addCount')
  // 将this.addCount(100)映射为this.$store.commit('addCount', 100)
}
```



### Action

> `action` 类似于 `mutation` ，不同点在于 **`action` 间接通过提交 `mutation` 来修改 `state `，不能直接修改 `state` ，并且🎃可以包含任意同步或异步操作，支持多个同名方法，按照注册的顺序依次触发**
>
> `action` 会接受一个与 `store` 实例具有相同方法和属性的 `context` 对象作为第一个参数，可以调用 `context.commit` 提交一个 `mutation`，或者通过 `context.state` 和 `context.getters` 来获取 `state` 和 `getters`，还有载荷 `payload`（即传入额外的参数）作为第二个参数，在多数情况下，载荷应该是一个对象

```js
const store  = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    addCount: (state, num = 1) => {
      state.count += num
    }
  },
  actions: {
    getAsyncCount({ commit }, num) {
      setTimeout(() => {
        commit('addCount', num)
      }, 1000)
    }
 }
})
```

组件分发 `actions` 的方式如下🗃：

- 方式一：**通过 `$store.dispatch` 分发 `action`**

```html
<div @click="$store.dispatch('getAsyncCount')"></div>
<div @click="$store.dispatch('getAsyncCount', 5)"></div>
<div @click="addAsyncCount(5)"></div>
```

```js
methods: {
	addAsyncCount(num) {
		this.$store.dispatch('getAsyncCount', num)
 	}
}
```

另外，**`store.dispatch` 可以处理被触发的 `action` 的处理函数返回的 `Promise`，并且 `store.dispatch` 仍旧返回 `Promise`，支持 `action` 的链式触发。**

- 方式二：**`mapActions` 辅助函数**

> 将组件的 `methods` 映射为 `store.dispatch` 调用

```js
import { mapActions } from 'vuex'

methods: {
	...mapActions(['getAsyncCount'])
  // 将this.getAsyncCount()映射为this.$store.dispatch('getAsyncCount')
  // 将this.getAsyncCount(100)映射为this.$store.dispatch('getAsyncCount', 100)
}
```



### Module

> 所有的全局数据、方法都集中在一起，导致 `Vuex` 的结构混乱，不利于开发和后期的维护
>
> ✔为了解决以上问题，`Vuex` 允许将 `store` 分割成模块（`module`），每个模块拥有自己的 `state`、`mutation`、`action`、`getter` ，甚至是嵌套子模块

![Vuex中的模块化](images/Vuex中的模块化.png)

如果希望模块具有更高的封装度和复用性，可以**通过添加 `namespaced: true` 的方式使其成为带命名空间的模块**，当模块被注册后，它的所有 `getter`、`action` 及 `mutation` 都会自动根据模块注册的路径调整命名。

有时可能需要创建一个模块的多个实例，🌰例如：

1. 创建多个 `store`，它们公用同一个模块
2. 在一个 `store` 中多次注册同一个模块

⚠要注意的是，**如果使用一个纯对象来声明模块的状态，那么这个状态对象会通过引用被共享，导致状态对象被修改时 `store` 或模块间数据互相污染的问题。**

和 `Vue` 组件内的 `data` 属性是同样的问题，解决办法也是相同的，✔**使用一个函数来声明模块状态**：

```js
// 模块user
const user = {
  namespaced: true,
  state: () => ({
  	token: '12345'
  }),
  mutations: {
    // 这里的state表示的是user的state
    updateToken (state) {
			state.token = 678910
    }
  },
  actions: { ... }
}

// 模块setting
const setting = {
  namespaced: true,
  state: () => ({
    name: 'Vuex实例'
  }),
  mutations: { ... },
  actions: { ... }
}

// 根节点getters
const getters = { ... }

const store = new Vuex.Store({
  modules: {
    user,
    setting
  },
  getters
})
```

（1）访问模块中的 `state`、`getters` 数据，有以下几种方式🗃：

- 方式一：**通过 `$store.state.模块名.属性名` 访问**

```html
<div>用户token {{ $store.state.user.token }}</div>
<div>网站名称 {{ $store.state.setting.name }}</div>
```

- 方式二：**通过 `mapGetters` 辅助函数引用根节点 `getters`**

```js
const getters = {
	token: state => state.user.token,
	name: state => state.setting.name
}

const store = new Vuex.Store({
  modules: { ... },
  getters
})
```

```js
computed: {
	...mapGetters(['token', 'name'])
	// this.$store.getters.token
	// this.$store.getters.name
}
```

- 方式三： **在 `mapState`、`mapGetters` 辅助函数中，传入模块名作为第一个参数**，则自动将该模块作为上下文

```js
computed: {
	...mapState('user', ['token']), // this.$store.state.user.token
	...mapState('setting', ['name']), // this.$store.state.setting.name   
}
```

- 方式四：**使用 `createNamespacedHelpers` 创建基于某个命名空间辅助函数**

```js
import { createNamespacedHelpers } from 'vuex'
const { mapState } = createNamespacedHelpers('user')

export default {
  methods: {
    ...mapState(['token'])
  }
}
```

（2）使用模块中的 `mutations`、`actions` 方法，有以下几种方式🗃：

- 方式一：**带上模块名直接调用**

```js
updateToken() {
   this.$store.dispatch('user/updateToken')
}
```

- 方式二：**在 `mapMutations`、`mapActions` 辅助函数中，传入模块名作为第一个参数**，则自动将该模块作为上下文

```js
methods: {
  ...mapMutations('user', ['updateToken'])
	// this.updateToken() => this.$store.commit('user/updateToken')
}
```

- 方式三：**使用 `createNamespacedHelpers` 创建基于某个命名空间辅助函数**

```js
import { createNamespacedHelpers } from 'vuex'
const { mapMutations } = createNamespacedHelpers('user')

export default {
  methods: {
    ...mapMutations(['updateToken'])
  }
}
```



------



## Pinia

> 🔗`Pinia` 官方中文文档：https://pinia.vuejs.org/zh/

> `Pinia` 是 `Vue` 专属的最新**状态管理**库，可以**实现跨组件或页面共享状态**，它是 `Vuex` 状态管理工具的替代品，与 `Vuex` 相比，具备以下优势🎏：
>
> 1. `Vue2` 和 `Vue3` 语法都能支持
> 2. **抛弃传统的 `Mutation`，只有 `state, getter` 和 `action`，简化状态管理库**，提供更加简单的 `API`
> 3. `pinia` 中 **`action` 支持同步和异步**，`Vuex` 不能同时支持
> 4. **去掉 `modules` 的概念，每一个 `store` 都是一个独立的模块，可以创建多个全局仓库，不用像 `Vuex` 中的 `store` 嵌套多个 `modules`，结构非常复杂**
> 5. 良好的 `Typescript` 支持，搭配 `TypeScript` 一起使用时有非常可靠的类型推断支持
> 6. 体积非常小，只有 `1KB` 左右

### 基本使用

（1）安装 `pinia`

```tex
npm install pinia
```

（2）在 `main.js` 中引入和挂载 `pinia`

```js
import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";

const pinia = createPinia();

createApp(App).use(pinia).mount("#app");
```



### Setup Store

> **`store` 是用 `defineStore()` 函数定义的**，它的**第一个参数**要求是一个**独一无二的名字**，并且是必须传入的，类似于模块名，它**是 `store` 的唯一 `ID`**，**第二个参数**是与 `Vue3` 组合式 `API` 的 `setup()` 函数相似，可以**传入一个函数**，该函数定义了一些响应式属性和方法，并且**返回一个带有想暴露出去的属性和方法的对象**，**`defineStore()` 函数的返回值也是一个函数，暴露给外部使用**
>
> 👍函数命名推荐：**小驼峰命名法，并以 `use` 开头和以 `Store` 结尾**
>
> 在 `Setup Store` 中📦：
>
> - `ref()` 就是 `state` 
> - `computed()` 就是 `getter`
> - `function()` 就是 `action`
>
> **一旦 `store` 被实例化，可以直接访问在 `store` 的 `state`、`getter` 和 `action` 中定义的任何属性**

```js
import { defineStore } from "pinia"
import { ref, computed } from "vue"

export const useCounterStore = defineStore('counter', () => {
  const count = ref(100)
  const doubleCount = computed(() => count.value * 2)
  const increment = () => {
    count.value++
  }
  const addNum = (num) => {
    count.value += num
  }
  const asyncIncrement = () => {
    setTimeout(() => {
      count.value++
    }, 1000)
  }

  return { count, doubleCount, increment, addNum, asyncIncrement }
})
```



### State

#### 获取 state

> 获取 `state` 除了 `store.属性名` 这种方式之外，还有 `computed()` 计算属性和解构两种方式

```vue
<script setup>
import { useCounterStore } from "../store";
import { computed } from "vue"
import { storeToRefs } from 'pinia'

const counterStore = useCounterStore();
// 计算属性
const countCom = computed(() => counterStore.count)
// 解构
const { count } = storeToRefs(counterStore)
</script>

<template>
  <div>普通方式 {{ counterStore.count }}</div>
	<div>计算属性 {{ countCom }}</div>
	<div>解构 {{ count }}</div>
</template>
```

#### 修改 state

> 默认情况下，可通过 `store` 实例访问 `state`，**直接对其进行读写**

```vue
<script setup>
import { useCounterStore } from "../store";

const counterStore = useCounterStore();
const updateCount = () => {
  // 直接进行读写操作
  counterStore.count++;
};
</script>

<template>
  <div>{{ counterStore.count }}</div>
	<button @click="updateCount">count+1</button>
</template>
```

除了用 `store.count++` 直接改变 `store`，**还可以调用 `$patch` 方法，它允许用一个 `state` 的补丁对象或者函数在同一时间更改多个属性**：

```js
// 1.对象方式
counterStore.$patch({
  count: counterStore.count + 1,
  age: 120,
  name: 'DIO',
})

// 2.函数方式
counterStore.$patch((state) => {
  state.items.push({ name: 'shoes', quantity: 1 })
  state.hasChanged = true
})
```

但以上两种方式一般不建议这么做，👍**推荐通过 `action` 去修改 `state`**：

```vue
<script setup>
import { useCounterStore } from "../store";

const counterStore = useCounterStore();
</script>

<template>
  <div>{{ counterStore.count }}</div>
	<button @click="counterStore.increment">count+1</button>
</template>
```



### Getter

> 与 `state` 完全一样，可以直接访问任何 `getter`，和 `Vue` 中的计算属性几乎一样，也**具有缓存特性**

```vue
<script setup>
import { useCounterStore } from "../store";

const counterStore = useCounterStore();
</script>

<template>
  <div>{{ counterStore.count }}</div>
	<div>{{ counterStore.doubleCount }}</div>
	<button @click="counterStore.increment">count+1</button>
</template>
```

**想要使用另一个 `store` 的 `getter` 的话，引入对应的 `store` 后直接在 `getter` 内使用就好**：

```js
import { computed } from "vue"
import { useOtherStore } from './other-store'

export const useCounterStore = defineStore('counter', () => {
	// ...
	const otherGetter = computed(() => {
    const otherStore = useOtherStore()
    return otherStore.getterdata
  })
})
```



### Action

> `action` 可以像函数或者通常意义上的方法一样被调用，同时**支持同步和异步操作**🚩

```vue
<script setup>
import { useCounterStore } from "../store";

const counterStore = useCounterStore();
</script>

<template>
  <div>{{ counterStore.count }}</div>
	<button @click="counterStore.increment">同步+1</button>
	<button @click="counterStore.addNum(10)">同步+10</button>
	<button @click="counterStore.asyncIncrement">异步+1</button>
</template>
```

与 `getter` 一样，**想要使用另一个 `store` 的 `action` 的话，引入对应的 `store` 后直接在 `action` 中调用就好**：

```js
import { useAuthStore } from './auth-store'

export const useCounterStore = defineStore('counter', () => {
	const fetchFunc = async () => {
		const auth = useAuthStore()
		const res = await fetchAPI()
    // ...
  }
})
```



### storeToRefs 函数

> **`store` 是一个用 `reactive` 包装的对象，像 `setup` 中直接解构 `props` 一样，会失去其响应性**，为了从 `store` 中提取**状态**（`state` 和 `getter`）时保持其响应性，需要使用到 `storeToRefs()` 函数
>
> ⚠注意：直接从 `store` 中解构 `action` 即可，**使用 `storeToRefs()` 函数从 `store` 中解构 `action` 会报错，因为它会跳过所有的 `action` 或非响应式 (不是 `ref` 或 `reactive`) 的属性**

```vue
<script setup>
import { useCounterStore } from "../store";
import { storeToRefs } from 'pinia'
  
const counterStore = useCounterStore()
const { count, doubleCount } = storeToRefs(counterStore)
// 作为action的increment可以直接解构
const { increment } = counterStore
</script>
```



### 模块化

> 在复杂项目中，不可能将多个模块的数据都定义到一个 `store` 中，一般来说一个模块对应一个 `store`，可以通过导入和使用另一个 `store` 来隐含地嵌套 `stores` 空间，即通过一个根 `store` 进行整合

（1）新建 `store/modules/user.js` 和 `store/modules/counter.js` 文件

```js
// src/store/modules/user.js
import { defineStore } from "pinia";
import { ref } from "vue";

export const useUserStore = defineStore("user", () => {
  const name = ref("张三");
  const age = ref(18);

  return { name, age };
});
```

```js
// src/store/modules/counter.js
import { defineStore } from "pinia";
import { ref, computed } from "vue";

export const useCounterStore = defineStore("counter", () => {
  const count = ref(100);
  const doubleCount = computed(() => count.value * 2);
  const increment = () => {
    count.value++;
  };
  const addNum = (num) => {
    count.value += num;
  };
  const asyncIncrement = () => {
    setTimeout(() => {
      count.value++;
    }, 1000);
  };

  return { count, doubleCount, increment, addNum, asyncIncrement };
});
```

（2）新建 `store/index.js` 文件

```js
// src/store/index.js
// 建立总主管，引入各个模块的数据，并通过export暴露给外部
export * from "./modules/user";
export * from "./modules/counter";
```

（3）在组件中使用时，直接引入总主管，解构出对应的模块方法即可

```vue
<script setup>
import { useUserStore, useCounterStore } from "../store";
import { storeToRefs } from "pinia";
const userStore = useUserStore();
const counterStore = useCounterStore();

const { name, age } = storeToRefs(userStore);
const { count, doubleCount } = storeToRefs(counterStore);
const { increment } = counterStore;
</script>

<template>
  <div>name: {{ name }}</div>
  <div>age: {{ age }}</div>
  <div>count: {{ count }}</div>
  <div>doubleCount: {{ doubleCount }}</div>
  <button @click="increment">count+1</button>
</template>
```



### 数据持久化

#### $subscribe 和本地存储搭配使用

（1）订阅 `store` 中的数据变化

> 默认情况下，订阅器会被绑定到添加它们的组件上 （如果 `store` 在组件的 `setup()` 里面），这意味着，当该组件被卸载时，它们将被自动删除
>
> **如果你想在组件卸载后依旧保留它们，请将 `{ detached: true }` 作为第二个参数**，以将订阅器从当前组件中分离

```js
import { useCounterStore } from "../store";
const counterStore = useCounterStore();

counterStore.$subscribe(() => {
  localStorage.setItem('count', JSON.stringify(counterStore.count))
});
```

```js
// 此订阅器即便在组件卸载之后仍会被保留
counterStore.$subscribe(() => {
  localStorage.setItem('count', JSON.stringify(counterStore.count))
}, { detached: true });
```

（2）获取数据时，先从本地存储中获取

```js
export const useCounterStore = defineStore("counter", () => {
  const count = ref(JSON.parse(localStorage.getItem("count")) || 100);
  // ...
});
```

#### pinia-plugin-persistedstate 插件

> 🔗插件文档：https://prazdevs.github.io/pinia-plugin-persistedstate/zh/

> 还有另一个 `pinia-plugin-persist` 插件，语法相似，**两者择一即可**

（1）安装插件

```tex
npm i pinia-plugin-persistedstate
```

（2）引入插件

```js
import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

createApp(App).use(pinia).mount('#app')
```

（3）使用插件

- 默认配置

> 无需额外处理，插件会自己更新到最新数据，其**默认配置**如下🛠:
>
> 1. **使用 `localStorage` 进行存储**
> 2. `store.$id` 作为 `storage` 默认的 `key`
> 3. 使用 `JSON.stringify`/`JSON.parse` 进行序列化/反序列化
> 4. **整个 `state` 默认将被持久化**

```js
import { defineStore } from "pinia";
import { ref, computed } from "vue";

export const useCounterStore = defineStore(
  "counter",
  () => {
    // ...
  },
  {
    persist: true,
  }
);
```

- 设置自定义 `key`、指定存储方式和保存内容

```js
import { defineStore } from "pinia";
import { ref } from "vue";

export const useUserStore = defineStore(
  "user",
  () => {
    const userInfo = ref({
      name: "张三",
      age: 18,
    });
    const userId = ref(123456);
    // ...
  },
  {
    persist: {
			key: 'userStore',
			storage: sessionStorage,
			// []表示不持久化任何状态，undefined或null表示持久化整个state
      // 只有userInfo.age和userId被持久化，而userInfo.name不会被持久化
			paths: ['userInfo.age', 'userId']
    }
  }
);
```



### 组件外的 Store

> 大多数时候，只需调用定义好的 `useStore()` 函数，完全开箱即用，例如在 `setup()` 中，不需要再做任何事情，但在组件之外，情况就有点不同， 实际上 `useStore()` 会给 `app` 自动注入了 `pinia` 实例，这意味着如果 `pinia` 实例不能自动注入，必须手动提供给 `useStore()` 函数

在 `Vue Router` 的导航守卫中使用 `store` 的例子🌰：

```typescript
import { createRouter } from 'vue-router'
const router = createRouter({
  // ...
})

// ❌ 由于引入顺序的问题，这将失败
const store = useStore()
router.beforeEach((to, from, next) => {
  // 我们想要在这里使用store
  if (store.isLoggedIn) next()
  else next('/login')
})

router.beforeEach((to) => {
  // ✅ 这样做是可行的，因为路由器是在其被安装之后开始导航的
  // 而此时Pinia也已经被安装。
  const store = useStore()

  if (to.meta.requiresAuth && !store.isLoggedIn) return '/login'
})
```



------



## Vite

> 🔗`Vite` 官方中文文档：https://cn.vitejs.dev/

> `Vite` 是一种新型前端构建工具，能够显著提升前端开发体验，其**优势**如下🚩：
>
> 1. **极速的服务启动，使用原生 `ESM` 文件，无需打包**
> 2. 轻量快速的热重载，始终**极快的模块热重载**（`HMR`）
> 3. 丰富的功能，**对 `TypeScript` 、`JSX`、`CSS` 等支持开箱即用**
> 4. ....等等

- **传统方式**

> 基于**打包器**的方式启动，必须优先抓取并构建你的整个应用，然后才能提供服务，**更新速度会随着应用体积增长而直线下降**

![基于Bundle的开发服务器](images/基于Bundle的开发服务器.png)

- **`Vite` 方式**

> `Vite` 以**原生 `ESM`**（原生 `ECMAScript module`，也就是 `import`、`export`）方式提供源码，这实际上是让浏览器接管了打包程序的部分工作：**`Vite` 只需要在浏览器请求源码时进行转换并按需提供源码，根据情景动态导入代码，即只在当前屏幕上实际使用时才会被处理**

![基于原生ESM的开发服务器](images/基于原生ESM的开发服务器.png)

- **`Vite` 与 `Webpack` 的关系**

|                          `Webpack`                           |                            `Vite`                            |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| 目前整个前端使用最多的构建工具，**先打包，再起服务**，全能选手，啥都能干，只是配置有点复杂，打包也比较慢<br /> | 一种新型前端构建工具，**先起服务，使用到了模块再引入**，能够显著提升前端开发体验 |
