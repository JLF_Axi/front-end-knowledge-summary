# 🌭JavaScript🌭

## 变量提升

> 通俗来说，变量提升是指**在 `JS` 代码编译过程中，`JS` 引擎把变量的声明部分和函数的声明部分提升到代码开头的行为，当变量被提升后，会给变量设置默认值为 `undefined`，而声明函数前执行函数，则会直接执行函数**
>
> 这是 `JS` 的一个设计缺陷，通过引入**块级作用域并配合使用 `let`、`const` 关键字，可避开这种设计缺陷**

在全局作用域中声明一个 `num` 变量，并在声明之前打印它：

```js
console.log(num) 
var num = 1
```

这里会输出 `undefined`，因为变量的声明被提升了，它等价于：

```js
var num
console.log(num)
num = 1
```

除此之外，**在函数作用域中也存在变量提升**：

```js
function getNum() {
	console.log(num) 
  var num = 1  
}
getNum()
```

这里也会输出 `undefined`，因为函数内部的变量声明会被提升至函数作用域的顶端，它等价于：

```js
function getNum() {
	var num 
  console.log(num) 
  num = 1  
}
getNum()
```

**除了变量提升，函数实际上也是存在提升的**，`JS` 具名函数的声明形式有两种：

```js
// 函数声明式
function foo() {}
// 变量形式声明
var fn = function() {}
```

**当使用变量形式声明函数时，和普通的变量一样会存在提升的现象，而函数声明式会提升到作用域最前边，并且将声明内容一起提升到最上边**，如下所示：

```js
fn() // Uncaught TypeError: fn is not a function
var fn = function() {
	console.log(1)  
}

foo() // 2
function foo() {
	console.log(2)
}
```

⚠需要注意的是，**当有多个同名变量声明的时候，函数声明会覆盖其他的声明，而如果有多个函数声明，则是由最后的一个函数声明覆盖之前所有的声明。**

### 暂时性死区

> 在 `ES6` 中，引入了 `let` 和 `const` 两个新的命令，并且使用这两个命令定义的变量不存在变量提升，且**使用 `let` 和 `const` 声明变量之前，该变量都是不可用的**，这在语法上被称为暂时性死区

```js
console.log(a)
let a = 100  // Uncaught ReferenceError: Cannot access 'name' before initialization
```



## 数据类型

> 目前 `JS` 数据类型总共有 `8` 种，按照类型来分为基本数据类型和引用数据类型：
>
> - **基本数据类型**：`String`、`Number`、`Boolean`、`Null`、`Undefined`、`Symbol`、`BigInt`
> - **引用数据类型**：`Object`（其中也包含 `Array`、`Function`、`Date`、`RegExp` 等）

（1）`Null` 与 `Undefined` 区别

|                         `undefined`                          |                            `null`                            |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| `undefined` 代表的含义是未定义，**一般变量声明却还没有定义的时候会返回 `undefined`**，`undefined` 在 `JavaScript` 中不是一个保留字，这意味着可以使用 `undefined` 来作为一个变量名，但是这样的做法是非常危险的，它会影响对 `undefined` 值的判断，我们可以通过一些方法获得安全的 `undefined` 值，比如说 `void 0` | `null` 代表的含义是空值，**主要用于赋值给一些可能会返回值的变量，作为初始化值** |

两种类型在**进行逻辑判断**时：

```js
null == undefined // true => 值都为空
null === undefined // false => 值相同但类型不同
```

如果**进行相连或相加**时，**⚠需要注意结果**：

```js
let v1;
console.log(v1); // undefined 
console.log('阿牛' + v1); // 阿牛undefined 
console.log(11 + v1); // NaN 
console.log(true + v1); // NaN 

let v2 = null;
console.log (v2); // null
console.log ('阿牛' + v2); // 阿牛null 
console.log (11 + v2); // 11
console.log (true + v2); // 1
```

（2）基本数据类型与引用数据类型区别

|                         基本数据类型                         |                         引用数据类型                         |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| **数据存储在栈内存中**，在变量拷贝时，会将数据直接赋值给新变量，两个变量有相同的值，但**彼此间互不影响** | **将内存地址存储在栈内存中，而真实数据存储在堆内存中**，在变量拷贝时，会将内存地址赋值给新变量，两个变量都指向同一内存地址的数据，**一旦一方修改，另一方也会受到影响** |

### 类型检测

（1）**`typeof` 运算符**

|     类型     |      结果      |
| :----------: | :------------: |
|  Undefined   | `"undefined"`  |
|  🚨**Null**   | **`"object"`** |
|   Boolean    |  `"boolean"`   |
|    Number    |   `"number"`   |
|    BigInt    |   `"bigint"`   |
|    String    |   `"string"`   |
|    Symbol    |   `"symbol"`   |
|   Function   |  `"function"`  |
| 其他任何对象 |   `"object"`   |

但是 `typeof` 运算符也有一点局限性，**⚙`typeof` 运算符只能区分基本数据类型**，对于 `Null`、`Array`、`Object` 来说，使用 `typeof` 运算符都会统一返回 `"object"` 字符串。

（2）**`instanceof` 运算符**

**`instanceof` 运算符只能正确判断引用数据类型**，而不能判断基本数据类型，其内部运行机制🛠是**判断构造函数的 `prototype` 属性是否出现在某个实例对象的原型链上的任何位置**。

```js
console.log (2 instanceof Number); // false
console.log (true instanceof Boolean); // false
console.log ('str' instanceof String); // false

console.log ([] instanceof Array); // true
console.log (function() {} instanceof Function); // true
console.log ({} instanceof Object); // true
```

（3）**`constructor` 属性**

`constructor` 属性可以用来判断数据的类型，还有另一个作用是实例对象通过 `constrcutor` 属性访问它的构造函数。

```js
console.log ((2).constructor === Number); // true
console.log ((true).constructor === Boolean); // true
console.log (('str').constructor === String); // true
console.log (([]).constructor === Array); // true
console.log ((function() {}).constructor === Function); // true
console.log (({}).constructor === Object); // true
```

⚠需要注意的是，**如果改变了它的原型，`constructor` 就不能用来判断数据类型。**

```js
function Fn() {};
Fn.prototype = new Array();
let f = new Fn();

console.log (f.constructor === Fn); // false
console.log (f.constructor === Array); // true
```

（4）**`Object.prototype.toString.call().slice(8, -1)` 方法**（**推荐👍**）

|   数据类型   | **字符串**结果 |
| :----------: | :------------: |
| `Undefined`  | `"Undefined"`  |
|    `Null`    |    `"Null"`    |
|  `Boolean`   |  `"Boolean"`   |
|   `Number`   |   `"Number"`   |
|   `BigInt`   |   `"BigInt"`   |
|   `String`   |   `"String"`   |
|   `Symbol`   |   `"Symbol"`   |
|   `Array`    |   `"Array"`    |
|  `Function`  |  `"Function"`  |
|    `Date`    |    `"Date"`    |
|   `RegExp`   |   `"RegExp"`   |
| 其他任何对象 |   `"Object"`   |

### 类型转换

> 在 `JS` 中，类型转换可分为显式和隐式两种

#### 显式转换

（1）**转换成 `Number` 型**

|      方法      | 说明                                                         |
| :------------: | ------------------------------------------------------------ |
|  `parseInt()`  | 从左到右解析，**遇到非数字结束，将解析好的整数返回**         |
| `parseFloat()` | 与 `parseInt()` 最大区别是**可解析字符串的第一位小数点**     |
|   `Number()`   | 可以将任何值转换成数值，但**如果要转换的字符串只要有一个不是数字，则返回 `NaN`**，比如 `""` 空字符串或 `null` 转换成 `0`，`undefined` 转换成 `NaN` |

📣一般情况下，**数字字符串使用 `parseInt()` 和 `parseFloat()`，其他类型转换数字使用 `Number()`。**

（2）**转换成 `String` 型**

|        方法         | 说明                                                         |
| :-----------------: | :----------------------------------------------------------- |
| `变量名.toString()` | **如果变量值为 `undefined` 或 `null`，则会报错**             |
|     `String()`      | **如果变量值为 `undefined` 或 `null`，不会报错**，会得到 `"undefined"` 或 `"null"` 字符串 |

（3）**转换成 `Boolean` 型**

|    方法     | 说明                                                         |
| :---------: | ------------------------------------------------------------ |
| `Boolean()` | **代表空、否定的值会被转换成 `false`，比如 `""`、`±0`、`NaN`、`null`、`undefined` 还有 `document.all` 等**，其余值都会被转换成 `true` |

#### 隐式转换

> 在 `if` 语句、逻辑语句、数学运算逻辑、`==/===` 等情况下都可能出现隐式类型转换

|      原始值       | 转换为 `Number` 类型 |  转换为 `String` 类型  | 转换为 `Boolean` 类型 |
| :---------------: | :------------------: | :--------------------: | :-------------------: |
|       false       |          0           |        "false"         |         false         |
|       true        |          1           |         "true"         |         true          |
|         0         |          0           |          "0"           |         false         |
|         1         |          1           |          "1"           |         true          |
|        "0"        |          0           |          "0"           |         true          |
|        "1"        |          1           |          "1"           |         true          |
|        NaN        |         NaN          |         "NaN"          |      🚨**false**       |
|     Infinity      |       Infinity       |       "Infinity"       |         true          |
|     -Infinity     |      -Infinity       |      "-Infinity"       |         true          |
|        ""         |          0           |           ""           |         false         |
|       "abc"       |         NaN          |         "abc"          |         true          |
|        []         |        🚨**0**        |        🚨**""**         |       🚨**true**       |
|       [20]        |          20          |          "20"          |         true          |
|     [10, 20]      |         NaN          |        "10, 20"        |         true          |
|      ["ten"]      |         NaN          |         "ten"          |         true          |
| ["ten", "twenty"] |         NaN          |     "ten, twenty"      |         true          |
|   function() {}   |         NaN          |    "function() {}"     |         true          |
|        {}         |       🚨**NaN**       | 🚨**"[object Object]"** |       🚨**true**       |
|       null        |        🚨**0**        |         "null"         |         false         |
|     undefined     |       🚨**NaN**       |      "undefined"       |         false         |

### 基本包装类型

在 `JS` 中，基本数据类型是没有属性和方法，但是为了便于操作基本数据类型的值，`JS` 提供了三个特殊的引用类型：`Number`、`String` 和 `Boolean`，**在调用基本数据类型的属性或方法时， `JS` 会在后台隐式地将基本数据类型的值转换为对象**，如下所示：

```js
let str = 'test';
let str2 = str.substring(2);
console.log(str2); //st
```

在执行第二行代码时，`JS` 会自动进行下面的步骤：

```js
let str = new String('test');
let str2 = str.substring(2)
str = null // 用后即焚
```

**基本包装类型和引用类型的区别：两者的生存期不同**

- 基本包装类型在方法执行结束后立即销毁
- 对象在释放内存后销毁



## 同步和异步

> **⛰`JS` 三大山之一**，`JS` 语言的一大特点是**单线程**，也就是说，**在同一时间只能做一件事**，意味着所有任务需要排队，**后面的任务需要等到前面的任务完成之后才能执行，如果前面的任务很耗时，就会导致后面的任务一直等待，页面的渲染不连贯，就会有页面渲染加载阻塞的感觉**
>
> 于是为了解决这个问题，`JS` 中出现了**同步任务**和**异步任务**

![JS任务](images/JS任务.png)

- **<u>同步任务</u>**

在主线程上排队执行的任务形成了一个执行栈，只有前一个任务执行完毕，才能执行后一个任务，比如当打开一个网站时，网站渲染过程中的元素渲染，其实就是一个同步任务。

- **<u>异步任务</u>**

不进入主线程，而是进入**任务队列**的任务，只有当主线程中的任务执行完毕，才从任务队列中取出任务放进主线程中来执行，比如当打开一个网站时，像图片和音乐的加载，其实就是一个异步任务。

而 `JS` 又将异步任务做了进一步的划分，分为宏任务和微任务，其中**宏任务是由宿主（浏览器、`Node`）发起的，而微任务由 `JS` 自身发起。**

|                         **宏任务**                          | 浏览器 | `Node` |
| :---------------------------------------------------------: | :----: | :----: |
|                    `<script>` 整体代码块                    |   ✔    |   ✔    |
|             `setTimeout`、`setInterval` 定时器              |   ✔    |   ✔    |
|                   `setImmediate` 立刻执行                   |   ❌    |   ✔    |
| `requestAnimationFrame`（要求浏览器在下次重绘之前更新动画） |   ✔    |   ❌    |
|                      异步 `AJAX` 请求                       |   ✔    |   ✔    |
|                         `DOM` 事件                          |   ✔    |   ✔    |
|          `I/O` 操作（输入输出，比如读取文件操作）           |   ✔    |   ✔    |

|                **微任务**                 | 浏览器 | `Node` |
| :---------------------------------------: | :----: | :----: |
|  `Promise.then`、`.catch` 和 `.finally`   |   ✔    |   ✔    |
| `MutationObserver`（监听 `DOM` 树的变化） |   ✔    |   ❌    |
|            `process.nextTick`             |   ❌    |   ✔    |

- **<u>宏任务和微任务的执行顺序</u>**

![宏任务和微任务的执行顺序](images/宏任务和微任务的执行顺序.png)

每一个宏任务执行完之后，都会检查是否存在待执行的微任务。如果有，则依次执行完所有微任务之后，再继续执行下一个宏任务（**即完成一次事件循环**）。

- **<u>事件循环</u>**

在执行 `JS` 代码时，会先执行主线程上的所有**同步任务**，遇到**异步宏任务**则将异步宏任务放入下一个**宏任务队列**中，遇到**异步微任务**则将异步微任务放入**微任务队列**中，当所有同步任务执行完毕后，判断微任务队列中是否有任务可以执行，如果有，则依次将所有异步微任务从队列中调入主线程执行，待所有微任务执行完毕后，再将异步宏任务从队列中调入主线程执行，一直循环至所有的任务执行完毕。

由于主线程不断重复地获取任务、执行任务、再获取、再执行，所以这种异步机制被叫做**事件循环（`Event Loop`）**，**保证了异步任务不会阻塞主线程，避免了代码的阻塞和死锁。**

**⚠注意点：**

1. 主线程上的同步任务可以看做第一个宏任务
2. 宏任务队列可以有多个，但微任务队列只有一个
3. **一次事件循环只能处理一个宏任务，一次事件循环可以将所有的微任务处理完毕**

⭕练习题1：

```js
console.log(1)

setTimeout(function() {
	console.log(2) // 宏任务2
}, 0)

const p = new Promise((resolve, reject) => {
	resolve(1000)
})
p.then(data => {
	console.log(data)  // 微任务
})

console.log(3)

// 输出结果：1 3 1000 2
```

![宏微任务练习题](images/宏微任务练习题.png)

⭕练习题2：

```js
async function fn () {
  console.log('嘿嘿')
  const res = await fn2()
  console.log(res)  // 微任务
}

async function fn2 () {
  console.log('gaga')
}

fn()
console.log(222)

// 输出结果：嘿嘿 gaga 222 undefined
```

⭕练习题3：

```js
async function async1() {
    console.log("async1 start");
    await async2();
    console.log("async1 end") // 微任务1
}

async function async2() {
    console.log("async2")
}

console.log("script start")

setTimeout(function () {
    console.log("setTimeout") // 宏任务2
},0)

async1()

new Promise(function (resolve) {
    console.log("promise1");
    resolve()
}).then(function () {
    console.log("promise2") // 微任务1
})

console.log("script end");

/*
	输出结果：
	script start
	async1 start
	async2
	promise1
	script end
	async1 end
	promise2
	setTimeout
*/
```



## 作用域和作用域链

> **⛰`JS` 三大山之一**，一般可以将作用域分为全局作用域、局部作用域和块级作用域
>
> 1. **全局作用域**：任何不在函数中或是⼤括号 `{}` 中声明的变量都是在全局作⽤域下，全局作⽤域下声明的变量可以在代码的任意位置访问
> 2. **局部作用域**：**函数作⽤域也叫局部作⽤域**，在函数内部声明的变量就在⼀个函数作⽤域下⾯，这些变量只能在函数内部访问，不能在函数以外去访问
> 3. **块级作用域**：使用 `ES6` 中**新增的 `let` 和 `const` 指令可以声明块级作用域的变量**，块级作用域可以在函数中创建也可以在一个代码块中的创建（由⼤括号 `{}` 包裹的代码片段），在⼤括号之外不能访问这些变量

- **<u>词法作⽤域</u>**

词法作⽤域，⼜叫静态作⽤域，**`JS` 遵循的是词法作⽤域**，在变量被创建时，它的作用域就确定好了，⽽⾮执⾏阶段确定的，也就是**在写代码的时候就已经决定了变量的作用域**。

```js
var a = 2;
function foo(){
	console.log(a)
}
function bar(){
	var a = 3;
	foo();
}

bar(); // 2
```

由于 `JS` 遵循词法作⽤域，相同层级的 `foo` 和 `bar` 就没有办法访问到彼此作⽤域中的变量，所以输出 `2`。

- **<u>作用域链</u>**

当使⽤⼀个变量的时候，⾸先 `JS` 会尝试在当前作⽤域下去寻找该变量，如果没找到，再到它的上层作⽤域寻找，以此类推，直到找到该变量或是直到访问全局作⽤域被终止，如果在全局作⽤域⾥仍然找不到该变量，它就会在全局范围内隐式声明该变量（⾮严格模式下）或是直接报错，这一层层的关系就是作用域链。

```js
var sex = '男';
function person() {
	var name = '张三';
	function student() {
		var age = 18;
		console.log(name); // 张三
		console.log(sex); // 男
	}
	student();
	console.log(age); // Uncaught ReferenceError: age is not defined
}
person();
```

上述代码主要做了以下⼯作：

（1）`student` 函数内部属于最内层作⽤域，找不到 `name`，向上⼀层作⽤域 `person` 函数内部找，找到了输出“张三”

（2）`student` 内部输出 `sex` 时找不到，向上⼀层作⽤域 `person` 函数找，还找不到继续向上⼀层找， 即全局作⽤域，找到了输出“男”

（3）在 `person` 函数内部输出 `age` 时找不到，向上⼀层作⽤域找，即全局作⽤域，还是找不到则报错



## 原型和原型链

> **⛰`JS` 三大山之一**，在 `JS` 中是通过 `new` 运算符调用构造函数来新建一个实例对象，每个构造函数的内部都有一个 `prototype` 属性指向它的原型对象，而原型对象中有一个 `constructor` 属性指向自身的构造函数，另外，每个实例对象都有一个（`__proto__`）私有属性也是指向它的构造函数的 `prototype` 原型对象，该私有属性无法直接访问，可以通过 `Object.getPrototypeOf()` 方法来获取该原型对象

举一个例子🌰，`Person` 构造函数、原型和实例对象的三角关系图：

![原型的三角关系图](images/原型的三角关系图.png)

**📣同一构造函数创建出来的实例对象，都可以访问该原型对象共享的属性和方法，原型的应用一般是挂载方法，构造函数负责提供属性，原型负责提供方法。**

- **<u>原型链</u>**

当访问一个对象的属性或者方法时，如果当前对象内部不存在，那么它就会去它的原型对象里寻找，而这个原型对象又会有自己的原型，层层向上地一直寻找下去，因此这样就串联形成一个链式结构，也就是原型链。

原型链上的所有原型都是对象，所有的对象都是由 `Object` 构造的，而 `Object.prototype` 的下一级是`Object.prototype.__proto__`，即 `null`。

<img src="images/原型链.png" alt="原型链" style="zoom: 50%;" />



## 闭包

> 官方解释是，闭包是**由函数和声明该函数的词法环境组合而成**的，而更通俗一点的解释是，**引用外部变量的函数**，也可以说，**闭包就是能够访问其他函数内部变量的函数**
>
> 📣实现闭包最常见的方式就是**函数嵌套**，并不是形成闭包的唯一方式，在本质上，闭包就是将函数内部和函数外部连接起来的一座桥梁，**使函数外部能访问到函数内部的变量**

💡闭包的最大**用处**：


1. 可以读取函数内部的变量（**数据私有化，保障数据安全，不被全局污染**）
2. 让这些变量的值始终**保持在内存中**

⚠使用闭包的**注意点：**

1. 由于闭包会使得函数中的变量都被保存在内存中，**占用内存**，内存消耗很大，所以不能滥用闭包，否则会造成网页的性能问题，在 `IE` 中**可能导致内存泄露**

2. 如果你把父级函数当作对象使用，把闭包当作它的公用方法，把内部变量当作它的私有属性，这时闭包可以在外部改变父级函数内部变量的值，因此一定要小心，不要随便改变父级函数内部变量的值

🌰举例说明：

```js
function f1() {
	let n = 999;
  // nAdd是一个匿名函数（全局变量），也是一个闭包，等价于window.nAdd
	nAdd = function() { n += 1 }
  // 并不一定要return一个函数才算闭包
  // return只是让外面可以间接或者直接访问到该函数
  // 间接就是下面的做法，直接是另一种做法——立即执行函数
	return function f2() {
		console.log(n);
	}
}

var result = f1();
result(); // 999
nAdd();
result(); // 1000
```

在上段代码中，`result` 实际上就是闭包 `f2` 函数，它一共运行了两次，第一次的值是 `999`，第二次的值是`1000`。这证明了函数 `f1` 中的局部变量 `n` 一直保存在内存中，并没有在 `f1` 调用后被自动清除，原因就在于 `f1` 是 `f2` 的父级函数，而 `f2` 被赋值给了一个全局变量，这导致 `f2` 始终在内存中，而 `f2` 的存在依赖于 `f1`，因此 `f1` 也始终在内存中，不会在调用结束后被垃圾回收机制所回收。

**<u>⭕经典面试题（考虑异步、作用域两方面）：</u>**

```js
for (var i = 1; i <= 5; i++) {
	setTimeout(() => {
		console.log(i);
	}, 1000)
} // 6 6 6 6 6

// 1. 用立即执行函数(IIFE)可以实现闭包
for (var i = 1; i <= 5; i++) {
	(function (j) {
		setTimeout(() => {
			console.log(j);
		}, 1000)
	})(i)
} // 1 2 3 4 5

// 2. 使用let声明i变量，也实现闭包
for (let i = 1; i <= 5; i++) {
	setTimeout(() => {
		console.log(i);
	}, 1000)
}
```



## Promise

> `Promise` 是**异步编程**的一种解决方案，可以获取异步操作的消息，它的出现大大改善了异步编程的困境，**避免了回调地狱**，它比传统的解决方案中的回调函数和事件更合理和更强大

`Promise` 是一个构造函数，其接收一个函数作为参数，**该构造函数是同步的并且会给立即执行**，会返回一个 `Promise` 实例对象。

一个 `Promise` 实例对象总共有进行中 `pending`（默认）、已成功 `fulfilled` 和已失败 `rejected` 三种状态，但 `Promise` 对象只能由 `pending` 转变为 `fulfilled` 或 `rejected` 状态，以及状态的改变是通过 `resolve()` 和 `reject()` 方法来实现的，可以在异步操作结束后调用这两个方法来改变 `Promise` 对象的状态，并且**状态一经改变，就无法再被改变**。

|               `Promise` 中的**常见的静态方法**               |
| :----------------------------------------------------------: |
|                     `Promise.resolve()`                      |
|                      `Promise.reject()`                      |
| `Promise.all([promise1, promise2, promise3, ...])` 在**所有** `promise` 都完成后执行，可以用于处理一些**并发任务** |
| `Promise.race([promise1, promise2, promise3, ...])`只要**任意一个** `promise` 满足条件，就会执行 |

```js
// Promise.resolve()等价于
let resolve = new Promise((resolve, reject) => resolve())

// Promise.reject()等价于
let reject = new Promise((resolve, reject) => reject())

// 在前面的所有promise都完成后才执行后面的.then中配置的函数
Promise.all([promise1, promise2, promise3]).then((values) => {
  // values是一个数组，会收集前面promise的结果，即values[0] => promise1的成功的结果
  // ...
})
```

### async/await 语法糖

> `async` 和 `await` 是基于 `Promise` 的一个语法糖，**📣用于优化其多个 `.then()` 链式调用的问题**

**`async` 用于声明⼀个异步函数，其返回值是⼀个 `Promise` 对象，如果在函数中 `return` 一个直接量，`async` 会将该直接量通过 `Promise.resolve()` 隐式地封装成 `Promise` 对象，而在函数中没有返回值的话，则会返回 `Promise.resolve(undefined)`。**

**`await` 只能在 `async` 函数内部使⽤**，当与 `Promise` ⼀起使⽤时，**`await` 会强制暂停当前 `async` 函数的执行，等待该 `Promise` 成功或者失败之后再恢复执行**。当 `Promise` 状态变为 `fulfilled` 时，`await` 表达式会获取 `Promise` 中 `resolve()` 的值并执行 `async` 函数的余下代码（**在 `await` 表达式下面的代码可以被认为是在链式调用的 `then()` 回调之内**），而当状态变为 `reject` 时，`await` 表达式会将拒绝原因抛出，并终止函数的继续执行，而当与非 `Promise` ⼀起使⽤时，`await` 表达式的值会被 `Promise.resolve()` 隐式地转换为 `Promise` 对象。

|           **`async/await` 对比 `Promise` 的优势**            |
| :----------------------------------------------------------: |
|         同步化代码的结构更清晰简洁，阅读体验相对更好         |
| 对于条件语句和其他流程语句比较友好，可以直接写到判断条件里面 |
| `Promise` 传递中间值⾮常麻烦，而 `async/await` ⼏乎是同步的写法，⾮常简单直观 |

- **<u>错误的处理方式</u>**

在 `async` 函数里，无论是 `reject()` 中的数据或是逻辑报错都会被默默吞掉，所以最好把 `await` 放入`try/catch` 中，`catch` 能够捕捉到 `reject()` 中的数据或者抛出的异常：

```js
function timeout(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => { reject('error') }, ms);  // 模拟出错
  });
}

async function asyncPrint(ms) {
  try {
     console.log('start');
     await timeout(ms);
     console.log('end');  // 这句代码不会被执行
  } catch(err) {
     console.log(err); // 从这里捕捉错误
  }
}

asyncPrint(1000);
```

如果不用 `try/catch` 的话，也可以像下面这样处理错误：

```js
function timeout(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => { reject('error') }, ms);  // 模拟出错
  });
}

async function asyncPrint(ms) {
  console.log('start');
  await timeout(ms)
  console.log('end');  // 这句代码不会被执行
}

asyncPrint(1000).catch(err => {
    console.log(err)； // 从这里捕捉错误
});
```

如果你**不想让错误中断后面代码的执行**，也可以提前截留错误：

```js
function timeout(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => { reject('error') }, ms);  // 模拟出错
  });
}

async function asyncPrint(ms) {
  console.log('start');
  await timeout(ms).catch(err => { console.log(err) }); // 注意要用catch
  console.log('end');  // 这句代码会被执行
}

asyncPrint(1000);
```



## 继承

> 继承是一种类与类之间的关系，可以使得子类具有父类的各种属性和方法，而不需要再次编写相同的代码，常见的继承方式有以下几种：
>
> - **原型继承**
> - **借用构造函数继承**
> - **组合继承**
> - **寄生组合继承**
> - **`Class` 继承**

### 原型继承

> 关键🔑：**将子类的原型指向了父类的实例对象**
>
> **优点**：
>
> 1. **继承父类的所有属性和方法**
> 2. 简单，易于实现
>
> **缺点**：
>
> 1. 创建子类实例时，无法向父类传参
> 2. 存在子类实例共享父类引用属性的问题
> 3. 无法实现多继承

```js
function Person(name, age) {
	this.name = name,
	this.age = age,
	this.play = [1, 2, 3],
	this.setName = function() {}
}
Person.prototype.setAge = function() {}

function Student(price) {
	this.price = price,
	this.setScore = function() {}
}
Student.prototype = new Person() // 要想为子类新增或重写属性和方法，必须要在这之后执行
Student.prototype.constructor = Student
Student.prototype.sayHello = function() {}

let s1 = new Student(15000)
let s2 = new Student(14000)

console.log(s1)
s1.play.push(4)
console.log(s1.play) // [1, 2, 3, 4]
console.log(s2.play) // [1, 2, 3, 4]
```

![原型继承](images/原型继承.png)

### 借用构造函数继承

> 关键🔑：**在子类构造函数中使用 `call()` 方法借调父类构造函数**
>
> **优点**：
>
> 1. 解决了原型继承中子类实例共享父类引用属性的问题
> 2. 创建子类实例时，可以向父类传递参数
> 3. 可以实现多继承(`call()` 多个父类)
>
> **缺点**：
>
> 1. **只能继承父类实例的属性和方法，不能继承原型的属性和方法**
> 2. 无法实现方法复用，每个子类都有父类实例方法的副本，影响性能

```js
function Person(name, age) {
	this.name = name,
	this.age = age,
	this.setName = function() {}
}
Person.prototype.setAge = function() {}

function Student(name, age, price) {
	Person.call(this, name, age),
	this.price = price,
  this.setScore = function() {}
}

let s1 = new Student('Tom', 20, 15000)
console.log(s1)
```

![借用构造函数继承](images/借用构造函数继承.png)

### 组合继承

> 关键🔑：**通过借用父类构造函数来实现属性的继承，并通过将父类实例作为子类原型来实现方法的继承**
>
> **优点**：
>
> 1. **可以继承父类实例的属性和方法，也可以继承原型的属性和方法**
> 2. 解决了原型继承中子类实例共享父类引用属性的问题
> 3. 创建子类实例时，可以向父类传递参数
> 4. 实现方法复用
>
> **缺点**：无论在什么情况下，都会调用两次父类构造函数，第一次是在创建子类原型的时候，另外一次是在子类构造函数的内部，会导致子类的原型中会包含不必要的父类实例属性

```js
function Person(name, age) {
	this.name = name,
	this.age = age
}
Person.prototype.setAge = function() {}

function Student(name, age, price) {
	Person.call(this, name, age),
	this.price = price,
	this.setScore = function() {}
}
Student.prototype = new Person()
Student.prototype.constructor = Student
Student.prototype.sayHello = function() {}

let s1 = new Student('Tom', 20, 15000)
console.log(s1)
```

![组合继承](images/组合继承.png)

### 寄生组合继承

> 关键🔑：**通过 `Object.create()` 方法创建一个新对象，并使用父类原型来作为新对象的原型**
>
> **优点**：避免了组合继承的问题
>
> **缺点**：无法辨别实例对象是子类还是父类创建的

```js
function Person(name, age) {
	this.name = name,
	this.age = age
}
Person.prototype.setAge = function() {}

function Student(name, age, price) {
	Person.call(this, name, age),
	this.price = price,
	this.setScore = function() {}
}
Student.prototype = Object.create(Person.prototype)
Student.prototype.constructor = Student
Student.prototype.sayHello = function() {}

let s1 = new Student('Tom', 20, 15000)
console.log(s1)
console.log(s1 instanceof Student, s1 instanceof Person) // true true
```

![寄生组合继承](images/寄生组合继承.png)

### Class 继承

> `ES6` 中引入了 `class` 类，`class` 可以通过 `extends` 关键字实现继承，⚠需要注意的是，**`class` 只是原型的语法糖，`JS` 继承仍然是基于原型实现的**
>
> **优点**：语法简单易懂，操作更方便
>
> **缺点**：并不是所有的浏览器都支持 `class` 关键字

```js
class Person {
	constructor(name, age) {
		this.name = name
		this.age = age
	}
	showName() {
		console.log("调用父类的方法")
		console.log(this.name, this.age);
	}
}

class Student extends Person {
	constructor(name, age, salary) {
		super(name, age) // 通过super调用父类的构造函数
		this.salary = salary
	}
	showName() {
    super.showName() // 通过super调用父类的方法
		console.log("调用子类的方法")
		console.log(this.name, this.age, this.salary);
	}
}

let p1 = new Person('kobe', 39)
let s1 = new Student('wade', 38, 100)
console.log(p1)
console.log(s1)
s1.showName() // 调用父类的方法 wade 38 调用子类的方法 wade 38 100
```

![Class继承](images/Class继承.png)



## AJAX

> `AJAX` 是 `Asynchronous JavaScript and XML` 的缩写，指的是通过 `JS` 的异步通信，从服务器获取的 `XML` 文档中提取数据，**⚙在不重新加载整个网页的情况下，对网页的某部分进行更新**
>
> 其**缺点**如下：
>
> - 本身是针对 `MVC` 编程，不符合前端 `MVVM` 的浪潮
> - 基于原生 `XHR` 开发，`XHR` 本身的架构不清晰
> - 不符合关注分离的原则
> - 配置和调用方式非常混乱，而且基于事件的异步模型不友好

创建 `AJAX` 请求的步骤：

1. 创建一个 `XMLHttpRequest` 对象
2. 在这个对象上使用 `open` 方法创建一个 `HTTP` 请求，所需要的参数是请求方法、请求地址、是否异步和用户的认证信息
3. 在发起请求前，可以为这个对象添加一些头信息和监听函数
   -  通过 `setRequestHeader` 方法可以为请求添加头信息
   -  当它的状态变化时，会触发 `onreadystatechange` 状态监听函数，所以可通过设置监听函数，来处理请求成功后的结果。当对象的 `readyState` 变为 `4` 的时候，代表服务器返回的数据接收完成，这个时候可以通过判断请求的状态，如果状态是 `2xx` 或者 `304` 的话，则可以通过 `response` 中的数据来对页面进行更新
4. 当对象的属性和监听函数设置完成后，最后调用 `send` 方法来向服务器发起请求，可以传入参数作为发送的数据体

使用 `Promise` 封装 `AJAX`：

```js
function request(url) {
	// 创建一个promise对象
	let promise = new Promise(function(resolve, reject) {
		let xhr = new XMLHttpRequest();
		// 创建一个http对象
		xhr.open("GET", url, true);
		// 设置响应的数据类型
		xhr.responseType = "json";
		// 设置请求头信息
		xhr.setRequestHeader("Accept", "application/json");
		// 设置状态监听函数
		xhr.onreadystatechange = function() {
			if(this.readState !== 4) return;
			// 当请求成功或失败时，改变promise的状态
			if(this.status === 200) {
				resolve(this.response);
			} else {
				reject(new Error(this.statusText));
			}
		};
		// 设置错误监听函数
		xhr.onerror = function() {
			reject(new Error(this.statusText));
		};
		// 发送http请求
		xhr.send();
  });
  return promise;
}
```



## 📌*进阶知识点*📌

### *解释下什么是 JS 垃圾回收机制？*

> [参考文章：JS 垃圾回收机制](https://zhuanlan.zhihu.com/p/576722965)

所谓垃圾回收机制（`Garbage Collection`）就是**清理内存的方式**，简称 `GC`，在创建变量的时候，`JS` 引擎会自动给对象分配对应的内存空间，不需要我们手动分配，然后 `JS` 的垃圾回收机制**会定期（周期性）找出那些不再用到的内存（变量）**，并且释放其内存。

🛠常见 `GC` 算法：**引用计数**、**标记清除**、**标记整理**和 **`Scavenge`** 等。

### *Object.is() 方法与比较运算符 “ === ”、“ == ” 的区别？*

|               情况                | 说明                                                         |
| :-------------------------------: | ------------------------------------------------------------ |
|  使用双等号（`==`）进行相等判断   | 如果两边的类型**不一致**时，则会**进行强制类型转化后再进行比较** |
|  使用三等号（`===`）进行相等判断  | 如果两边的类型**不一致**时，不会做强制类型转化，**直接返回 `false`** |
| 使用 `Object.is()` 来进行相等判断 | 一般情况下**和三等号（`===`）的判断相同**，**它处理了一些特殊的情况，比如 `-0` 和 `+0` 不再相等，两个 `NaN` 是相等的** |

### *for...in 和 for...of 两个语句之间的区别？*

> 无论是 `for...in` 还是 `for...of` 语句都是迭代一些东西，它们之间的主要区别在于迭代方式：
>
> - `for...in` 语句以任意顺序迭代对象的可枚举属性（**遍历数组的本身及原型链中的所有可枚举属性**）
> - `for...of` 语句遍历可迭代对象定义要迭代的数据（**只遍历当前数组下标对应的数据**）
>
> ⚠需要注意的是，**两者都不能遍历 `symbol` 类型**

简单而言，对数组来说，`for...in` 是遍历键名，`for...of` 是遍历键值，需要说明的是 **`for...in` 还能遍历到手动添加的键名和原型链上添加的键名，`for...of` 则不能遍历到**，而对普通对象来说，`for...in` 也是一样的作用，但 `for...of` 则会报错。

🌰举例说明：

```js
Object.prototype.objCustom = function() {};
Array.prototype.arrCustom = function() {};

let iterable = [3, 5, 7];
iterable.foo = 'hello';

for (let i in iterable) {
  console.log(i); // 0, 1, 2, "foo", "arrCustom", "objCustom"
}

for (let i in iterable) {
  if (iterable.hasOwnProperty(i)) {
    console.log(i); // 0, 1, 2, "foo"
  }
}

for (let i of iterable) {
  console.log(i); // 3, 5, 7
}
```

### *如何判断一个对象是空对象？*

（1）**使用 `JSON.stringify()` 方法，空对象对应的字符串为 `"{}"`**

```js
let data = {};
let b = JSON.stringify(data) === "{}";
console.log(b); // true
```

（2）**`for...in` 循环**

```js
let a = {};
let b = function(obj) {
  for (let key in obj) {
    return false; // 若不为空则可遍历，返回false
  }
  return true;
};
console.log(b(a)); //true
```

（3）**使用 `Object.keys()` 方法**

```js
let data = {};
let arr = Object.keys(data);
console.log(arr.length == 0); // true
```

### *如何判断是否为一个数组？*

（1）**使用 `toString` 方法**


```js
function isArray(arg) {
	// return Object.prototype.toString.call(arg) === '[object Array]'
  return Object.prototype.toString.call(arg).slice(8, -1).toLowerCase() === "string"
}

let arr = [1,2,3]
isArray(arr)  // true
```

（2）**使用 `Array.isArray` 方法**


```js
let arr = [1,2,3]
Array.isArray(arr) // true
```

### *JS 数组去重有哪些方法？*

> [参考文章：JavaScript 数组去重的方法](https://segmentfault.com/a/1190000016418021?utm_source=tag-newest)

（1）**利用 `Set` 对象**（**推荐👍**）

> 🔑核心：`Set` 对象中的元素是唯一的
>
> 📦范围：只可以对基本数据类型（包括 `NaN`）和引用变量进行去重，但**无法对 `{}` 空对象进行去重**（能够对引用变量进行去重的原因在于对比的是引用地址，不是内容）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

// 方式1：Array.from()
function uniqueArray1(arr) {
  return Array.from(new Set(arr))
}
// 方式2：扩展运算符
function uniqueArray2(arr) {
  return [...new Set(arr)]
}

console.log(uniqueArray1(arr)) // {}没有去重
console.log(uniqueArray2(arr)) // {}没有去重
```

（2）**利用 `Map` 对象**

> 🔑核心：`Map` 对象中的键可以是任何值并且只能出现一次
>
> 📦范围：只可以对基本数据类型（包括 `NaN`）和引用变量进行去重，但**无法对 `{}` 空对象进行去重**（能够对引用变量进行去重的原因在于对比的是引用地址，不是内容）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

function uniqueArray(arr) {
  let map = new Map()
	let newArray = []
	for (let i = 0; i < arr.length; i++) {
		if (!map.has(arr[i])) {
			map.set(arr[i])
			newArray.push(arr[i])
		}
	}
	return newArray
}

console.log(uniqueArray(arr)) // {}没有去重
```

（3）**使用 `indexOf` 方法**

> 🔑核心：在数组中是否可以找到给定元素的第一个索引
>
> 📦范围：只可以对基本数据类型和引用变量进行去重，但需要注意的是**不包括 `NaN` 和 `{}` 空对象的去重**（`indexOf` 方法采用严格相等，`NaN` 之间不会相等，且总是返回 `-1`）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

function uniqueArray(arr) {
	let newArray = []
	for (let i = 0; i < arr.length; i++) {
		if (newArray.indexOf(arr[i]) === -1) {
			newArray.push(arr[i])
		}
	}
	return newArray
}

console.log(uniqueArray(arr)) // NaN、{}没有去重
```

（4）**使用 `includes` 方法**

> 🔑核心：判断数组是否包含指定的一个值
>
> 📦范围：只可以对基本数据类型（包括 `NaN`）和引用变量进行去重，但**无法对 `{}` 空对象进行去重**（能够对引用变量进行去重的原因在于对比的是引用地址，不是内容）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

function uniqueArray(arr) {
	let newArray = []
	for (let i = 0; i < arr.length; i++) {
		if (!newArray.includes(arr[i])) {
			newArray.push(arr[i])
		}
	}
	return newArray
}

console.log(uniqueArray(arr)) // {}没有去重
```

（5）**使用 `filter` + `indexOf` 方法**

> 🔑核心：给定元素的第一个索引是否与当前索引一致
>
> 📦范围：可以对基本数据类型和引用变量进行去重，但需要注意的是**无法对 `{}` 空对象进行去重以及会将所有 `NaN` 去掉**（`indexOf` 方法采用严格相等，`NaN` 之间不会相等，且总是返回 `-1`）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

function uniqueArray(arr) {
	return arr.filter((item, index) => arr.indexOf(item) === index)
}

console.log(uniqueArray(arr)) // 所有NaN去掉，{}没有去重
```

（6）**使用 `reduce` + `includes` 方法**

> 🔑核心：判断数组是否包含指定的一个值
>
> 📦范围：只可以对基本数据类型（包括 `NaN`）和引用变量进行去重，但**无法对 `{}` 空对象进行去重**（能够对引用变量进行去重的原因在于对比的是引用地址，不是内容）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

function uniqueArray(arr) {
	return arr.reduce((newArray, cur) => newArray.includes(cur) ? newArray : [...newArray, cur], [])
}

console.log(uniqueArray(arr)) // {}没有去重
```

（7）**双重 `for` 循环 + `splice` 方法**

> 🔑核心：两个元素相等，利用 `splice` 方法删除第二个元素
>
> 📦范围：只可以对基本数据类型和引用变量进行去重，但需要注意的是**不包括 `NaN` 和 `{}` 空对象的去重**（ 全等 `===` 对于 `NaN` 之间不会被比较为相等）

```js
let a = {
  name: 1,
	age: "2"
}
let arr = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', a, a, {}, {}]

function uniqueArray(arr) {            
	for(let i = 0; i < arr.length; i++) {
		for(let j = i + 1; j < arr.length; j++) {
			if(arr[i] === arr[j]) {
				arr.splice(j, 1)
			}
		}
  }
	return arr
}

console.log(uniqueArray(arr)) // NaN、{}没有去重
```

### *解释下什么是深拷贝和浅拷贝及其实现方法？*

> 如何区分深拷贝与浅拷贝，简单点来说，就是假设 `B` 复制了 `A`，当修改 `A` 时，看 `B` 是否会发生变化，如果 `B` 也跟着变了，说明这是**浅拷贝，拿人手短**，如果 `B` 没变，那就是**深拷贝，自食其力**

基本数据类型和引用数据类型，前者是数据存储在栈内存中，后者是将其地址存在栈内存中，而真实数据存储在堆内存中。

| 赋值                                                         | 浅拷贝                                                       | 深拷贝                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 赋值是将某一数值或对象赋给某个变量的过程，**对于基本数据类型是赋值**，赋值之后两个变量**互不影响**，而**对于引用数据类型是赋址**，两个变量具有相同的引用地址指向同一个对象，**相互之间有影响** | **创建一个新对象**，这个对象有着原始对象属性值的一份精确拷贝，**如果属性是基本类型，拷贝的就是基本类型的值，如果属性是引用类型，拷贝的就是内存地址**，所以如果其中一个对象改变了这个内存地址对应的数据，就会影响到另一个对象（可以理解为，**只拷贝对象的一层数据，再深处层次的引用类型将只会拷贝引用**） | 将旧对象从内存中完整的拷贝一份出来给新对象，**并且在堆内存中开辟一个新的区域来存储新对象**，且修改新对象不会影响旧对象 |

![深拷贝和浅拷贝](images/深拷贝和浅拷贝.png)

- **<u>总结</u>**

|        | **和原数据是否指向同一对象** |   第一层数据为**基本类型**   |   原数据中包含**引用类型**   |
| :----: | :--------------------------: | :--------------------------: | :--------------------------: |
|  赋值  |              是              | 改变**不会**使原数据一同改变 |    改变会使原数据一同改变    |
| 浅拷贝 |              否              | 改变**不会**使原数据一同改变 |    改变会使原数据一同改变    |
| 深拷贝 |              否              | 改变**不会**使原数据一同改变 | 改变**不会**使原数据一同改变 |

#### 浅拷贝

（1）**`Object.assign()` 方法**

> 如果对象的**属性值全为基本类型**，通过 `Object.assign({}, obj)` 得到的新对象为**深拷贝**，而如果**属性值中有对象或其他引用类型**，那对于这个新对象而言是**浅拷贝**

```js
let a = {
	name: "muyiy",
	book: {
		title: "You Don't Know JS",
		price: "45"
	}
}
let b = Object.assign({}, a);
a.name = "change";
a.book.price = "55";
console.log(a);
// {
//  name: "change",
//  book: {title: "You Don't Know JS", price: "55"}
// } 
console.log(b);
// {
//  name: "muyiy",
//  book: {title: "You Don't Know JS", price: "55"}
// } 
```

⚠但是使用 `Object.assign()` 方法有几点需要**注意**：

1. 它不会拷贝对象原型链上的属性
2. 它不会拷贝对象的不可枚举属性
3. 可以拷贝 `Symbol` 类型的属性

（2）**数组 `slice()` 方法和 `concat()` 方法**

> 如果数组**元素全为基本类型**，则为**深拷贝**，而如果**元素中有对象或其他引用类型**，则为**浅拷贝**

```js
let a = [0, "1", [2, 3]];
let b = a.slice();
// let b = a.concat();

a[1] = "99";
a[2][0] = 4;
console.log(a); // [0, "99", [4, 3]]
console.log(b); // [0, "1", [4, 3]]
```

（3）**`...` 扩展运算符**

> **和上述的对象、数组方法一样**，但使用 `...` 扩展运算符进行浅拷贝会更加方便

```js
let a = {
	name: "muyiy",
	book: {
		title: "You Don't Know JS",
		price: "45"
	}
}
let b = {...a};
a.name = "change";
a.book.price = "55";
console.log(a);
// {
//  name: "change",
//  book: {title: "You Don't Know JS", price: "55"}
// } 
console.log(b);
// {
//  name: "muyiy",
//  book: {title: "You Don't Know JS", price: "55"}
// } 

let c = [0, "1", [2, 3]];
let d = [...c];

c[1] = "99";
c[2][0] = 4;
console.log(c); // [0, "99", [4, 3]]
console.log(d); // [0, "1", [4, 3]]
```

#### 深拷贝

（1）**`JSON` 序列化和反序列化**

```js
let a = {
	name: "muyiy",
	book: {
		title: "You Don't Know JS",
		price: "45"
	}
}
let b = JSON.parse(JSON.stringify(a));

a.name = "change";
a.book.price = "55";
console.log(a);
// {
//  name: "change",
//  book: {title: "You Don't Know JS", price: "55"}
// } 
console.log(b);
// {
//  name: "muyiy",
//  book: {title: "You Don't Know JS", price: "45"}
// } 

let c = [0, "1", [2, 3]];
let d = JSON.parse(JSON.stringify(c));

c[1] = "99";
c[2][0] = 4;
console.log(c); // [0, "99", [4, 3]]
console.log(d); // [0, "1", [2, 3]]
```

⚠但是 `JSON` 并不是那么完美的，它也有以下几点**局限性**：

1. 对于函数、`undefined`、`symbol` 类型，在序列化过程中会被忽略（出现在非数组对象的属性值中时），或者被转换成 `null`（出现在数组中时），而函数、`undefined` 类型被单独转换时，会返回 `undefined`
2. 拷贝 `Date` 引用类型会变成字符串
3. 拷贝 `RegExp` 引用类型会变成空对象
4. 对象中含有 `NaN`、`Infinity/-Infinity` 以及 `null`，序列化的结果会变成 `null`
5. 无法拷贝不可枚举的属性
6. 无法拷贝对象原型链上的属性
7. 无法拷贝 `BigInt` 类型数据，会抛出异常
8. 无法拷贝对象的循环应用，即对象成环 (`obj[key] = obj`)，会抛出异常

（2）**递归函数**

```js
function deepClone(source) {
	let target = null;
	if (source.constructor === Array || source.constructor === Object) {
		target = source.constructor === Array ? [] : {};
		for (let key in source) {
			if (source.hasOwnProperty(key)) {
				if (source[key] && typeof source[key] === 'object') {
					target[key] = deepClone(source[key]);
				} else {
					target[key] = source[key];
				}
			}
		}
	} else {
    // 不是数组、对象，则原路返回
		target = source;
	}
	return target;
}
```

（3）**第三方库（`lodash`、`jQuery`）**

```js
var a = [0, "1", [2, 3]];

// 1. 通过lodash函数库实现深拷贝
var _ = require('lodash');
var b = _.cloneDeep(a);

// 2. 通过jQuery的extend方法实现深拷贝
// var b = $.extend(true, [], a); // true为深拷贝，false为浅拷贝

a[1] = "99";
a[2][0] = 4;
console.log(a); // [0, "99", [4, 3]]
console.log(b); // [0, "1", [2, 3]]
```

### *箭头函数的 this 指向哪⾥？*

> 箭头函数不同于传统 `JS` 中的函数，箭头函数并没有属于⾃⼰的 `this`，它所谓的 `this` 是**捕获其所在上下⽂的 `this` 值**作为⾃⼰的 `this` 值，并且由于没有属于⾃⼰的 `this`，所以是**不会被 `new` 调⽤**的，这个所谓的 `this` 也不会被改变

🌰举例说明，可以通过 `Babel` 转换前后的代码来更清晰的理解箭头函数：

```js
// 转换前的 ES6 代码
let obj = { 
  test() { 
    return () => { 
      console.log(this === obj)
    }
  } 
}

// 转换后的 ES5 代码
var obj = { 
  test: function() { 
    var that = this
    return function() { 
      console.log(that === obj)
    }
  } 
}
```

### *函数内部的 this 指向什么？*

> 在实际开发中，**📣只有当函数调用时才确定 `this` 的指向**，`this` 的指向可以通过四种调用模式来判断

|       调用模式       | 说明                                                         |
| :------------------: | ------------------------------------------------------------ |
|   **函数**调用模式   | 如果作为普通函数来调用时，**`this` 指向全局对象**            |
|   **方法**调用模式   | 如果函数作为对象的方法来调用时，**`this` 指向对象**          |
| **构造函数**调用模式 | 如果通过 `new` 运算符调用构造函数来创建实例对象，**`this` 指向新创建的实例对象** |
|  **上下文**调用模式  | `apply`、`call` 和 `bind` 三种方法都可以**显示指定调用函数的 `this` 指向** |

### *JS 中的 new 运算符到底做了些什么？*

在 `MDN` 文档中解释，`new` 关键字会进行如下的操作🛠：

1. 创建一个空的 `JS` 对象（即 `{}`）
2. 将新对象的 `__proto__` 私有属性链接至构造函数的 `prototype` 原型对象
3. 将新对象作为 `this` 的上下文（将构造函数中的 `this` 指向新对象，为该对象添加属性和方法）
4. 最后如果该函数没有返回对象，则返回 `this`（新对象）

### *ES6 模块与 CommonJS 模块有什么差异？*

> [参考文章：ES6 模块与 CommonJS 模块的差异](https://es6.ruanyifeng.com/#docs/module-loader#ES6-%E6%A8%A1%E5%9D%97%E4%B8%8E-CommonJS-%E6%A8%A1%E5%9D%97%E7%9A%84%E5%B7%AE%E5%BC%82)

（1）`CommonJS` 模块的 `require()` 是**同步加载**，`ES6` 模块的 `import` 命令是**异步加载**

（2）`CommonJS` 模块是**运行时加载**，`ES6` 模块是**编译时加载**

|                       `CommonJS` 模块                        |                          `ES6` 模块                          |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| `CommonJS` 模块就是对象，即在输入时是**先加载整个模块来生成一个对象，然后再从这个对象上面读取方法**，这种加载称为“运行时加载” | `ES6` 模块不是对象，它的对外接口只是一种静态定义，即**在 `import` 时可以指定加载某个输出值，而不是加载整个模块**，这种加载称为“编译时加载” |

（3）`CommonJS` 模块输出的是**值的拷贝**，`ES6` 模块输出的是**值的引用**

|                       `CommonJS` 模块                        |                          `ES6` 模块                          |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| `CommonJS` 模块输出的是值的拷贝，也就是说一旦输出一个值，**模块内部的变化就影响不到这个值**，并且**可以对 `CommonJS` 输出的变量重新赋值** | `ES6` 模块的运行机制与 `CommonJS` 不一样，`JS` 引擎对脚本静态分析的时候，遇到模块 `import` 加载命令，就会生成一个**只读引用**，**不能改变其值，对 `ES6` 模块赋值会编译报错**，等到脚本真正执行时，再根据这个只读引用，到被加载的那个模块里面去取值，**当原始值发生变化，`import` 加载的值也会跟着变，因此 `ES6` 模块是动态引用，并且不会缓存值** |

### *CommonJS、AMD 和 CMD 之间有什么区别？*

|               `CommonJS`               |                            `AMD`                             |                            `CMD`                             |
| :------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| **同步加载**，输出的是一个**值的拷贝** | **异步加载**，**推崇依赖前置**，在定义模块的时候就要声明其依赖的模块，**对于依赖的模块，`AMD` 是立即执行** | **异步加载**，**推崇依赖就近**，只有在用到某个模块的时候再去 `require`，**对于依赖的模块，`CMD` 是延迟执行** |
|                                        | `AMD` 在加载模块完成后就会立即执行该模块，所有模块都加载执行完后会进入 `require` 的回调函数，再执行主逻辑，这样的效果就是依赖模块的执行顺序和书写顺序不一定一致，看网络速度，哪个先下载下来，哪个先执行，但是**主逻辑一定在所有依赖加载完成后才执行** | `CMD` 加载完某个依赖模块后并不执行，只是下载而已，**在所有依赖模块加载完成后进入主逻辑，遇到`require` 语句的时候才执行对应的模块**，这样模块的执行顺序和书写顺序是完全一致的 |

### *JS 脚本延迟加载的方式有哪些？*

**延迟加载就是等页面加载完成之后再加载 `JS` 文件**，其有助于**提高页面加载速度💡**，一般有以下几种方式：

1. 可在 `<script>` 标签上添加 `defer` 或者 `async` 属性
2. 将 `JS` 脚本放在文档的底部
3. 使用 `setTimeout` 方式来延迟
4. 动态创建 `DOM` 元素：可以对文档的加载事件进行监听，当文档加载完成后再动态的创建 `<script>` 标签来引入 `JS` 脚本


### *JS 实现异步编程有哪些方式？*

在 `JavaScript` 中，异步机制可以分为以下几种：

|     方式      | 说明                                                         |
| :-----------: | ------------------------------------------------------------ |
|   回调函数    | 使用回调函数的方式有一个缺点是，多个回调函数嵌套的时候会造成**回调函数地狱**，上下两层的回调函数间的**代码耦合度太高，不利于代码的可维护** |
|   `Promise`   | 使用 `Promise` 的方式可以将嵌套的回调函数作为链式调用，但是使用这种方法，**有时也会造成多个 `.then` 的链式调用，可能会造成代码的语义不够明确** |
| `async/await` | `async` 是 `Promise` 的语法糖，它内部自带执行器，当函数内部执行到一个 `await` 语句时，如果该语句是返回一个 `promise` 对象，那么函数将会等待 `promise` 对象的状态变为 `resolve` 后再继续向下执行，因此可以将异步逻辑转化为同步的顺序来书写，并且这个函数可以自动执行 |

### *JS 创建对象的方式有哪些？*

> `JS` 创建对象有以下几种方式：
>
> - **字面量**
> - **`new Object()` 构造函数**
> - **工厂函数**
> - **自定义构造函数**
> - **`prototype` 原型**
> - **构造函数与 `prototype` 原型的组合**
> - **`Class` 类**

（1）**字面量或者 `Object()` 构造函数**

> 这两种创建对象的方式同样存在两个问题：
>
> 1. 创建多个对象时，会产生代码冗余
> 2. **对象中的方法不能共享**，每个对象中的方法都是独立的

```js
let person = {
  name: "zhangsan",
  age: 18,
  gender: 'male',
  sayName: function() {
		console.log(this.name);
  }
}

let person = new Object();
// 为这个实例化的对象添加属性
person.name = "zhangsan";
person.age =21;
person.gender = 'male';
person.sayName = function() {
	console.log(this.name)
}
```

（2）**工厂函数**

> 这种方式是使用一个函数来创建对象，减少重复代码，解决了代码冗余的问题，但是**方法不能共享**的问题还是存在，另一个问题是**无法识别对象的类型**，都是 `Object` 类型

```js
// 将创建对象的代码封装在一个函数中
function createPerson(name, age, gender) {
  let person = new Object();
  person.name = name;
  person.age = age;
  person.gender = gender;
  person.sayName = function () {
    console.log(this.name);
  }
  return person;
}
// 利用工厂函数来创建对象
let person1 = createPerson("zhangsan", 18, 'male');
let person2 = createPerson("lisi", 20, 'female');
```

（3）**自定义构造函数**

> 任何函数只要通过 `new` 运算符来调用，那它就可以作为构造函数，如果不用 `new` 运算符来调用，它就是一个普通函数的调用，但该方式还是没有解决**方法不能共享**的问题，对比工厂函数有以下不同之处：
>
> 1. 没有显式地创建对象
> 2. 直接将属性和方法赋给了 `this` 对象
> 3. 没有 `return` 语句
>
> 相比于工厂函数，实例对象和构造函数建立起了联系，因此可以**通过原型来识别对象的类型**

```js
// 自定义构造函数
function Person(name, age, gender) {
  this.name = name;
  this.age = age;
  this.gender = gender;
  this.sayName = function () {
    console.log(this.name);
  }
}
 
let person1 = new Person('zhangsan', 29, 'male');
let person2 = new Person('lisi', 19, 'female');
 
person1.sayName(); // zhangsan
person2.sayName(); // lisi
console.log(person1.sayName === person2.sayName); // false
```

（4）**`prototype` 原型**

> 该方式与构造函数不同，无法传递初始化参数，但其定义的**属性和方法是由所有实例对象共享的**
>
> 假如原型中包含有**引用类型**的属性，那某个实例对象修改了该属性的值，该原型创建的**所有实例对象访问的值都会改变**，并且如果在实例对象上添加一个与原型对象中**同名属性**，那就会在实例对象上创建这个属性，这个属性会**遮住原型对象上的属性**

```js
function Person() {};
Person.prototype.name = "zhangsan";
Person.prototype.age = 29;
Person.prototype.gender = "male";
Person.prototype.friends = ["lisi", "wangwu"],
Person.prototype.sayName = function() {
  console.log(this.name);
};

let person1 = new Person();
let person2 = new Person();

console.log(person1.name);; // zhangsan 来自原型
console.log(person2.name);; // zhangsan 来自原型
console.log(person1.sayName == person2.sayName); // true

person1.name = 'lisi';
console.log(person1.name); // lisi 来自实例

person1.friends.push("zhaoliu");
console.log(person1.friends); // ['lisi', 'wangwu', 'zhaoliu']
console.log(person2.friends); // ['lisi', 'wangwu', 'zhaoliu']
```

（5）**构造函数与 `prototype` 原型的组合**

> **构造函数用于定义实例属性，原型用于定义共享的方法和属性**，这种方式结合了上面两种方式，解决了代码冗余，方法不能共享，引用类型改变值等问题

```js
function Person(name, age, gender) {
  this.name = name;
  this.age = age;
  this.gender = gender;
  this.friends = ['zhangsan', 'lisi'];
}
Person.prototype.sayName = function() {
  console.log(this.name);
}

let p1 = new Person('larry', 44, 'male');
let p2 = new Person('terry', 39, 'male');
 
p1.firends.push('robin');
console.log(p1.friends); // ['zhangsan', 'lisi', 'robin']
console.log(p2.friends); // ['zhangsan', 'lisi']
console.log(p1.sayName === p2.sayName); // true
```

### *JS 导致内存泄漏的情况有哪些？*

> 内存泄漏是由于疏忽或错误造成程序未能释放已经不再使用的内存

（1）**意外使用全局变量**

> 为了防止这种错误的发生，可以在 `JavaScript` 文件开头添加 `'use strict';` 语句

```js
function foo1(arg) {
  // 一个未声明变量的引用会在全局对象中创建一个新的变量
	bar = "this is a hidden global variable";
}
// 或者
function foo2(){
	this.varable = "this is a hidden global variable"
}
foo2(); // 调用后，this指向window全局变量
```

（2）**被遗忘的计时器和回调函数**

> 如果 `id` 为 `Node` 的元素从 `DOM` 中移除，该定时器仍会存在，同时因回调函数中包含对 `someResource` 变量的引用，导致定时器外面的 `someResource` 变量的内存不会被释放

```js
var someResource = getData();
setInterval(function() {
	var node = document.getElementById('Node');
	if(node) {
		node.innerHTML = JSON.stringify(someResource);
	}
}, 1000);
```

（3）**没有及时清理对 `DOM` 元素的引用**

> 获取一个 `DOM` 元素的引用，而后面这个元素被删除，由于一直保留了对这个元素的引用，所以它的内存无法被回收

```js
const refA = document.getElementById('refA');
document.body.removeChild(refA);
console.log(refA, 'refA');
refA = null; // 解除引用
console.log(refA, 'refA'); 
```

（4）**闭包**

> 内部函数引用外部函数变量，使其内存得不到释放

```js
function fn1() {
  var n = 1;
  return function fn2(){
    console.log(n);
  }
}
var fn = fn1();
fn();
fn = null; // 解除引用
```

（5）**事件监听 `addEventListener()`**

在不监听的情况下使用 `removeEventListener()` 取消对事件监听，常见于组件被销毁的生命钩子函数中。

