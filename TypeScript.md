# 🌮TypeScript🌮

> 🔗官方网站：https://www.typescriptlang.org/
>
> 🔗中文官网： https://nodejs.cn/typescript/handbook/

> `TypeScript` 是微软开发的开源编程语言，简称：`TS`，**是 `JavaScript` 的超集**，可以在任何运行 `JavaScript` 的地方运行，简单来说就是：`JS` 有的，`TS` 都有
>
> ⚠注意：**`TS` 需要编译才能在浏览器运行**

![TypeScript](images/TypeScript.png)

`JavaScript` 存在“先天缺陷”，`JavaScript` 中绝大部分错误都是**类型错误**，而 `TypeScript` 是一种带有**类型语法支持**的 `JavaScript` 语言，简单来说就是：`TypeScript` = `Type` + `JavaScript`。

在 `TS` 中，常用基础类型可以细分为两类：

- `JS` 已有类型

  - 简单类型：`number`、`string`、`boolean`、`null`、`undefined`、`symbol` 等
  - 复杂类型：对象、数组、函数等

- `TS` 新增类型

  联合类型 `|`、交叉类型 `&`、`type` 类型别名（自定义类型）、`interface` 接口、`tuple` 元组、字面量类型、`enum` 枚举、`void`、`any`、泛型等类型

`TypeScript` 与 `JavaScript` 两者之间的**区别🔎**，如下表格所示：

> 代码会先进行编译然后去执行
>
> - 静态类型：**编译期做类型检查**
> - 动态类型：**执行期做类型检查**

|                         `TypeScript`                         |                     `JavaScript`                     |
| :----------------------------------------------------------: | :--------------------------------------------------: |
|      `JavaScript` 的超集，用于解决大型项目的代码复杂性       |            一种脚本语言，用于创建动态网页            |
| 属于**静态类型**的编程语言，**可以在编译期间发现并纠正错误** | 属于**动态类型**的编程语言，**只能在运行时发现错误** |
|                **强类型**，支持静态和动态类型                |             **弱类型**，没有静态类型选项             |
|       最终被编译成 `JavaScript` 代码，使浏览器可以理解       |                可以直接在浏览器中使用                |
|                   **支持模块、泛型和接口**                   |              **不支持模块，泛型或接口**              |
|               社区的支持仍在增长，但还不是很大               |      大量的社区支持以及大量文档和解决问题的支持      |

`TypeScript` 相比 `JavaScript` 的**优势💡**，如下所示：

1. 更早发现错误，**提高开发效率**
2. 随时随地提示，**增强开发体验**
3. 强大类型系统，代码**可维护性更好**，**重构代码更容易**
4. 趋势：`Vue3` 源码用 `TS` 重写，`React` 和 `TS` 完美配合，`Angular` 默认支持 `TS`，**大中型前端项目首选**



## 类型注解

> **为变量提供类型约束**，约定了什么类型，就只能给该变量赋值什么类型的值，否则报错
>
> 📍语法：`let 变量名：类型注解 = 值`

```typescript
// 约定变量age的类型为number类型
let age: number = 18;
// 报错：不能将类型“string”分配给类型“number”
age = '19';
```



## number 类型

> 除了十进制和十六进制，`TS` 还支持二进制和八进制

```typescript
let decLiteral: number = 6;
let hexLiteral: number = 0xf00d;
let binaryLiteral: number = 0b1010;
let octalLiteral: number = 0o744;
```



## string 类型

> 和 `JS` 一样，可以使用双引号（`""`）或单引号（`''`）表示字符串，还可以使用模版字符串（``` `）

```typescript
let name: string = 'Gene';
let age: number = 37;
let sentence: string = `Hello, my name is ${ name }.

I'll be ${ age + 1 } years old next month.`;
```



## boolean 类型

```typescript
let isDone: boolean = false;
```



## null 和 undefined 类型

```typescript
let u: undefined = undefined;
let n: null = null;
```



## array 类型

> 在 `TS` 中，数组类型有**两种写法**

- 写法一：`类型[]`（**推荐👍**）

```typescript
let list: number[] = [1, 2, 3];
// ES5：var list = [1, 2, 3];
```

- 写法二：`Array<类型>` **泛型语法**

```typescript
let list: Array<string> = ['a', 'b', 'c'];
// ES5：var list = ['a', 'b', 'c'];
```



## void 类型

> **表示没有任何类型**，当一个函数没有返回值时，通常会见到其返回值类型是 `void` 类型

```tsx
// 声明函数返回值为void
function warnUser(): void {
  console.log("This is my warning message");
}
```

⚠需要注意的是，声明一个 `void` 类型的变量没有什么作用，因为在严格模式下，它的值只能为 `undefined`。

```tsx
let unusable: void = undefined;
```



## any 类型

> 在 `TS` 中，**任何类型都可以被归为 `any` 类型，当变量的类型指定为 `any` 的时候，`TS` 会忽略类型检查，可以对该值进行任意操作**，不会有任何错误，也不会有代码提示
>
> ⚠**不推荐**使用 `any` 类型，尽量避免使用，这会让变量失去 `TS` 类型保护的优势

```typescript
let notSure: any = 666;
notSure = "semlinker";
notSure = false;

let value: any;
value.foo.bar; // OK
value.trim(); // OK
value(); // OK
new value(); // OK
value[0][1]; // OK
```

📣**隐式**具有 `any` 类型的情况：

1. 声明变量不给类型或初始值
2. 函数形参不给类型或默认值

```typescript
// 1.声明变量不给类型或初始值
let a;

// 2.函数形参不给类型或默认值
const fn = (n) => {};
```



## unknown 类型

> 就像所有类型都可以赋值给 `any`，**所有类型也都可以赋值给 `unknown`**，比 `any` 更安全
>
> ⚠注意：**`unknown` 类型只能被赋值给 `any` 类型和 `unknown` 类型本身，并且除赋值之外的操作不被允许**

```typescript
let value: unknown;

value = true; // OK
value = 42; // OK
value = "Hello World"; // OK
value = []; // OK
value = {}; // OK
value = Math.random; // OK
value = null; // OK
value = undefined; // OK

// 能被赋值给any类型和unknown类型本身
let value1: unknown = value; // OK
let value2: any = value; // OK
let value3: boolean = value; // Error
let value4: number = value; // Error
let value5: string = value; // Error
let value6: object = value; // Error
let value7: any[] = value; // Error
let value8: Function = value; // Error

// 除了赋值之外的操作不被允许
value.foo.bar; // Error
value.trim(); // Error
value(); // Error
new value(); // Error
value[0][1]; // Error
```



## never 类型

> `never` 类型表示的是那些**永不存在的值的类型**，例如：
>
> -  `never` 类型是那些总会抛出异常或根本就不会有返回值的函数表达式或箭头函数表达式的返回值类型
> -  变量也可能是 `never` 类型，当它们被永不为真的类型保护所约束时
>
> **`never` 类型是任何类型的子类型，也可以赋值给任何类型，然而没有类型是 `never` 的子类型或可以赋值给 `never` 类型（除了 `never` 本身之外），即使  `any` 也不可以赋值给 `never`**

```typescript
// 返回never的函数必须存在无法达到的终点
function error(message: string): never {
    throw new Error(message);
}

// 推断的返回值类型为never
function fail() {
    return error("Something failed");
}

// 返回never的函数必须存在无法达到的终点
function infiniteLoop(): never {
    while (true) {
    }
}
```

**在 `TS` 中，可以利用 `never` 类型的特性来实现全面性检查**，🌰具体示例如下：

```typescript
type Foo = string | number;

function controlFlowAnalysisWithNever(foo: Foo) {
  if (typeof foo === "string") {
    // 这里foo被收窄为string类型
  } else if (typeof foo === "number") {
    // 这里foo被收窄为number类型
  } else {
    // foo在这里是never类型
    const check: never = foo;
  }
}
```



## 联合类型

> `|`（竖线）在 `TS` 中叫做**联合类型**，即**由两个或多个其他类型组成的类型**，表示数据可以是这些类型中的任意一种
>
> ⚠注意：不要与 `JS` 中的或（`||` ）混淆

```typescript
// 数组的数据中既有number类型，又有string类型
let arr1: (number | string)[] = [1, 'a', 3, 'b'];
// 需要注意“|”的优先级，要用小括号“()”包裹
// arr2表示可以是number类型或者是string类型的数组
let arr2: number | string[] = ['a', 'b'];
```



## 类型保护

> **类型保护是可执行运行时检查的一种表达式，用于确保该类型在一定的范围内**，目前主要有以下四种的方式来实现类型保护：
>
> - `typeof` 运算符
> - `in` 运算符
> - `instanceof` 运算符
> - 类型谓词

### typeof 运算符

> `TS` 期望 `typeof` 运算符返回一组特定的字符串：
>
> - `"string"`
> - `"number"`
> - `"bigint"`
> - `"boolean"`
> - `"symbol"`
> - `"undefined"`
> - `"object"`
> - `"function"`
>
> `typeof` 运算符的作用之一⚙是**类型保护**，只支持两种形式：
>
> 1. `typeof 变量或属性 === "类型"` 
> 2. `typeof 变量或属性 !== "类型"`
>
> ⚠注意：**`TS` 并不会阻止你与其它字符串比较，但不会把那些表达式识别为类型保护**
>
> `typeof` 运算符的作用之二⚙是**类型查询，在类型上下文中使用它来引用变量或属性的类型**

（1）**类型保护**

```typescript
function padLeft(value: string, padding: string | number) {
  // 将类型精炼为比声明的更具体的类型的过程称为缩小
  // 可以理解typeof来缩小不同分支中的类型
  if (typeof padding === "number") {
      return Array(padding + 1).join(" ") + value;
  }
  if (typeof padding === "string") {
      return padding + value;
  }
  throw new Error(`Expected string or number, got '${padding}'.`);
}
```

（2）**引用变量或属性的类型**

```typescript
// typeof出现在类型注解的位置（参数名称的冒号后面）所处的环境就在类型上下文
let s = "hello";
let n: typeof s;

interface Person {
  name: string;
  age: number;
}
const sem: Person = { name: 'semlinker', age: 33 };
type Sem = typeof sem; // -> Person

function toArray(x: number): Array<number> {
  return [x];
}
type Func = typeof toArray; // -> (x: number) => number[]
```

### in 运算符

> `in` 运算符用于**确定对象类型是否具有带某个名称的属性**，返回值为 `true` 或 `false`
>
> - `true` 代表具有**可选或必需**属性的类型
> - `false` 代表具有**可选或缺少**属性的类型

```typescript
type Fish = { swim: () => void };
type Bird = { fly: () => void };

function move(animal: Fish | Bird) {
  if ("swim" in animal) {
    return animal.swim();
  }
  return animal.fly();
}
```

### instanceof 运算符

> `instanceof` 运算符**只能正确判断引用数据类型**，而不能判断基本数据类型，其内部运行机制🛠是**判断构造函数的 `prototype` 属性是否出现在某个实例对象的原型链上的任何位置**

```typescript
function logValue(x: Date | string) {
  if (x instanceof Date) {
    console.log(x.toUTCString());
  } else {
    console.log(x.toUpperCase());
  }
}
```

### 类型谓词

> 只需要定义一个返回类型为类型谓词的函数，用户即可自定义类型保护
>
> **📍谓词采用 `parameterName is Type` 的形式，其中 `parameterName` 必须是当前函数中的参数名称**

```typescript
function isNumber(x: any): x is number {
  return typeof x === "number";
}

function isString(x: any): x is string {
  return typeof x === "string";
}
```



## keyof 运算符

> `keyof` 运算符可以用于获取某种（对象）类型的所有键，并**生成其键名称（可能是字符串或数字）的联合类型 `|`**

```typescript
interface Person {
  name: string;
  age: number;
  location: string;
}

type People = keyof Person;
// 等价于type People = "name" | "age" | "location"

// 如果该类型具有string或number索引签名，则keyof将返回这些类型
interface A {
  // 字符串索引 -> keyof A => string | number
  [index: string]: string; 
}

interface B {
  // 数字索引 -> keyof B => number
  [index: number]: string;
}
```



## type 类型别名

> **当同一类型（复杂）被多次使用时，可以通过类型别名给该类型取别字，即可简化该类型的使用**
>
> 📍语法：`type 类型别名 = 具体类型`
>
> 类型别名可以是任意合法的变量名称（**推荐使用大驼峰命名法👍**），创建之后直接使用该类型别名作为变量的类型注解即可，以及**可以使用交叉类型 `&` 来实现 `type` 类型别名继承的功能**

```typescript
type CustomArr = (number | string | boolean)[];
let arr1: CustomArr = [1, 'a', 4];
let arr2: CustomArr = [2, 'b', 8, true];
```



## 交叉类型

> 在 `TS` 中，**交叉类型是将多个类型合并为一个类型**（常用于对象类型），通过 `&` 运算符可以将现有的多种类型叠加到一起成为一种类型，**它包含了所需的所有类型的特性**

```typescript
type Point2D = {
  x: number;
  y: number;
};

// 使用交叉类型来实现继承的功能
// Point3D === { x: number; y: number; z: number }
type Point3D = Point2D & {
  z: number;
};

let point: Point3D = {
  x: 1,
  y: 2,
  z: 3,
};
```

### 基础类型同名属性不能合并

```typescript
interface X {
  c: string
  d: string
}

interface Y {
  c: number
  e: string
}

type XY = X & Y;
type YX = Y & X;

let p: XY = { c: 6, d: "d", e: "e" }; 
// Type 'number' is not assignable to type 'never'
let q: YX = { c: "c", d: "d", e: "e" }; 
// Type 'string' is not assignable to type 'never'
```

交叉后成员 `c` 的类型为 `string & number`，即成员 `c` 的类型既可以是 `string` 类型又可以是 `number` 类型，很明显这种类型是不存在的，所以交叉后成员 `c` 的类型变为 `never`。

### 非基础类型同名属性可以合并

```typescript
interface D { d: boolean; }
interface E { e: string; }
interface F { f: number; }

interface A { x: D; }
interface B { x: E; }
interface C { x: F; }

type ABC = A & B & C;

let abc: ABC = {
  x: {
    d: true,
    e: 'semlinker',
    f: 666
  }
};

console.log('abc:', abc);
```

以上代码成功运行后，控制台会输出以下结果：

![交叉类型](images/交叉类型.png)

由上图可知，**在交叉多个类型时，若存在相同的成员，且成员类型为非基本数据类型，那么是可以成功合并。**



## 函数类型

> 给函数指定类型，其实是**给参数和返回值指定类型**，有两种写法✏：
>
> 1. 在函数基础上**分别指定**参数和返回值类型
>    - 普通函数声明
>    - 箭头函数声明（**推荐👍**）
> 2. 使用类型别名**同时指定**参数和返回值类型
>
> ⚠需要注意的是，**类似箭头函数的语法只适用于函数表达式**

```typescript
// 1.在函数基础上分别指定参数和返回值类型
// 1.1 普通函数
function add1(num1: number, num2: number): number {
  return num1 + num2;
};
// 1.2 箭头函数（推荐👍）
const add2 = (num1: number, num2: number): number => {
  return num1 + num2;
};

// 2.使用类型别名同时指定参数和返回值类型
// 📍语法：type 类型别名 = (参数1: 类型, 参数2: 类型) => 类型
type AddFn = (num1: number, num2: number) => number;
const add3: AddFn = (num1, num2) => {
  return num1 + num2;
};
```

在 `JS` 中，如果没有返回值，默认返回的是 `undefined`，然而在 `TS` 中，**如果函数没有返回任何值，需要指定返回值类型为 `void` 类型，也可以留空，默认为 `void` 类型**，但是 `undefined` 在 `TS` 中并不是一回事，如果指定返回值类型是 `undefined`，那返回值必须是 `undefined`。

```typescript
const add = (): undefined => {
  return undefined;
};
```

### TS 函数与 JS 函数的区别

|  `TypeScript`  |     `JavaScript`      |
| :------------: | :-------------------: |
|    含有类型    |        无类型         |
|    箭头函数    | 箭头函数（`ES 2015`） |
|    函数类型    |      无函数类型       |
| 必填和可选参数 |  所有参数都是可选的   |
|    默认参数    |       默认参数        |
|    剩余参数    |       剩余参数        |
|    函数重载    |      无函数重载       |

### 可选参数

> 如果函数的参数可传也可不传，使用 `?` 将参数标记为可选
>
> 📍语法：**在参数后加 `?` 即可**
>
> ⚠需要注意的是，**可选参数只能出现在参数列表的最后**，也就是说可选参数后面不能再出现必选参数，`(start?: number, end: number)` 这样是不行❌的

```typescript
const mySlice = (start?: number, end?: number) => {
  console.log('起始Index:', start, '结束Index:', end);
};
mySlice();
mySlice(1);
mySlice(1, 2);
```

### 默认参数

> 可给函数参数**添加默认值，设置之后，`TS` 会自动推导出参数的类型**
>
> ⚠需要注意的是，**如果使用默认值，则不能再将参数变为可选**，会造成歧义

```typescript
const mySlice = (start = 0, end = 10) => {
  console.log('起始Index:', start, '结束Index:', end);
};
mySlice();
mySlice(1);
mySlice(1, 2);
```



## 类型推导

> **在 `TS` 中存在类型推导机制🛠，在某些没有明确指定类型的情况下，`TS` 也会给变量提供类型**，换句话说，由于类型推导的存在，这些地方的类型注解可以省略不写
>
> 推荐👍：**能省略类型注解的地方就省略**（充分利用 `TS` 的类型推导，提升开发效率）
>
> 技巧💡：如果你不知道类型怎么写，可以把鼠标🖱悬停变量上，可以通过 `VScode` 提示看到类型

发生类型推导的几个常见场景📣：

（1）**声明变量并初始化时（也包括设置默认参数值）**

```typescript
// 变量age的类型被自动推断为：number
let age = 18;

// 变量start、end的类型被自动推断为：number
const mySlice = (start = 0, end = 10) => {
  console.log('起始Index:', start, '结束Index:', end);
};
```

（2）**决定函数返回值时**

```typescript
// 函数返回值的类型被自动推断为：number
const add = (num1: number, num2: number) => {
  return num1 + num2;
};
```



## 类型断言

> 有时候开发人员会比 `TS` 更加明确一个值的类型，此时可以**使用类型断言 `as` 来指定更具体的类型**
>
> 📍语法：`变量 as 类型`

```html
<a href="xxx" id="link">link链接</a>
```

```typescript
// aLink的类型HTMLElement只包含所有标签公共的属性或方法
// 这个类型太宽泛，没包含a元素特有的属性或方法，如href
const aLink = document.getElementById('link')
```

```typescript
// 开发者明确知道获取的是一个a链接，可以通过类型断言给它指定一个更具体的类型
// 这样就可以访问a标签特有的属性或方法
// 如果不知道标签的类型：document.querySelector('div')在VSCode中利用鼠标🖱摸上去就可以看见
const aLink = document.getElementById('link') as HTMLAnchorElement
aLink.href = 'https://www.baidu.com/'
```

**另一种语法📍使用尖括号 `<>` 语法**，这种语法形式不常用，了解即可：

```typescript
let someValue: any = "this is a string";
let strLength: number = (<string>someValue).length;
```



## 非空断言

> 操作符 `!` 可以**用于断言操作对象是非 `null` 和非 `undefined` 类型**

下面是非空断言操作符 `!` 的一些使用场景📣：

（1）**忽略 `undefined` 和 `null` 类型**

```typescript
function myFunc(maybeString: string | undefined | null) {
  // Type 'string | null | undefined' is not assignable to type 'string'.
  // Type 'undefined' is not assignable to type 'string'. 
  const onlyString: string = maybeString; // Error
  const ignoreUndefinedAndNull: string = maybeString!; // OK
}
```

（2）**调用函数时忽略 `undefined` 类型**

```typescript
type NumGenerator = () => number;

function myFunc(numGenerator: NumGenerator | undefined) {
  // Object is possibly 'undefined'
  // Cannot invoke an object which is possibly 'undefined'
  const num1 = numGenerator(); // Error
  const num2 = numGenerator!(); // OK
}
```

⚠因为 `!` 非空断言操作符会从编译生成的 `JS` 代码中移除，所以在实际使用的过程中要特别注意。

```typescript
const a: number | undefined = undefined;
const b: number = a!;
console.log(b); 
```

以上 `TS` 代码会编译生成以下 `ES5` 代码：

```js
"use strict";
const a = undefined;
const b = a;
console.log(b);
```

虽然在 `TS` 代码中，使用非空断言使得 `const b: number = a!;` 语句可以通过 `TS` 类型检查，但在生成的 `ES5` 代码中，`!` 非空断言操作符被移除了，所以在浏览器中执行以上代码，在控制台会输出 `undefined`。



## 确定赋值断言

> 在实例属性和变量声明后面放置一个 `!` 号，**可告诉 `TS` 该属性会被明确地赋值**

为了更好地理解它的作用，可以来看个具体的例子🌰：

```typescript
let x: number;
initialize();
// Variable 'x' is used before being assigned
console.log(2 * x); // Error

function initialize() {
  x = 10;
}
```

要解决该问题，可以使用确定赋值断言，`TS` 编译器就会知道该属性会被明确地赋值：

```typescript
let x!: number;
initialize();
console.log(2 * x); // OK

function initialize() {
  x = 10;
}
```



## 对象类型

> `JS` 中的对象是由属性和方法构成的，而 `TS` 对象类型就是在描述对象的结构（有什么类型的属性和方法），对象类型的写法步骤✏：
>
> 1. 使用 `{}` 来描述对象结构
> 2. 属性采用 `属性名: 类型` 的形式
> 3. 方法采用 `方法名(): 返回值类型` 或者 `方法名: () => 返回值类型` 两种形式
> 4. 如果方法有参数，就在方法名后面的小括号 `()` 中指定参数类型 
>
> 💡技巧：**开发中，也可用 `object` 表示对象类型**

```typescript
let obj: {
  name: string;
  age: number;
  sayHi(): void;
  sayHello: () => void;
  add: (num1: number, num2: number) => number;
} = {
  name: '小明',
  age: 20,
  sayHi: () => {},
  sayHello() {},
  add: (a, b) => { return a + b; }
}
```

> **📍扩展语法**：使用类型别名（**个人推荐👍**）

```typescript
 type ObjType = {
  name: string;
  age: number;
  sayHi(): void;
  sayHello: () => void;
  add: (num1: number, num2: number) => number;
}

let obj: ObjType = {
  name: '小明',
  age: 20,
  sayHi: () => {},
  sayHello() {},
  add: (a, b) => { return a + b; }
}
```

### 可选属性

> ⚠注意：**一般可选只用在属性，而不会用在方法上**

```typescript
 type ObjType = {
  name: string;
  age?: number;
  sayHi(): void;
  sayHello: () => void;
  add: (num1: number, num2: number) => number;
}

let obj1: ObjType = {
  name: '小明',
  age: 20,
  sayHi: () => {},
  sayHello() {},
  add: (a, b) => { return a + b; }
}

let obj2: ObjType = {
  name: '小红',
  sayHi: () => {},
  sayHello() {},
  add: (a, b) => { return a + b; }
}
```



## interface 接口

> 早期时候，并没有 `type` 类型别名，`interface` 接口声明是命名对象类型的另一种方式，当一个对象类型被多次使用时，**一般会使用接口来描述对象的类型，以达到复用的目的**
>
> 📍语法：`interface 接口名称 { 对象属性和方法 }`
>
> `interface` 接口名称可以是任意合法的变量名称（**推荐使用大驼峰命名法👍**），声明之后直接使用该接口名称作为变量的类型注解即可，用法与 `type` 类型别名（有 `=` 等于号）类似，但仅可以描述对象，在过去开发框架上使用比较多

```typescript
interface IPerson {
  name: string
  age: number
  sayHi(): void
}

let person: IPerson = {
  name: "小明",
  age: 18,
  sayHi: () => {}
}
```

### 可选、只读属性

> **只读属性用于限制只能在对象刚刚创建的时候修改其值**

```typescript
interface Person {
  readonly name: string;
  age?: number;
}
```

### 任意属性

> 有时候希望一个接口中除了包含必选和可选属性之外，还允许有其他的任意属性，这时可以使用**索引签名**的形式来满足上述要求

```typescript
interface Person {
  name: string;
  age?: number;
  [propName: string]: any;
}

const p1: Person = { name: "semlinker" };
const p2: Person = { name: "lolo", age: 5 };
const p3: Person = { name: "kakuqo", sex: 1 };
```

### extends 继承

> 如果两个接口之间有相同属性或方法，可以将公共属性或方法抽离出来，通过 `extends` 继承来实现复用
>
> 📍语法：`interface 接口A extends 接口B {}`
>
> 继承后，接口 `A` 拥有接口 `B` 的所有属性和函数的类型声明
>
> ⚠注意：**同名属性不能合并，否则会报错**

```typescript
interface Point2D {
  x: number;
  y: number;
}
// 继承Point2D
interface Point3D extends Point2D {
  z: number;
}
// 继承后Point3D的结构：{ x: number; y: number; z: number }
```

**`interface` 接口和 `type` 类型别名的继承不是互斥的**，有以下两种情况📣：

（1）`interface` 接口继承 `type` 类型别名

```typescript
type PointX = { x: number; }
interface Point extends PointX { y: number; }
```

（2）`type` 类型别名继承 `interface` 接口

```typescript
interface PointX { x: number; }
type Point = PointX & { y: number; }
```

### interface VS type

|               `interface` 接口               |                       `type` 类型别名                        |
| :------------------------------------------: | :----------------------------------------------------------: |
|              **仅支持对象类型**              | **不仅支持对象类型，还支持其他类型（比如基本类型、联合类型和元组等）** |
|             通过 `extends` 继承              |                    通过交叉类型 `&` 继承                     |
| **接口可以定义多次，会被自动合并为单个接口** |                         不可重复定义                         |

```typescript
interface Point { x: number; }
interface Point { y: number; }
// 类型会合并，⚠注意：属性类型和方法类型不能重复定义
const point: Point = { x: 1, y: 2 };
```



## tuple 元组类型

> 在 `JS` 中是没有 `tuple` 元组类型的，`tuple` 元组是 `TS` 中特有的类型，是**另一种类型的数组**，其**工作方式类似于数组**，`tuple` 元组可用于定义一个**已知元素数量和类型**的数组，各元素的类型不必相同，**可以明确地标记出有多少个元素，以及每个元素的类型**
>
> ⚠注意：**使用 `tuple` 元组时，必须提供每个属性的值**
>
> 📣使用场景：在地图中利用 `tuple` 元组保存经纬度坐标来标记位置信息

```typescript
let tupleType: [string, boolean];
tupleType = ["semlinker", true];

// 与数组一样，我们可以通过下标来访问元组中的元素
console.log(tupleType[0]); // semlinker
console.log(tupleType[1]); // true

// 在元组初始化的时候，必须提供每个属性的值，不然会出现错误
tupleType = ["semlinker"];
// 此时，TypeScript编译器会提示以下错误信息：
// Property '1' is missing in type '[string]' but required in type '[string, boolean]'
```



## 字面量类型

> 任意的 `JS` 字面量（**字符串、数字、布尔、对象等**）都可以作为类型使用，**定义什么就只能赋值什么**
>
> 📣使用场景：**字面量类型常常搭配联合类型 `|` 一起使用**（**`string` 类型使用较多👍**），用来**表示一组明确的可选值列表**，比如：用户性别选择、组件按钮大小颜色等

```typescript
// 相比于string类型，使用字面量类型更加精确、严谨
type Gender = '男' | '女' | '未知'
let gender: Gender = '男'
gender = '女'
// gender的值只能是男、女、未知三者中的任意一个
// gender = '外星人' // 报错
```



## enum 枚举类型

> 枚举的功能类似于字面量类型 + 联合类型组合的功能，也可以**表示一组明确的可选值**
>
> 📍语法： `enum 枚举名 { 枚举项, 枚举项... }`
>
> **约定枚举名称、枚举项均以大写字母开头👍**，枚举中的多个值之间通过 `,`（逗号）分隔，定义好枚举后直接使用枚举名称作为类型注解

```typescript
// 创建枚举
enum Direction { Up, Down, Left, Right }

// 使用枚举类型
function changeDirection(direction: Direction) {
  console.log(direction)
}

// 调用函数时，需要应该传入枚举Direction成员的任意一个
// 类似于JS中的对象，直接通过点（.）语法访问枚举的成员
// Direction.Up = 0
changeDirection(Direction.Up)
```

### 数字枚举

> 枚举成员的值为数字的枚举称为数字枚举，**默认为从 `0` 开始自增的数值**，也可给枚举中的成员初始化值
>
> 👍**在开发中，能使用默认值就尽量使用默认值（第一项赋值，其他项递增）**

```typescript
// Up -> 0、Down -> 1、Left -> 2、Right -> 3
enum Direction { Up, Down, Left, Right }

// Down -> 11、Left -> 12、Right -> 13
enum Direction { Up = 10, Down, Left, Right }

enum Direction { Up = 2, Down = 4, Left = 8, Right = 16 }
```

### 字符串枚举

> 枚举成员的值为字符串的枚举称为字符串枚举，字符串枚举使用较少，**数字枚举使用较多，一般是使用字面量类型 + 联合类型 `|` 替代字符串枚举**👍
>
> ⚠注意：字符串枚举没有自增长行为，因此**字符串枚举的每个成员必须有初始值**

```typescript
enum Direction {
  Up = 'UP',
  Down = 'DOWN',
  Left = 'LEFT',
  Right = 'RIGHT'
}
console.log(Direction.Up) // 'UP'

// 使用字面量类型 + 联合类型替代字符串枚举
type Direction = 'up' | 'down' | 'left' | 'right'
```

### 实现原理

> 枚举是 `TS` 为数不多的非 `JS` 类型扩展（不仅仅是类型）的特性之一
>
> 因为其他类型仅仅被当做类型，而枚举不仅用作类型，还提供值（枚举成员都是有值的），也就是说，**其他的类型会在编译为 `JS` 代码时自动移除，但是枚举类型会被编译为 `JS` 代码**

```typescript
enum Direction { NORTH, SOUTH, EAST, WEST }
```

以上的枚举示例🌰经编译后，对应的 `ES5` 代码如下：

```js
"use strict";
var Direction;
(function (Direction) {
  // Direction["NORTH"] = 0 这句代码不仅会进行赋值操作，并且还会返回0
  // 下面代码等价于Direction[0] = "NORTH";
  Direction[(Direction["NORTH"] = 0)] = "NORTH";
  Direction[(Direction["SOUTH"] = 1)] = "SOUTH";
  Direction[(Direction["EAST"] = 2)] = "EAST";
  Direction[(Direction["WEST"] = 3)] = "WEST";
})(Direction || (Direction = {}));
var dir = Direction.NORTH; // => var dir = 0;
console.log(Direction) 
// {0: 'NORTH', 1: 'SOUTH', 2: 'EAST', 3: 'WEST', NORTH: 0, SOUTH: 1, EAST: 2, WEST: 3}
```

通过观察上面的编译结果，可以知道数字枚举除了支持**从成员名称到成员值**的普通映射之外，它还支持**从成员值到成员名称**的反向映射。（**📣反向映射一般用于反显后台响应回来的数据**）

```typescript
enum Direction { NORTH, SOUTH, EAST, WEST }

let dirName = Direction[0]; // NORTH
let dirVal = Direction["NORTH"]; // 0
// let dirVal = Direction.NORTH;
```



## 泛型

> 在 `TS` 中，**泛型是一种创建可复用代码组件的工具**，这种组件不只能被一种类型使用，而是**能被多种类型复用**，类似于参数的作用，其是一种用以**增强类型（`types`）、接口（`interfaces`）、函数类型等**能力的非常可靠的手段

### 泛型函数

> 泛型可以**在保证类型安全前提下，让函数与多种类型一起工作，从而实现复用**，在调用函数的时候，需要传入具体的类型，函数任何位置均可使用这个类型变量
>
> 📍语法：`function fn<T>(value: T): T { return value }`
>
> 在函数名称后加上 `<T>`，其中 `T` 代表 `Type`，在定义泛型时常用作类型变量名称，但实际上 `T` 可以用任何有效名称代替（**建议首字母大写👍**），除了 `T` 之外，以下是常见泛型变量代表的意思：
>
> - `K（Key）`：表示对象中的键类型
> - `V（Value）`：表示对象中的值类型
> - `E（Element）`：表示元素类型
>
> **泛型的类型变量可以有多个，使用 `,` 号来分隔多种类型变量，并且类型变量之间还可以相互约束**

```typescript
function identity<T, U>(value: T, message: U): T {
  console.log(message);
  return value;
}
console.log(identity<Number, string>(68, "Semlinker"));
```

除了为类型变量显式设定值之外，一种更常见的做法是使**编译器自动选择这些类型，可以完全省略尖括号** `<>`（**推荐👍**），从而使代码更简洁，比如：

```typescript
function identity<T, U>(value: T, message: U): T {
  console.log(message);
  return value;
}
// 当编译器无法推断类型或者推断的类型不准确时，就需要显式地传入类型参数
console.log(identity(68, "Semlinker"));
```

### 泛型约束

> 默认情况下，泛型函数的类型变量 `T` 可以代表多个类型，这可能会导致无法访问任何属性，如果**希望类型变量对应的类型上存在某些属性**，此时就需要为泛型添加约束来收缩类型（缩窄类型取值范围）

比如下面示例🌰中，希望能够访问 `arg` 的 `.length` 属性，但编译器无法证明每种类型都有 `.length` 属性，所以它会警告我们。

```typescript
function loggingIdentity<T>(arg: T): T {
  console.log(arg.length); // Error
  return arg;
}
```

添加泛型约束收缩类型，主要有以下几种方式：

（1）**指定更加具体的类型**

```typescript
// 比如将类型修改为T[]，因为只要是数组就一定存在length属性，因此就可以访问
function identity<T>(arg: T[]): T[] {
   console.log(arg.length);  
   return arg; 
}
```

（2）**通过 `extends` 添加约束，传入的类型必须具有某属性**

```typescript
interface Length {
  length: number;
}

function identity<T extends Length>(arg: T): T {
  console.log(arg.length); 
  return arg;
}
```

当使用不含有 `length` 属性的对象作为参数调用 `identity` 函数时，`TS` 会提示相关的错误信息：

```typescript
identity(68); 
// Argument of type '68' is not assignable to parameter of type 'Length'
```

此外还可以**使用 `,` 号来分隔多种约束类型，比如：`<T extends Length, Type2, Type3>`**。

（3）**通过 `extends` 结合 `keyof` 检查对象上的键是否存在**

```typescript
// 类型变量K受T约束，可以理解为：K只能是T所有键中的任意一个，或者说只能访问对象中存在的属性
// 这是一个类型安全的解决方案，与简单调用 let value = obj[key]; 不同
function getProperty<T, K extends keyof T>(obj: T, key: K): T[K] {
  return obj[key];
}
```

### 泛型接口

> 接口也可以配合泛型来使用，以增加其灵活性，增强其复用性
>
> 📍语法：`interface 接口名称<T> { 对象属性和方法 }`
>
> **使用泛型接口时，需要显式指定具体的类型**，接口中所有成员都可以使用类型变量

```typescript
interface User {
  name: string;
  age: number;
}

interface Goods {
  id: number;
  goodsName: string;
}

interface Data<T> {
  msg: string;
  code: number;
  data: T
}

const user: Data<User> = {
  code: 200, 
  msg: "响应成功", 
  data: {name: "zs", age: 18}
}

const goods: Data<Goods> = {
  code: 200, 
  msg: "响应成功", 
  data: {id: 1001, goodsName: "car"}
}
```

### 泛型默认值

> 为泛型中的类型变量指定默认类型（默认为 `unknown` 类型），当使用泛型时没有在代码中直接指定类型参数，从实际值参数中也无法推断出类型时，这个默认类型就会起作用（**泛型接口使用的场景较多**👍）
>
> 📍语法：`<T = Default Type>`

```typescript
interface A<T = string> {
  name: T;
}

const strA: A = { name: "Semlinker" };
const numB: A<number> = { name: 101 };
```

### 泛型工具

> `TS` 内置了一些常用的泛型工具，都是**基于映射类型实现**的，用于简化 `TS` 中的一些常见操作，可以直接在代码中使用

#### Partial

> 📍`Partial<T>` 的作用是将某个类型里的**属性全部变为可选项** `?`

```typescript
type Props =  {
  id: string
  children: number[]
}
// 新类型PartialProps结构和Props相同，但所有属性都变为可选的
type PartialProps = Partial<Props>
```

#### Readonly

> 📍`Readonly<T>` 的作用是将某个类型里的**属性全部变为 `readonly` 只读**

```typescript
type Props =  {
  id: string
  children: number[]
}
// 新类型ReadonlyProps结构和Props相同，但所有属性都变为只读的
type ReadonlyProps = Readonly<Props>
let props: ReadonlyProps = { id: '1', children: [] }
// 会报错：无法分配到"id" ，因为它是只读属性
props.id = '2'
```

#### Pick

> 📍`Pick<T, K extends keyof T>` 的作用是**从某个类型中选择一组属性来创建一个新类型**
>
> 其中 `T` 表示选择谁的属性，`K` 表示选择哪几个属性，**`K` 传入的属性只能是 `T` 中存在的属性**，如果只选择一个，则只传入该属性名即可

```typescript
interface Props {
  id: string
  title: string
  children: number[]
}
// 新类型PickProps，只有id和title两个属性
type PickProps = Pick<Props, 'id' | 'title'>
```

#### Omit

> 📍`Omit<K, T>` 的作用是**从某个类型中剔除某些属性来创建一个新类型**

```typescript
type Props {
	name: string
	age: number
	hobby: string
	sex: “男" | "女"
}

type NewProps = Omit<Props, 'hobby' | 'sex'>
// 等价于
// type NewProps = {
//   name: string
//   age: number
// }
```



## 索引签名类型

> 绝大多数情况下，都可以在使用对象前就确定对象的结构，并为对象添加准确的类型
>
> 当**无法确定对象中有哪些属性（或者说对象中可以出现任意多个属性）**，此时就用到索引签名类型
>
> 📍语法：`[key: 类型]: 类型`
>
> 其中 `key` 只是一个占位符，可以换成任意合法的变量名称

```typescript
type Horse = {};
type OnlyBoolsAndHorses = {
  [key: string]: boolean | Horse;
};

const conforms: OnlyBoolsAndHorses = {
  del: true,
  rodney: false,
};
```



## 索引访问类型

> 索引访问类型来**查找另一种类型的特定属性，通常搭配联合类型 `|`、 `keyof` 或其他类型一起使用👍**
>
> 📍语法：`类型["属性"]`
>
> ⚠注意：**`[]` 中的属性必须存在于被查询类型中，否则就会报错**

```typescript
type Person = { age: number; name: string; alive: boolean };
type Age = Person["age"]; // number
type I1 = Person["age" | "name"]; // number | string
type I2 = Person[keyof Person]; // number | string | boolean

type AliveOrName = "alive" | "name";
type I3 = Person[AliveOrName]; // boolean | string
```



## 映射类型

> 映射类型是一种泛型，建立在索引签名的语法之上，其作用是**基于旧类型创建一个新类型（对象类型）**，减少重复、提升开发效率
>
> 📍语法：`[Key in 类型]: 类型`
>
> 其中 `Key` 可换成任意合法的变量名称（**建议首字母大写👍**），**常搭配联合类型 `|` 和 `keyof` 一起使用👍**
>
> ⚠注意：**映射类型只能在 `type` 类型别名中使用，不能在 `interface` 接口中使用**

```typescript
type PropKeys = 'x' | 'y' | 'z'
type Type1 = { x: number; y: number; z: number }
// 类型Type2和类型Type1结构完全相同
type Type2 = { [Key in PropKeys]: number }

type Props = { a: number; b: string; c: boolean }
type Type3 = { [Key in keyof Props]: number }

// 泛型工具都是基于映射类型实现
type Partial<T> = {
  [P in keyof T]?: T[P];
};
```



## 类型兼容性

> 类型系统可分为结构化类型系统（`Structural Type System`）和标明类型系统（`Nominal Type System`），**`TS` 采用的是结构化类型系统**，`C#`、`Java` 等采用的是标明类型系统
>
> 在结构类型系统中，可以简单认为**如果两个对象具有相同的形状，则认为它们属于同一类型**
>
> `TS` 结构类型系统的基本规则🛠：**如果 `y` 至少具有与 `x` 相同的成员，则 `x` 与 `y` 兼容**

- **`interface` 接口兼容性**

> 通俗来讲，**成员多的可以赋值给少的**

```typescript
interface Point { x: number; y: number }
interface Point2D { x: number; y: number }
interface Point3D { x: number; y: number; z: number }

let p1: Point = { x: 1, y: 1 }
let p2: Point2D = p1
let p3: Point3D = { x: 1, y: 1, z: 1 }
// 成员多的可以赋值给少的
p2 = p3
```

- **函数兼容性**

> 函数之间兼容性比较复杂，需要**考虑参数个数和类型、返回值类型**

（1）**参数少的可以赋值给多的，相同位置的参数类型要相同或兼容**

```typescript
let x = (a: number) => 0;
let y = (b: number, s: string) => 0;

y = x; // OK
x = y; // Error
```

允许这种赋值的原因是实际上在 `JS` 中忽略额外的函数参数是很常见的，这样的使用方式促成了 `TS` 中函数类型之间的兼容性。

```typescript
let items = [1, 2, 3];
items.forEach((item, index, array) => console.log(item));
items.forEach((item) => console.log(item));
```

（2）**仅返回类型不同的函数**，分为两种情况：

1. 如果返回值类型是**简单类型，两个类型要相同**
2. 如果返回值类型是**对象类型，成员多的可以赋值给成员少的**

```typescript
// 1.简单类型
let a = () => 0;
let b = () => 'abc';
a = b; // Error

// 2.对象类型
let x = () => ({ name: "Alice" });
let y = () => ({ name: "Alice", location: "Seattle" });
x = y; // OK
y = x; // Error
```



## .d.ts 类型声明文件

> 通俗来讲，**在 `TS` 中以 `.d.ts` 为后缀的文件，称之为类型声明文件，它的主要作用⚙是描述 `JS` 模块内所有导出成员的类型信息**
>
> 项目中安装的第三方库里面都是打包后的 `JS` 代码，但在使用时却有对应的 `TS` 类型提示，正因为在第三方库中的 `JS` 代码都有对应的 `TS` 类型声明文件

在 `TS` 中有两种文件类型：

|            `.ts` 文件            |                         `.d.ts` 文件                         |
| :------------------------------: | :----------------------------------------------------------: |
|        编写程序代码的地方        |                     为 `JS` 提供类型信息                     |
| **既包含类型信息，又可执行代码** |                        只包含类型信息                        |
|     可以被编译为 `.js` 文件      | **不会生成 `.js` 文件，在 `.d.ts` 文件中不允许出现可执行的代码，仅用于提供类型信息** |

### 第三方库类型声明文件

> 目前，几乎所有常用的第三方库都有相应的类型声明文件，除了第三方库类型声明文件，`TS` 还有内置类型声明文件：`TS` 为 `JS` 运行时可用的所有标准化内置 `API` 都提供了声明文件

第三方库的类型声明文件有两种存在形式：

1. **库自带**类型声明文件：正常导入第三方库，`TS` 就会自动加载库自己的类型声明文件，以提供该库的类型声明
2. **由 [DefinitelyTyped](https://github.com/DefinitelyTyped/DefinitelyTyped/) 提供**，这是一个 `github` 仓库，用来提供高质量 `TS` 类型声明，可以通过 `npm` 来下载该仓库提供的 `TS` 类型声明包，这些包的名称格式为：`@types/*`

   - 比如：`jquery` 安装后导入，没有自带的声明文件，会提示需要安装 `@types/jquery` 类型声明包

   - 当安装相应的 `@types/*` 类型声明包后，`TS` 也会自动加载该类声明包，以提供该库的类型声明

### 自定义类型声明文件

> 创建自己的类型声明文件：
>
> - 项目内共享类型
> - 为已有 `JS` 文件提供类型声明

#### 共享类型

> **如果多个 `.ts` 文件中都用到同一个类型，可创建 `.d.ts` 文件提供该类型，实现类型共享**（**重要👍**）
>
> 操作步骤✏：
>
> 1. 创建 `index.d.ts` 类型声明文件
> 2. 创建需要共享的类型并使用 `export` 导出（`TS` 中的类型也可使用 `import/export` 实现模块化功能）
> 3. 在需要使用共享类型的 `.ts` 文件中，通过 `import` 导入即可（`.d.ts` 后缀导入时可直接省略）

```typescript
// src/types/data.d.ts
export type Person = {
  id: number;
  name: string;
  age: number;
};
```

```js
// src/user.ts
import { Person } from './types/data'
const p: Person = {
  id: 100,
  name: 'jack',
  age: 19
}
```

#### 给 JS 文件提供类型

> **在 `.ts` 文件中，也可以引入 `.js` 文件，`TS` 会自动加载与 `.js` 同名的 `.d.ts` 文件，以提供类型声明**
>
> 📍语法：`declare` 关键字
>
> `declare` 用于类型声明，为其他地方（比如 `.js` 文件）已存在的变量声明类型，而不是定义一个新的变量，🛠规则如下：
>
> 1. 对于 **`TS` 独有的关键字**，**可以省略 `declare` 关键字**，如 `type`、`interface` 等
> 2. 对于 **`TS`、`JS` 都有的关键字**，**要使用 `declare` 关键字**，如 `let`、`function` 等，明确指定此处用于类型声明

```js
// add/index.js
const addFn = (a, b) => {
  return a + b;
};

const pointFn = (p) => {
  console.log('坐标：', p.x, p.y);
};

export { addFn, pointFn }
```

```typescript
// add/index.d.ts
declare const addFn: (a: number, b: number) => number;

type Position = {
  x: number;
  y: number;
};
declare const pointFn: (p: Position) => void;

// 注意要导出
export { addFn , pointFn };
```

```typescript
// main.ts
import { addFn, pointFn } from './add';

addFn(3, 10)
pointFn({x: 100, y: 200})
```

#### 给第三方模块提供类型

> 使用关键字 `module`，其步骤如下✏：
>
> 1. 定义同名类型声明文件：`包名称.d.ts`
> 2. 使用 `declare module "包名称" {}` 定义模块类型
>
> `TS` 会自动加载 `包名称.d.ts` 文件中的类型，⚠需要注意的是：
>
> 1. `module` 范围内，不需再使用 `declare` 关键字
> 2. 类型声明文件名称、`module` 名称、模块包三者同名

```typescript
// abc.d.ts
declare module "abc" {
  // 注意：module范围内，不需要再使用declare关键字
  function $(tagName: "div" | "span"): any
  export default $
}
```

```typescript
// main.ts
import $ from 'abc';

$("span")
```
