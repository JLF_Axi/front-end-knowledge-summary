# 🧊Git🧊

> **分布式版本控制工具**，其有三大区域：工作区、暂存区和 `Git` 版本库
>
> **优点**：
>
> 1. 断网后支持离线本地版本提交，联网后再同步到服务器即可
> 2. 每个用户那里保存的都是所有版本数据，只要一个用户的设备没有问题就可以恢复所有数据
> 3. 支持多人协作开发
>
> **缺点**：
>
> 1. 由于保存所有版本数据，占用空间较大

- `Git` 常用命令

|                             指令                             | 描述                                                         |
| :----------------------------------------------------------: | ------------------------------------------------------------ |
|                     `git config --list`                      | 查看配置信息                                                 |
| `git config --global user.name ''`<br />`git config --global user.email ''` | 配置名字和邮箱                                               |
|                       **⭐`git init`**                        | **初始化仓库**                                               |
|                        **⭐`git add`**                        | **将文件添加到暂存区**，指定多个文件名，用空格隔开，**`.` 或者 `*` 代表添加所有文件** |
|                   **⭐`git commit -m ''`**                    | **将暂存区的文件提交到本地仓库**                             |
|                      `git status (-s)`                       | 检查文件详细状态，`-s` 即简要状态<br />不同标记代表的含义：<br />绿`A`：已执行了`git add` 命令<br />红`M`：被用户修改过<br />红`D`：已在工作区删除<br />红`？？`：新创建 |
|                        **⭐`git log`**                        | **查看历史版本**                                             |
|                **⭐`git reset --hard 版本号`**                | **可以使本地仓库代码回滚到指定版本**<br />⚠危险操作（**慎重**）：会将不属于此版本的数据全部删除<br />版本回退之后，无法直接 `push` 到远程仓库，`git push -f` 覆盖推送即可，将远程仓库也进行回退 |
|                      `git branch (-a)`                       | 查看本地所有分支，分支前的 `*` 号代表当前代码所在分支<br />`-a` 可查看本地和远程分支 |
|                     `git branch 分支名`                      | 创建本地分支                                                 |
|                    `git checkout 分支名`                     | 切换到其他本地分支                                           |
|                **⭐`git checkout -b 分支名`**                 | **创建并切换本地分支**                                       |
|                    `git branch -d 分支名`                    | 删除本地分支<br />⚠注意：不能删除当前所处分支，需先切换到其他分支 |
|                   **⭐`git merge 分支名`**                    | **合并本地分支，将分支代码合并到当前分支**<br />如果合并分支和当前分支内容相同，合并时会提示 `Already up to date` |
|                   `git branch -m 新分支名`                   | 修改当前的本地分支名                                         |
|                **⭐`git clone 远程仓库地址`**                 | **将远程仓库代码下载到本地**                                 |
|                       `git remote -v`                        | 查看所有远程仓库                                             |
|           **⭐`git remote add 别名 远程仓库地址`**            | **添加远程仓库**                                             |
|                   `git remote remove 别名`                   | 删除远程仓库                                                 |
|              `git remote rename 旧别名 新别名`               | 修改远程仓库名                                               |
|           **⭐`git push (-u) 远程仓库地址 分支名`**           | **将本地代码推送到远程仓库中**<br />⚠注意：使用 `-u` 指定一个默认别名，之后可以不加任何参数使用 `git push` 来提交代码 |
|             **⭐`git pull 远程仓库地址 分支名`**              | **从远程仓库拉取代码并合并到本地**                           |

-  代码提交

| `commit` 常见前缀 |                说明                |
| :---------------: | :--------------------------------: |
|      `feat`       |               新功能               |
|       `fix`       |             修复 `BUG`             |
|      `docs`       |              文档修改              |
|      `perf`       |              性能优化              |
|     `revert`      |              版本回退              |
|       `ci`        |          `CICD` 集成相关           |
|      `test`       |            添加测试代码            |
|    `refactor`     |              代码重构              |
|      `build`      |       影响项目构建或依赖修改       |
|      `style`      |      不影响程序逻辑的代码修改      |
|      `chore`      | 不属于以上类型的其他类型(日常事务) |